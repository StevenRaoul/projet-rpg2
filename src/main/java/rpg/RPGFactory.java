package rpg;

import com.almasb.fxgl.dsl.FXGL;
import com.almasb.fxgl.entity.Entity;
import com.almasb.fxgl.entity.EntityFactory;
import com.almasb.fxgl.entity.SpawnData;
import com.almasb.fxgl.entity.Spawns;
import com.almasb.fxgl.entity.components.CollidableComponent;
import com.almasb.fxgl.entity.components.IrremovableComponent;
import com.almasb.fxgl.physics.BoundingShape;
import com.almasb.fxgl.physics.HitBox;


public class RPGFactory implements EntityFactory{
	
	@Spawns("bloc")
	public Entity newBloc(SpawnData data ) {
		//Créer un objet de type bloc 
		return FXGL.entityBuilder(data)
				.type(EntityType.BLOC)
				.bbox(new HitBox(BoundingShape.box(50, 50)))
				.with(new CollidableComponent(true))
				.build();
	}
	
	@Spawns("monstre")
	public Entity newMonster(SpawnData data ) {
		//Créer un objet de type bloc 
		return FXGL.entityBuilder(data)
				.type(EntityType.MONSTER)
				.bbox(new HitBox(BoundingShape.box(50, 50)))
				.with(new CollidableComponent(true))
				.build();
	}
	
	@Spawns("coffre")
	public Entity newCoffre(SpawnData data ) {
		//Créer un objet de type bloc 
		return FXGL.entityBuilder(data)
				.type(EntityType.CHEST)
				.bbox(new HitBox(BoundingShape.box(50, 50)))
				.with(new CollidableComponent(true))
				.build();
	}
	
	@Spawns("portal")
	public Entity newPortal(SpawnData data ) {
		//Créer un objet de type bloc 
		return FXGL.entityBuilder(data)
				.type(EntityType.PORTAL)
				.with(new CollidableComponent(true))
				.bbox(new HitBox(BoundingShape.box(50, 50)))
				.build();
	}
	
	@Spawns("player")
	public Entity newPlayer(SpawnData data ) {
	return FXGL.entityBuilder(data)
			.type(EntityType.PLAYER)
			.bbox(new HitBox(BoundingShape.box(63, 63)))
			.with(new CollidableComponent(true))
			.with(new IrremovableComponent())
			.build();
	}
	
	@Spawns("pnj")
	public Entity newNPC(SpawnData data ) {
		//Créer un objet de type pnj 
		return FXGL.entityBuilder(data)
				.type(EntityType.NPC)
				.bbox(new HitBox(BoundingShape.box(63, 63)))
				.with(new CollidableComponent(true))
				.build();
	}
	
	@Spawns("torche")
	public Entity newTorch(SpawnData data) {
		return FXGL.entityBuilder(data)
				.view("animations/monde/Torche.gif")
				.build();
	}
	
	@Spawns("feu")
	public Entity newFire(SpawnData data) {
		return FXGL.entityBuilder(data)
				.view("animations/monde/Feu.gif")
				.build();
	}
	
	@Spawns("indice")
	public Entity newClue(SpawnData data) {
		return FXGL.entityBuilder(data)
				.type(EntityType.CLUE)
				.view("animations/monde/Indice.gif")
				.build();
	}
	
	@Spawns("achat")
	public Entity newPurchase(SpawnData data) {
		return FXGL.entityBuilder(data)
				.type(EntityType.PURCHASE)
				.bbox(new HitBox(BoundingShape.box(50, 100)))
				.with(new CollidableComponent(true))
				.build();
	}
}
