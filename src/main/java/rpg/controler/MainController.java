package rpg.controler;

import com.almasb.fxgl.entity.Entity;

import javafx.geometry.Point2D;
import javafx.scene.Node;
import rpg.RPGApp;
import rpg.entities.character.Hero;
import rpg.entities.character.Monster;
import rpg.entities.character.Npc;
import rpg.entities.items.Item;
import rpg.system.QuestTarget;
import rpg.system.SaveAndLoadController;
import rpg.view.DisplayQuest;

public class MainController {
	private SaveAndLoadController saveAndLoadController = new SaveAndLoadController();
	private BestiaryController bestiaryController = new BestiaryController();
	private FightController fightController = new FightController();
	private MapController mapController =  new MapController();
	private QuestComponent questController = new QuestComponent();
	private NpcComponent npcController = new NpcComponent();
	private UserInterfaceController userInterfaceController = new UserInterfaceController();
	private HeroController heroController =new HeroController();
	private ChestController chestController = new ChestController();
	private PurchaseController purchaseController = new PurchaseController();

	private FileController fileController = new FileController();

	private MusicController musicController = new MusicController();

	private Hero hero;

	public MainController(){}
	
	public void init(Hero hero) {
		try {
			bestiaryController.loadBestiary();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		mapController.init(bestiaryController, purchaseController);
		purchaseController.init(mapController.getPurchases("shopRPG.tmx"));
		npcController.initNpc("src/main/resources/assets/npcs/baseNpc.json");
		musicController.musicPlayMap(hero.getCurrentMap());
		DialogComponent.setDialog("maisonRPG.tmx", new Point2D(1024, 960), "src/main/resources/assets/dialogues/pere/dialogue-1-0.json");
		this.hero = hero;
		fightController.init(hero,heroController,questController);
		userInterfaceController.init(hero);
		chestController.init(userInterfaceController);
		npcController.init(questController, fileController);
		questController.init(mapController, npcController, fileController,hero);
	}
	public void changeUINodeVisibility(String property) {
		userInterfaceController.changeUINodeVisibility(property);
	}
	public void nextQuest() {
		questController.nextQuest();
	}
	public void analyze() {
		questController.advanceQuest(QuestTarget.CLUE);
	}
	public void beginDialog(Npc npc) {
		npcController.initDialog(npc);
	}
	public Node getUINode(String property) {
		return userInterfaceController.getUINode(property);
	}
	
	public void findChest(Point2D pos) {
		chestController.findChest(pos);
	}
	
	public void initUINodes() {
		initUINode("Hero",heroController.getInitImage());
		initUINode("Quest",DisplayQuest.createQuest());
		initUINode("Inventory",userInterfaceController.getInventory());
		initUINode("Equipment",userInterfaceController.getEquipment());
	}
	
	public void initUINode(String property,Node node) {
		userInterfaceController.addUINode(property,node);
	}
	
	public void initFight(Monster monster,Entity monsterEntity) {
		fightController.displayFight(monster,monsterEntity);
		musicController.musicPlayBattle(monster);	//monster.getMusic()
	}
	public void updateMonsterToDeathImage() {
		
	}
	public void initScene() {
		fightController.init(hero,heroController,questController);
	}
	
	public BestiaryController getBestiaryController() {
		return bestiaryController;
	}
	
	public MapController getMapController() {
		return mapController;
	}
	
	public void changeMap(String oldMap, Point2D pos) {
		RPGApp.player.getComponent(MoveComponent.class).hyperClean();
		mapController.changeMap(oldMap, pos);
	}
	
	public void save(Hero hero) {
		//if preconditions true then
		saveAndLoadController.save(hero);
	}
	
	public void save(Hero hero, String nameSave) {
		saveAndLoadController.save(hero, nameSave);
	}
	
	public Hero load() {
		return saveAndLoadController.load();
	}
	
	public Hero save(String nameSave) {
		return saveAndLoadController.load(nameSave);
	}
	
	public void lookPurchase(Point2D position) {
		purchaseController.lookPurchase(position);
	}
	
	public void quitPurchase(Point2D position) {
		purchaseController.quitPurchase(position);
	}
	
	public void buy() {
		purchaseController.buy(hero);
	}
	
	public void playSound(String sound) {
		musicController.soundPlay(sound);
	}
	
	public void playMusic(String music) {
		musicController.musicPlay(music);
	}
	
	public void playMusicMap(String music) {
		musicController.musicPlayMap(music);
	}
	
	public void playMusicBattle(Monster monster) {
		musicController.musicPlayBattle(monster);
	}
	
	public void replayMusic() {
		musicController.musicReplay();
	}
	
	public void updateInventory(String action, Item item, int index) {
		userInterfaceController.updateInventory(action, item, index);
	}
}