package rpg.controler;

import com.almasb.fxgl.dsl.FXGL;
//import com.almasb.fxgl.app.FXGL;
import com.almasb.fxgl.entity.Entity;
import com.almasb.fxgl.entity.component.Component;

import javafx.scene.text.Text;
import rpg.RPGApp;
import rpg.view.DisplayBasic;

public class PlayerComponent extends Component {
	
	public static void levelUp(int level) {
		String notif = "Félicitations tu est maintenant niveau " + level + " !";
		if (RPGApp.listeMaps.get(RPGApp.hero.getCurrentMap()).notif != null) {
			Entity notifEntity = RPGApp.listeMaps.get(RPGApp.hero.getCurrentMap()).notif;
			String oldText = ((Text) (notifEntity.getViewComponent().getChildren().get(1))).getText();
			DisplayBasic.updateNotif(oldText + "\n" + notif);
		} 
		else {
			RPGApp.listeMaps.get(RPGApp.hero.getCurrentMap()).notif = DisplayBasic.createNotif(notif);
			FXGL.getGameWorld().addEntity(RPGApp.listeMaps.get(RPGApp.hero.getCurrentMap()).notif);
		}
	}
}