package rpg.controler;

import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.almasb.fxgl.dsl.FXGL;
//import com.almasb.fxgl;
import com.almasb.fxgl.entity.component.Component;

import javafx.geometry.Point2D;
import javafx.scene.text.Text;
import rpg.RPGApp;
import rpg.entities.items.Item;
import rpg.entities.character.Hero;
import rpg.entities.character.Monsters;
import rpg.entities.character.Npc;
import rpg.entities.objects.Chest;
import rpg.entities.objects.Clue;
import rpg.system.ModeleMap;
import rpg.system.Quest;
import rpg.system.QuestTarget;
import rpg.view.DisplayBasic;
import rpg.view.DisplayQuest;


public class QuestComponent extends Component{
	
	private static String pathD = "src/main/resources/assets/dialogues/";
	private static String pathQ = "src/main/resources/assets/quest/";
	private static JSONParser parser = new JSONParser();
	private Object obj;
	private static JSONArray dialogs;
	private static JSONArray clues;
	private static JSONArray monsters;
	private static JSONArray chests;
	private static Iterator<JSONObject> iteratorD;
	private static Iterator<JSONObject> iteratorCl;
	private static Iterator<JSONObject> iteratorM;
	private static Iterator<JSONObject> iteratorCh;
	private static Iterator<JSONObject> iterator;
	private static JSONObject iteratorNextD;
	private static JSONObject iteratorNextCl;
	private static JSONObject iteratorNextM;
	private static JSONObject iteratorNextCh;
	private static JSONObject iteratorNext;
	private static JSONObject spawns;
	private static JSONObject positions;
	private static Quest heroQuest = new Quest();
	private static Quest npcQuest = new Quest();
	private static String givequest;
	private static Point2D position;
	private FileController fileController;
	private ArrayList<ArrayList<Quest>> quests = new ArrayList<>();
	private MapController mapController;
	private NpcComponent npcController;
	private DisplayQuest displayQuest = new DisplayQuest();
	private Hero hero;
	
	public void init(MapController mapController, NpcComponent npcComponent, FileController fileController,Hero hero) {
		this.mapController=mapController;
		this.npcController=npcComponent;
		this.fileController=fileController;
		this.hero=hero;
		loadQuests();
	}
	public void nextQuest() {
		nextQuest(hero.getCurrentquest());
	}
	public Quest getQuest(int id, int step) {
		System.out.println("Id : "+id+"// Step : "+step);
		return quests.get(id).get(step);
	}
	public void loadQuests() {
		ArrayList<File> questFiles = fileController.getQuestFiles();
		if(questFiles!=null) {
			ArrayList<Quest> questsContainer2 = new ArrayList<>();
			quests.add(questsContainer2);
			ArrayList<Quest> questsContainer = new ArrayList<>();
			for (File questFile : questFiles) {
				System.out.println(questFile.getName());
				JSONObject  questJSO = fileController.getQuest(questFile);
				Quest quest = fileController.getQuest(questJSO);
				questsContainer.add(quest);
			}
			quests.add(questsContainer); 
		}
		
	}
	public void manageQuest(int Id, int step) {
		if(hero.getCurrentquest()!=null) {
			nextQuest(quests.get(Id).get(step));
		}else {
			playQuest(Id,step);
		}
	}
	public void spawnsForQuest(Quest q) {
		for(LinkedHashMap clue : ((ArrayList<LinkedHashMap>)q.getSpawns().get("clue"))) {
			mapController.addClue(
					(String)clue.get("map"), 
					new Clue(
							(String)clue.get("name"),
							(String)clue.get("image")),
					new Point2D(
							(int)((LinkedHashMap)clue.get("position")).get("x"),
							(int)((LinkedHashMap)clue.get("position")).get("y"))
					);
		}
		
		for(LinkedHashMap monster : ((ArrayList<LinkedHashMap>)q.getSpawns().get("monster"))) {
			mapController.addMonster(
					(String)monster.get("map"),
					Monsters.valueOf((String)monster.get("type")),
					new Point2D(
							(int)((LinkedHashMap)monster.get("position")).get("x"),
							(int)((LinkedHashMap)monster.get("position")).get("y"))
					);
		}
		
		for(LinkedHashMap chest : ((ArrayList<LinkedHashMap>)q.getSpawns().get("chest"))) {
			mapController.addChest(
					(String)chest.get("map"),
					new Chest((Item)chest.get("loot")),
					new Point2D(
							(int)((LinkedHashMap)chest.get("position")).get("x"),
							(int)((LinkedHashMap)chest.get("position")).get("y"))
					);
		}
	}
	public void playQuest(int Id, int step) {
		Quest q = quests.get(Id).get(step);
		spawnsForQuest(q);
		setDialogsForQuest(q);
		System.out.println(q.getName());
		hero.setCurrentquest(q);
		displayQuest.updateQuest(q);
		
	}
	public void setDialogsForQuest(Quest q) {
		for (String npcName : q.getNpcs()) {
			npcController.setDialogForQuest(npcName, q.getStep(), q.getId());
		}
	}
	public void nextQuest(Quest q) {
		notifQuest(q);
		playQuest(q.getId(),q.getStep()+1);
		
	}
	public void advanceQuest(QuestTarget from,Monsters type) {
		Quest q = hero.getCurrentquest();
		//if(type.equals(q.getTypeMonstre())) {
			advanceQuest(from);
		//}
	}
	public void advanceQuest(QuestTarget from,String nameNpc) {
		Quest q = hero.getCurrentquest();
		System.out.println(nameNpc);
		System.out.println(q.getTargetToSee());
		if(nameNpc.equals(q.getTargetToSee())) {
			System.out.println("la");
			advanceQuest(from);
		}
	}
	public void advanceQuest(QuestTarget from) {
		Quest q = hero.getCurrentquest();
		if(q!=null) {
			if(q.getTypeTarget()==from) {
				if(q.advance()) {
					nextQuest(q);
				}
			}
		}
	}
	public void checkQuest(Quest q) {
		
	}
	/*public static void setQuest(String file) {
		
		try {
			Object obj = parser.parse(new FileReader(pathQ + file));
			JSONObject jsonObject = (JSONObject) obj;
			
			//Change dialog & add npcQuest
			dialogs = (JSONArray) jsonObject.get("dialogs");
			iteratorD = dialogs.iterator(); 
			while (iteratorD.hasNext()) {
				iteratorNextD = iteratorD.next();
				positions = (JSONObject) iteratorNextD.get("position");
				position = new Point2D(((Long) positions.get("x")).doubleValue(), ((Long) positions.get("y")).doubleValue());
				if (jsonObject.get("next")=="") {
					DialogComponent.setDialog(
							(String) iteratorNextD.get("map"), 
							position, 
							pathD + iteratorNextD.get("npc") + "/dialogue-base.json");
				} 
				else { 
					DialogComponent.setDialog(
							(String) iteratorNextD.get("map"), 
							position, 
							pathD + iteratorNextD.get("npc") + "/dialogue-" + jsonObject.get("quest") + "-" + jsonObject.get("step") + ".json");
				}
				npcQuest = new Quest((String) iteratorNextD.get("quest"));
				Npc npc = RPGApp.listeMaps.get(iteratorNextD.get("map")).getPNJList().get(position);
				npc.setQuest(
						npcQuest, 
						(String) iteratorNextD.get("givequest")
						);
			}
			
			//spawn monster
			spawns = (JSONObject) jsonObject.get("spawns");
			monsters = (JSONArray) spawns.get("monster");
			iteratorM = monsters.iterator();
			while (iteratorM.hasNext()) {
				iteratorNextM = iteratorM.next();
				JSONObject stats = (JSONObject) iteratorNextM.get("stats");
				positions = (JSONObject) iteratorNextM.get("position");
				position = new Point2D(((Long) positions.get("x")).doubleValue(), ((Long) positions.get("y")).doubleValue());
				RPGApp.createMonster(
						(String) iteratorNextM.get("map"), 
								(Monsters.valueOf((String) iteratorNextM.get("type"))
						), 
				position);
			}

			//spawn clue
			clues = (JSONArray) spawns.get("clue");
			iteratorCl = clues.iterator();
			while (iteratorCl.hasNext()) {
				iteratorNextCl = iteratorCl.next();
				positions = (JSONObject) iteratorNextCl.get("position");
				position = new Point2D(((Long) positions.get("x")).doubleValue(), ((Long) positions.get("y")).doubleValue());
				RPGApp.createClue(
						(String) iteratorNextCl.get("map"), 
						position, 
						new Clue(
								(String) iteratorNextCl.get("name"),
								(String) iteratorNextCl.get("image")
						)
				);
			}

			//spawn chest
			chests = (JSONArray) spawns.get("chest");
			iteratorCh = chests.iterator();
			while (iteratorCh.hasNext()) {
				iteratorNextCh = iteratorCh.next();
				positions = (JSONObject) iteratorNextCh.get("position");
				position = new Point2D(((Long) positions.get("x")).doubleValue(), ((Long) positions.get("y")).doubleValue());
				RPGApp.createChest(
						(String) iteratorNextCh.get("map"), 
						position, 
						new Chest());
			}

			//Set quest
			JSONObject goal = (JSONObject) jsonObject.get("goal");
			if (jsonObject.get("next")=="") {
				RPGApp.hero.setCurrentquest(null);
				RPGApp.hero.setQueststep(0);
			}
			else {
				heroQuest = new Quest(
						(String) jsonObject.get("name"), 
						((Long) jsonObject.get("reward")).intValue(), 
						(String) goal.get("type"), 
						QuestTarget.valueOf((String) goal.get("target")), 
						((Long) goal.get("number")).intValue(), 
						(String) jsonObject.get("description"), 
						((Long) jsonObject.get("quest")).intValue()
				);
				RPGApp.hero.setCurrentquest(heroQuest);
				RPGApp.hero.setQueststep(((Long) jsonObject.get("step")).intValue());
				RPGApp.mainController.playSound("getQuest.mp3");
			}
			DisplayQuest.updateQuest();
		} catch (Exception e) {
			e.printStackTrace();		
		}
	}*/
	
	public void endQuest() {
		notifQuest(RPGApp.hero.getCurrentquest());
		RPGApp.hero.setCurrentquest(null);
		RPGApp.hero.setQueststep(0);
		
	}
	
	/*public static void nextQuest() {
		int currentQuest = RPGApp.hero.getCurrentquest().getId();
		int currentStep = RPGApp.hero.getQueststep();
		String currentQuestFile = pathQ + "quest-" + currentQuest + "-" + currentStep + ".json";
		try {
			Object obj = parser.parse(new FileReader(currentQuestFile));
			JSONObject jsonObject = (JSONObject) obj;
			String newQuestFile = "quest-" + jsonObject.get("next") + ".json";
			if (!newQuestFile.equals( "quest--.json")) {
				setQuest(newQuestFile);
			}
			else {
				endQuest();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void verifQuest() {
		Quest q = RPGApp.hero.getCurrentquest();
		if(q.getNbTarget()==q.getNbTargetMax()) {
<<<<<<< HEAD
			q.validQuest(RPGApp.hero);
=======
			q.validQuest();
			RPGApp.mainController.playSound("finishQuest.mp3");
>>>>>>> main
			notifQuest(q);
			nextQuest();
		}
		else {
			System.out.println("la quête n'est pas accomplie");
		}
	}*/
	
	public static void notifQuest(Quest quest) {
		String notif = quest.getName() +" accomplie !";
		if (RPGApp.listeMaps.get(RPGApp.hero.getCurrentMap()).notif != null) {
			ModeleMap map = RPGApp.listeMaps.get(RPGApp.hero.getCurrentMap());
			String textAvant=((Text)((Text) map.notif.getViewComponent().getChildren().get(1))).getText();
			DisplayBasic.updateNotif(textAvant + "\n" + notif);
		}
		else {
			RPGApp.listeMaps.get(RPGApp.hero.getCurrentMap()).notif = DisplayBasic.createNotif(notif);
			FXGL.getGameWorld().addEntity(RPGApp.listeMaps.get(RPGApp.hero.getCurrentMap()).notif);
		}
	}
	
}
