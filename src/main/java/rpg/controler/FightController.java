package rpg.controler;

import java.util.HashMap;
import java.util.Map.Entry;

import com.almasb.fxgl.entity.Entity;

import javafx.geometry.Point2D;
import javafx.scene.paint.Color;
import rpg.RPGApp;
import rpg.entities.abilities.Attack;
import rpg.entities.abilities.Boost;
import rpg.entities.abilities.Skill;
import rpg.entities.abilities.SkillType;
import rpg.entities.character.Character;
import rpg.entities.character.Hero;
import rpg.entities.character.Monster;
import rpg.entities.character.Monsters;
import rpg.entities.character.State;
import rpg.entities.items.Item;
import rpg.entities.items.Weapon;
import rpg.system.QuestTarget;
import rpg.view.DisplayBattle;

public class FightController {
	private DisplayBattle displayBattle = new DisplayBattle();
	private SceneController sceneController = new SceneController();
	private MonsterController monsterController = new MonsterController();
	private String pathAnimationBattle = "animations/combat/";
	private Hero hero;
	private Monster monster;
	private ActionController actionController;
	private HashMap<Character, Boolean> CanAct = new HashMap<>();
	private HeroController heroController = new HeroController();
	private QuestComponent questController;
	

	public void manage(String choice, Character character) {
		if (choice.equals("attaque") || choice.equals("défense")) {
			useBasicAction(choice, character);
			endAction(actionController.getOpponent(character));
		} else {
			RPGApp.mainController.playSound("fuite.mp3");
			dissapear();
		}
	}
	
	public Boolean getAct(Character character) {
		return CanAct.get(character);
	}
	public HashMap<Character, Boolean> getCanAct() {
		return CanAct;
	}
	public void manage(Skill skill, Character character) {
		useSkill(skill, character);
		endAction(actionController.getOpponent(character));
	}

	public Hero getHero() {
		return hero;
	}

	public Monster getMonster() {
		return monster;
	}

	public void useBasicAction(String choice, Character character) {
		if (!choice.equals("skill")) {
			CanAct.replace(character, false);
			actionController.basicAction(choice, character);
		}
	}

	public void useSkill(Skill skill, Character character) {
		CanAct.replace(character, false);
		actionController.skillAction(skill, character);
		if (character instanceof Hero) {
			RPGApp.mainController.playSound(skill.getType().toString().toLowerCase() + ".wav");
		}
	}
	
	private void checkDeaths() {
		for (Entry<Character, Boolean> player : CanAct.entrySet()) {
			if (player.getKey().getState() == State.DEAD) {
				CanAct.replace(player.getKey(), false);
			}
		}
	}
	
	private void endAction(Character character) {
		checkDeaths();
		if (CanAct.get(character)) {
			if (character instanceof Monster) {
				monsterController.act(this);
				return;
			}
		}
		if (!CanAct.get(character) && !CanAct.get(actionController.getOpponent(character))) {
			String state = getState();
			if(state.equals("monsterDead")) {
				hero.gainExp(monster.getGive_experience());
				hero.gainGold(monster.getGive_gold());
				questController.advanceQuest(QuestTarget.KILL ,monster.getTypeMonstre());
			}
			nextTurn(state);
			for (Entry<Character, Boolean> player : CanAct.entrySet()) {
				if (player.getKey().getState() == State.ALIVE) {
					CanAct.replace(player.getKey(), true);
				}
			}
		}
	}
	
	public String getState() {
		if (hero.getState() == State.DEAD) {
			return "heroDead";
		} else if (monster.getState() == State.DEAD) {
			return "monsterDead";
		}else {
			return "continue";
		}
	}

	// private
	public void init(Hero hero,HeroController heroController,QuestComponent questController) {
		this.hero = hero;
		displayBattle.createCombatView();
		this.monster=new Monster("default",50,50,50,Monsters.RATGRIS);
		addMonsterView();
		addHeroView();
		addTitle();
		addHeroBars();
		addMonsterBars();
		addFightControllers();
		sceneController.initFightScene(displayBattle.getFightNode());
		this.questController=questController;
	}
	
	
	public void initPlayer(Hero hero, Monster monster) {
		this.monster = monster;
		this.hero = hero;
		actionController = new ActionController(hero, monster);
		CanAct.put(monster, true);
		CanAct.put(hero, true);
	}
	
	public void displayFight(Monster monster,Entity monsterEntity) {
		this.monster = monster;
		displayBattle.setMonster(monsterEntity);
		updateFightControllersAndView("begin");
		updateView();
		//addMonsterView();
		sceneController.fightSceneAction();
		actionController = new ActionController(hero, monster);
		CanAct.put(monster, true);
		CanAct.put(hero, true);
	}

	public void addTitle() {
		displayBattle.addText(new Point2D(352, 0), "Title");
	}

	public void addHeroBars() {
		displayBattle.addBar("heroLife", new Point2D(192, 185), Color.GREEN);
		displayBattle.addBar("heroMana", new Point2D(192, 205), Color.BLUE); // 185 + 20
	}

	public void addMonsterBars() {
		displayBattle.addBar("monsterLife", new Point2D(544, 32), Color.GREEN); // 64 * 8 + 32
	}

	public void updateHeroBars() {
		double percentage = ((hero.getHpMax() - hero.getHp()) * 100 / hero.getHpMax());
		displayBattle.updateBar(percentage, "red", "heroLifeRed");
		displayBattle.updateBar(percentage, "green", "heroLife");
		percentage = ((hero.getMpMax() - hero.getMp()) * 100 / hero.getMpMax());
		displayBattle.updateBar(percentage, "red", "heroManaRed");
		displayBattle.updateBar(percentage, "green", "heroMana");
	}

	public void updateMonsterBar() {
		double percentage = ((monster.getHpMax() - monster.getHp()) * 100 / monster.getHpMax());
		displayBattle.updateBar(percentage, "red", "monsterLifeRed");
		displayBattle.updateBar(percentage, "green", "monsterLife");
	}

	public void addFightControllers() {
		addFightButtons();
		addSkillMenu();
	}

	public void addFightButtons() {
		String[] listeButtonName = { "attaque", "défense", "fuir" };
		displayBattle.addButtons(listeButtonName, this);
	}

	public void addSkillMenu() {
		displayBattle.addMenu(hero.getSkills(), this);
	}

	public void addMonsterView() {
		displayBattle.addView(pathAnimationBattle + monster.getTypeMonstre() + "Combat.gif", new Point2D(544, 64),
				"monster"); // 64*8+32
	}

	public void rebootMonsterView() {
		updateMonsterView(pathAnimationBattle + monster.getTypeMonstre() + "Combat.gif");
	}

	public void updateHeroView() {
		Weapon weapon = (Weapon) hero.getEquipement().get("Arme");
		if (weapon != null) {
			displayBattle.updateView(pathAnimationBattle + "HerosCombat_" + weapon.getName() + ".gif", "hero");
		} else {
			displayBattle.updateView(pathAnimationBattle + "HerosCombat_" + "Poing.gif", "hero");
		}
	}

	public void updateMonsterView(String imagePath) {
		displayBattle.updateView(imagePath, "monster");
	}
	public void updateMonsterView() {
		updateMonsterView(pathAnimationBattle + monster.getTypeMonstre() + "Combat.gif");
	}
	public void dissapear() {
		sceneController.remove();
		rebootView();
		System.out.println("ici");
		RPGApp.mainController.playMusicMap(hero.getCurrentMap());
	}

	public void updateFightControllersAndView(String state) {
		if (state.equals("fuir")) {
			dissapear();
		} else {
			//displayBattle.updateLabelText("Title", updateText(state, monster));
			if (state.equals("begin")) {
				displayBattle.setVisibility("fuir", true);
				displayBattle.setVisibility("attaque", true);
				displayBattle.setVisibility("défense", true);
				displayBattle.setVisibilityMenu("skills", true);

				displayBattle.updateTextButton("fuir", "fuir");
				displayBattle.updateTextButton("attaque", "attaque");
				displayBattle.updateTextButton("défense", "défense");
				// ((BattleHandler) fuir.getOnAction()).update(nbTurn, "fuir");
				displayBattle.updateChoiceButton("fuir", "fuir");
				displayBattle.updateChoiceButton("attaque", "attaque");
				displayBattle.updateChoiceButton("défense", "défense");
			}
			if (state.equals("heroDead") || state.equals("monsterDead")) {
				displayBattle.setVisibilityMenu("skills", false);// ((Menu)
																	// combatView.getProperties().getObject("skills")).setVisible(false);
				displayBattle.updateLabelText("Title", updateText(state, monster));

				String nodePath = null;
				if (state.equals("heroDead")) {
					displayBattle.updateChoiceButton("attaque", "Retry");
					displayBattle.updateTextButton("défense", "Quit");
					displayBattle.updateChoiceButton("défense", "Quit");
					displayBattle.setVisibility("fuir", false);
					nodePath = "GameOver.gif";
					RPGApp.mainController.playMusic("defeat.mp3");
				} else if (state.equals("monsterDead")) {
					displayBattle.setVisibility("attaque", false);
					displayBattle.setVisibility("défense", false);
					// displayBattle.setVisibility("fuir", f);
					displayBattle.updateTextButton("fuir", "Partir");
					updateMonsterView(pathAnimationBattle+"Victory.gif");
					updateMonsterToDeathImage();
					RPGApp.mainController.playMusic("victory.mp3");
				}
			} else {
				displayBattle.updateDisabledSkills(heroController.getDisabledSkills(hero.getSkills()));
			}
		}
	}

	private void updateMonsterToDeathImage() {
		displayBattle.updateMonsterToDeathImage(monster.getDeathImage());
	}

	public void turnUpdateView() {
		updateHeroBars();
		updateMonsterBar();
	}

	public void updateView() {
		updateHeroView();
		updateMonsterView();
		updateHeroBars();
		updateMonsterBar();
	}

	public void addHeroView() {
		Item arme = hero.getEquipement().get("Arme");
		String textureHero;
		if (arme != null) {
			textureHero = pathAnimationBattle + "HerosCombat_" + arme.getName() + ".gif";
		} else {
			textureHero = pathAnimationBattle + "HerosCombat_Poing.gif";
		}
		displayBattle.addView(textureHero, new Point2D(192, 192), "hero");
	}


	public void nextTurn(String state) {
		updateFightControllersAndView(state);
		turnUpdateView();
		checkBoosts(hero);
		checkBoosts(monster);
	}

	public void checkBoosts(Character character) {
		for (Boost i : character.getBoosts()) {
			i.use();
			if (!i.getActivity()) {
				i.deleteBonus(character);
			}
		}
	}

	public void useSkill(Skill Skill, Character character, Character characterEnnemy) {
		if (Skill.getType() == SkillType.ATTACK) {
			Attack skill = (Attack) Skill;
			skill.effect(character, characterEnnemy);
		}
	}
	
	public void updateText(Skill skill, Character character) {
		String message = getMessage(skill,character);
		displayBattle.updateLabelText("Title", message);
	}
	
	public void updateText(String choice, Character character) {
		String message = getMessage(choice,character);
		displayBattle.updateLabelText("Title", message);
	}
	
	public String getMessage(String choice, Character character) {
		return character.getName() + " as fait "+choice;
	}
	
	public String getMessage(Skill skill, Character character) {
		return character.getName() + " as utilisé "+skill.getName();
	}
	
	public String updateText(String state, Monster monster) {
		String newText;
		String monsterName = monster.getName();
		if (state.equals("begin")) {
			newText = "Tu as trouvé " + monsterName + ", que veut tu faire ?";
		}
		else {
			newText = monsterName + " est encore vivant, que veut tu faire ?";
		}
		if (state.equals("monsterDead")) {
			newText = monsterName + " est mort !\nBravo tu as gagné " + monster.getGive_experience()
					+ " points d'expérience";
		}
		if (state.equals("heroDead")) {
			newText = "Tu est mort, veux tu recommencer ?";
		}
		return newText;
	}

	public void rebootView() {
		hero.resetTemporaryBonus();
		//rebootMonsterView();
	}

	public void rebootModel() {
		hero.setFullLife();
		monster.setFullLife();
	}

	public void reboot() {
		rebootModel();
		rebootView();
	}
	// TODO
//	if (monster.getState() == State.DEAD) {
//		RPGApp.hero.gainExp(monster.getGive_experience());
//		if (RPGApp.hero.getCurrentquest() != null) {
//			if (RPGApp.hero.getCurrentquest().getTypeMonstre() == monster.getTypeMonstre()) {
//				RPGApp.hero.getCurrentquest().setKill(RPGApp.hero.getCurrentquest().getKill() + 1);
//			}
//		}
//	}
}
