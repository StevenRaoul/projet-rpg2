package rpg.controler;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import com.almasb.fxgl.dsl.FXGL;

import javafx.geometry.Point2D;
import rpg.RPGApp;
import rpg.entities.character.Monsters;
import rpg.entities.character.Npc;
import rpg.entities.objects.Chest;
import rpg.entities.objects.Clue;
import rpg.entities.objects.Purchase;
import rpg.view.DisplayMap;

public class MapController {
	
	private BestiaryController bestiary;
	private DisplayMap displayMap;
	private PurchaseController purchaseController;
	private MusicController musicController = new MusicController();

	public void init(BestiaryController bestiary, PurchaseController purchaseController) {
		this.bestiary = bestiary;
		displayMap = new DisplayMap();
		this.purchaseController = purchaseController;
	}
	
	public void addMonster(String map,Monsters type, Point2D position) {
		RPGApp.listeMaps.get(map).getMonsterList().put(position, type);
	}
	
	public void addChest(String map,Chest chest, Point2D position) {
		RPGApp.listeMaps.get(map).getChestList().put(position, chest);
	}

	public void addClue(String map,Clue clue, Point2D position) {
		RPGApp.listeMaps.get(map).getClueList().put(position, clue);
	}
	
	public void spawnMonsters(String map) {
		for (Map.Entry<Point2D, Monsters> monster : RPGApp.listeMaps.get(map).getMonsterList().entrySet()) {
			displayMap.spawnMonster(bestiary.getMonster(monster.getValue()), monster.getKey());
		}
	}
	
	public void spawnMap(String map) {
		displayMap.spawnMap(map);
	}
	
	public void spawnPortals(String map) {
		for (Map.Entry<Point2D, String> portal : RPGApp.listeMaps.get(map).getPortalList().entrySet()) {
			displayMap.spawnPortal(portal.getKey());
		}
	}

	public void spawnClues(String map) {
		for (Entry<Point2D, Clue> clue : RPGApp.listeMaps.get(map).getClueList().entrySet()) {
			displayMap.spawnClue(clue.getKey());
		}
	}

	public void spawnNpcs(String map) {
		for (Entry<Point2D, Npc> npc : RPGApp.listeMaps.get(map).getPNJList().entrySet()) {
			String name = npc.getValue().getName();
			String path = ("images/entities/pnjs/monde/" + name + "/" + name + "_Face.png");
			displayMap.spawnNpc(path, npc.getKey());
		}
	}

	public void spawnChests(String map) {
		for (Map.Entry<Point2D, Chest> chest : RPGApp.listeMaps.get(map).getChestList().entrySet()) {
			String path = "images/entities/objets/Coffre_";
			if (chest.getValue().getLoot() == null) {
				path += "Ouvert.png";
			} 
			else {

				path += "Ferme.png";
			}
			displayMap.spawnChest(path, chest.getKey());
		}
	}
	
	public void spawnPurchases(String map) {
		for (Map.Entry<Point2D, Purchase> purchase : RPGApp.listeMaps.get(map).getPurchaseList().entrySet()) {
			String image = purchase.getValue().getImage();
			displayMap.spawnPurchase(image, purchase.getKey());
			purchaseController.setMap(map);
			purchaseController.createPurchase(purchase.getKey());
		}
	}
	
	public HashMap<Point2D, Purchase> getPurchases(String map) {
		return RPGApp.listeMaps.get(map).getPurchaseList();
	}

	public void changeMap(String oldMap, Point2D position) {
		String newMap = RPGApp.listeMaps.get(oldMap).getPortalList().get(position);
		chargeMapProgress(newMap, position, oldMap);
		RPGApp.mainController.playMusicMap(newMap);
	}

	public void chargeMapInit(String map) {
		spawnMap(map);
		chargeMap(map, RPGApp.hero.getPosition());
		if (RPGApp.player.getPosition() != RPGApp.hero.getPosition()) {
			RPGApp.player.setPosition(RPGApp.hero.getPosition());
		}
	}

	public void chargeMapProgress(String newMap, Point2D position, String oldMap) {
		Point2D newPosition = RPGApp.listeMaps.get(newMap).getReturnPortalList().get(oldMap + position.toString());
		FXGL.setLevelFromMap(newMap);
		chargeMap(newMap, newPosition);
		RPGApp.player.setPosition(newPosition);
	}

	public void chargeMap(String map, Point2D position) {
		RPGApp.listeMaps.get(map).monsterMove = true;
		RPGApp.listeMaps.get(map).notif = null;
		RPGApp.hero.setCurrentMap(map);
		if (RPGApp.listeMaps.get(map) != null) {
			spawnPortals(map);
			spawnMonsters(map);
			spawnNpcs(map);
			spawnChests(map);
			spawnClues(map);
			spawnPurchases(map);
		}
	}
}