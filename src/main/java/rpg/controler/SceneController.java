package rpg.controler;

import static com.almasb.fxgl.dsl.FXGL.getAppHeight;
import static com.almasb.fxgl.dsl.FXGL.getGameScene;

import com.almasb.fxgl.animation.AnimatedValue;
import com.almasb.fxgl.animation.Animation;
import com.almasb.fxgl.animation.Interpolators;
import com.almasb.fxgl.dsl.FXGL;

import javafx.scene.Node;
import javafx.util.Duration;
import rpg.scenes.FightScene;

public class SceneController {
	private Animation<Double> cameraAnimation;
	private FightScene fightScene;
	
	private Animation<Double> getAnimateCamera() {
        AnimatedValue<Double> value = new AnimatedValue<>(getAppHeight() * 1.0, 0.0);
        cameraAnimation = FXGL.animationBuilder()
                .duration(Duration.seconds(0.5))
                .interpolator(Interpolators.EXPONENTIAL.EASE_OUT())
                .animate(value)
                .onProgress(y -> getGameScene().getViewport().setY(y))
                .build();
        return cameraAnimation;
    }
	
	public void initFightScene(Node node) {
		fightScene = new FightScene(node);
	}
	
	public void remove() {
		fightScene.remove();
	}
	
	public void affiche() {
		Animation<Double> camera = getAnimateCamera();
		FXGL.getSceneService().popSubScene();
		camera.start();
	}
	public void fightSceneAction() {
		System.out.println("AfficheScene");
		AnimatedValue<Double> value = new AnimatedValue<>(getAppHeight() * 1.0, 0.0);
        cameraAnimation = FXGL.animationBuilder()
                .duration(Duration.seconds(0.5))
                .interpolator(Interpolators.EXPONENTIAL.EASE_OUT())
                .animate(value)
                .onProgress(y -> getGameScene().getViewport().setY(y))
                .build();
        FXGL.getSceneService().pushSubScene(fightScene);
        cameraAnimation.start();
	}
}