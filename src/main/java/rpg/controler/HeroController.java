package rpg.controler;

import java.util.ArrayList;

import javafx.scene.Node;
import rpg.RPGApp;
import rpg.entities.abilities.Skill;
import rpg.view.DisplayHero;

public class HeroController {
	private DisplayHero displayHero = new DisplayHero(); 
	
	
	public ArrayList<String> getDisabledSkills(ArrayList<Skill> skillListe) {
		ArrayList<String> disabledItems = new ArrayList<String>();
		for (Skill skill : skillListe) {
			if (skill.getCost() > RPGApp.hero.getMp()) {
				disabledItems.add(skill.getName() + " " + skill.getCost());
			}
		}
		return disabledItems;
	}
	public Node getInitImage() {
		return displayHero.getInitImage();
	}
}
