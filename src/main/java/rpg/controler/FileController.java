package rpg.controler;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;

import org.json.simple.JSONObject;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import rpg.system.Quest;
import rpg.system.QuestTarget;

public class FileController {

	public ArrayList<File> getQuestFiles() {
		return getFiles("./src/main/resources/assets/quest");
	}

	public ArrayList<File> getFiles(String Path) {
		boolean verif = true;
		File dir = new File(Path);
		File[] liste = dir.listFiles();
		ArrayList<File> files = new ArrayList<>();
		for (File item : liste) {
			if (!item.isFile()) {
				verif = false;
			} else {
				files.add(item);
			}
		}
		if (verif) {
			return files;
		}
		return null;
	}

	public ArrayList<LinkedHashMap> getNpcs(String file) {
		return getFile(file, "npcs");
	}

	public ArrayList<LinkedHashMap> getQuest(String file) {
		return getFile(file, "");
	}

	public HashMap<String, HashMap<String, String[]>> getNpcChat(String nameNpc, int stepQuest, int ID) {
		HashMap<String, HashMap<String, String[]>> dialog = new HashMap<>();
		ArrayList<LinkedHashMap> dialogue = getFile(
				"./src/main/resources/assets/dialogues/" + nameNpc + "/dialogue-" + ID + "-" + stepQuest + ".json",
				"chat");
		System.out.println("./src/main/resources/assets/" + nameNpc + "/dialogue-" + ID + "-" + stepQuest + ".json");
		for (LinkedHashMap dialogs : dialogue) {
			HashMap<String, String[]> text = new HashMap<>();
			text.put("answers",
					(
							convertToArray(((ArrayList<String>) dialogs.get("answers")))
					));
			String[] message = new String[1];
			message[0]=(String) ((LinkedHashMap) dialogs.get("question")).get("text");
			text.put("message",message);
			dialog.put((String) ((LinkedHashMap) dialogs.get("question")).get("from"), text);
		}
		return dialog;
	}
	public String[] convertToArray(ArrayList<String> toConvert) {
		String[] liste = new String[toConvert.size()];
		return toConvert.toArray(liste);
	}

	public Quest getQuest(JSONObject jso) {
		Quest q = new Quest();
		q.setDescription((String) jso.get("description"));
		q.setName((String) jso.get("name"));
		q.setReward((int) jso.get("reward"));
		q.setNbTarget(0);
		q.setTypeTarget(QuestTarget.valueOf((String) ((LinkedHashMap) jso.get("goal")).get("type")));
		q.setNbTargetMax((int) ((LinkedHashMap) jso.get("goal")).get("number"));
		q.setSpawns((LinkedHashMap) jso.get("spawns"));
		q.setId((int) jso.get("quest"));
		q.setStep((int) jso.get("step"));
		q.setTargetToSee((String) ((LinkedHashMap) jso.get("goal")).get("target"));
		for (LinkedHashMap name : ((ArrayList<LinkedHashMap>) jso.get("dialogs"))) {
			q.addNPC((String) name.get("npc"));
		}
		return q;

	}

	public JSONObject getQuest(File file) {
		ObjectMapper mapper = new ObjectMapper();
		try {
			return mapper.readValue(new FileReader(file), JSONObject.class);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public ArrayList<LinkedHashMap> getFile(String file, String target) {
		ObjectMapper mapper = new ObjectMapper();
		try {
			JSONObject liste = mapper.readValue(new FileReader(file), JSONObject.class);
			if (target.equals("")) {
				return (ArrayList<LinkedHashMap>) (Object) liste;
			}
			return (ArrayList<LinkedHashMap>) liste.get(target);
		} catch (JsonParseException e1) {
			e1.printStackTrace();
		} catch (JsonMappingException e1) {
			e1.printStackTrace();
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		return null;
	}
}
