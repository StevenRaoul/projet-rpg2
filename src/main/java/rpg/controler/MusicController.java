package rpg.controler;

import java.util.Random;

import com.almasb.fxgl.audio.Music;
import com.almasb.fxgl.dsl.FXGL;

import rpg.entities.character.Monster;
import rpg.entities.character.Monsters;

public class MusicController {
	
	private String currentMusicName;
	private Music currentMusic;
	private Music previousMusic;
	
	public String getCurrentMusicName() {
		return currentMusicName;
	}

	public void setCurrentMusicName(String currentMusicName) {
		this.currentMusicName = currentMusicName;
	}

	public Music getCurrentMusic() {
		return currentMusic;
	}
	
	public Music getPreviousMusic() {
		return previousMusic;
	}
	
	public void setCurrentMusic(Music music) {
		currentMusic = music;
	}
	
	public void setPreviousMusic(Music music) {
		previousMusic = music;
	}
	
	public String getMusicFileMap(String music) {
		String[] musicFile = music.split("RPG");
		return musicFile[0] + ".mp3";
	}

	public String getMusicFileFightBoss(String monster) {
		String musicFile = "battle_" + monster.toLowerCase() + ".mp3";
		return musicFile;
	}
	
	public String getMusicFileRandom() {
		Random random = new Random();
		int randomNumber = 1 + random.nextInt(3);
		String musicFile = "battle" + randomNumber + ".mp3";
		return musicFile;
	}
	
	public void musicPlay(String music) {
		if (currentMusic != null) {	
			setPreviousMusic(currentMusic);
			stopMusic();
		}
		Music musicPlay = FXGL.getAssetLoader().loadMusic(music);
		FXGL.getAudioPlayer().loopMusic(musicPlay);
		setCurrentMusic(musicPlay);
		setCurrentMusicName(music);
	}
	
	public void musicPlayMap(String music) {
		System.out.println(getMusicFileMap(music));
		System.out.println(getCurrentMusicName());
		System.out.println(!getMusicFileMap(music).equals(getCurrentMusicName()));
		if (!getMusicFileMap(music).equals(getCurrentMusicName())) {
			musicPlay(getMusicFileMap(music));
		}
	}
	
	public void musicPlayBattle(Monster monster) {
		String music;
		if (monster.getTypeMonstre() == Monsters.BOSSRAT || monster.getTypeMonstre() == Monsters.BOSSRATROI) {
			music = getMusicFileFightBoss(monster.getTypeMonstre().toString());
		}
		else {
			music = getMusicFileRandom();
		}
		musicPlay(music);
	}
	
	public void musicReplay() {
		if (previousMusic != null) {
			if (currentMusic != null) {
				stopMusic();
			}
			FXGL.getAudioPlayer().loopMusic(previousMusic);
			setCurrentMusic(previousMusic);
			setPreviousMusic(null);
		}
	}
	
	public void stopMusic() {
		FXGL.getAudioPlayer().stopMusic(currentMusic);
		setCurrentMusic(null);
	}
	
	public void soundPlay(String sound) {
		FXGL.getAudioPlayer().playSound(FXGL.getAssetLoader().loadSound(sound));
	}
	
	public void multipleSoundPlay(String soundString) throws InterruptedException {
		String[] sounds = soundString.split(",");
		for (String sound : sounds) {
			soundPlay(sound);
			FXGL.getAudioPlayer().wait(500);
		}
	}
}