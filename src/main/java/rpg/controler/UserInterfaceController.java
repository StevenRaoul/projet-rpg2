package rpg.controler;

import java.util.ArrayList;

import javafx.scene.Node;
import rpg.entities.character.Hero;
import rpg.entities.items.Item;
import rpg.view.CustomWindow;
import rpg.view.DisplayUserInterface;

public class UserInterfaceController {
	
	private DisplayUserInterface displayUserInterface = new DisplayUserInterface();
	private ArrayList<String> UINodes = new ArrayList<>();
	private InventoryController inventoryController = new InventoryController();
	private EquipmentController equipmentController = new EquipmentController();
	private Hero hero;
	
	public void addUINode(String property, Node node) {
		displayUserInterface.addUINode(node);
		UINodes.add(property);
	}
	
	public void init(Hero hero) {
		this.hero = hero;
		inventoryController.Init(hero, this);
		equipmentController.Init(hero, this);
	}
	
	public Node getUINode(String property) {
		return displayUserInterface.getUINode(UINodes.indexOf(property));
	}
	
	public void changeUINodeVisibility(String property) {
		displayUserInterface.changeVisibilityUINode(UINodes.indexOf(property));
	}
	
	public Node getInventory() {
		return inventoryController.createInventory();
	}
	
	public Node getEquipment() {
		return equipmentController.createEquipment();
	}
	
	public void updateEquipment(String action, Item item) {
		equipmentController.updateEquipment(action, item);
	}
	
	public void updateInventory(String action, Item item, int index) {
		inventoryController.updateInventory(action, item, index);
	}
	
	public void chestHandle(String action, Item item) {
		updateInventory(action, item, hero.getPositionVoid());
	}
	
	public void initEquipmentHandlers(Node view, CustomWindow inventorySquare, String type) {
		equipmentController.initHandlers(view, inventorySquare, type);
	}
	
	public void initInventoryHandlers(Node view, CustomWindow inventorySquare, int pos) {
		inventoryController.initHandlers(view, inventorySquare, pos);
	}
}