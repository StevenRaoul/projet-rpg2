package rpg.controler;

import java.util.HashMap;
import java.util.Map.Entry;

import com.almasb.fxgl.entity.Entity;

import javafx.geometry.Point2D;
import javafx.scene.Node;
import rpg.RPGApp;
import rpg.entities.character.Hero;
import rpg.entities.items.Equipment;
import rpg.entities.items.Item;
import rpg.eventHandler.EquipmentHandler;
import rpg.eventHandler.ItemHandler;
import rpg.view.CustomWindow;
import rpg.view.DisplayEquipment;

public class EquipmentController {
	private DisplayEquipment displayEquipment = new DisplayEquipment();
	private UserInterfaceController userInterfaceController;
	private Hero hero;

	public void Init(Hero hero,UserInterfaceController userInterfaceController) {
		this.userInterfaceController = userInterfaceController;
		this.hero = hero;
	}
	
	public Node createEquipment() {
		Entity equipmentTab = displayEquipment.createEquipment().getEntity();
		HashMap<String, Equipment> heroEquipment = RPGApp.hero.getEquipement();
		HashMap<String, Point2D> equipmentList = new HashMap<>();
		equipmentList.put("Arme", new Point2D(176, 144));	//2*64+48 & 64+80
		equipmentList.put("Armure", new Point2D(104, 120));	//64+40 & 64+56
		for (Entry<String, Point2D> equipment : equipmentList.entrySet()) {
			CustomWindow itemSquare = displayEquipment.createEquipmentSquare(equipment.getValue(),equipment.getKey());
			if (heroEquipment.get(equipment.getKey()) != null) {		
				displayEquipment.initItemViewInventory(heroEquipment.get(equipment.getKey()), itemSquare, "desequip");
				initHandlers(displayEquipment.getBigView(heroEquipment.get(equipment.getKey())), itemSquare, equipment.getKey());
			}
			displayEquipment.addEquipmentSquare(equipmentTab, itemSquare,equipment.getKey());
		}
		return equipmentTab.getViewComponent().getParent();
	}
	public void updateEquipment(String action, Item item) {
		if (action.equals("ajout")) {
			ajoutItem(item);
		} 
		else if (action.equals("remove")) {
			
			removeItem(item);
		}
	}
	public void removeItem(Item item) {
		CustomWindow itemSquare = (CustomWindow) displayEquipment.getEquipment().get(item.getType());
		displayEquipment.removeItemEquipment(itemSquare);
		((EquipmentHandler) itemSquare.getOnMouseClicked()).setState(false);
		((ItemHandler) itemSquare.getOnMouseEntered()).setState(false);
						
						
					
	}
	
	public void ajoutItem(Item item) {
//		for (Entry<Object, Object> entry : displayEquipment.getEquipment().entrySet()) {
//			if (entry.getValue().getClass() == CustomWindow.class) {
//				CustomWindow itemSquare = (CustomWindow) entry.getValue();
//				if (itemSquare.getAccessibleText() != null) {
//					if (itemSquare.getAccessibleText().equals(item.getType())) {
						CustomWindow itemSquare = (CustomWindow) displayEquipment.getEquipment().get(item.getType());
						displayEquipment.initItemViewInventory(item, itemSquare, "desequip");
						userInterfaceController.initEquipmentHandlers(displayEquipment.getBigView(item),itemSquare,item.getType());
						
					//}}}}
	}
	public void handleUnequip(String type) {
		Equipment item = getEquipment(type);
		item.setPosition(hero.getPositionVoid());
		int pos = hero.getPositionVoid();
		hero.unequip(item);
		userInterfaceController.updateInventory("ajout", item, pos);
		userInterfaceController.updateEquipment("remove", item);
	}
	
	public Equipment getEquipment(String type) {
		return hero.getEquipement().get(type);
	}
	
	public void initHandlers(Node view, CustomWindow inventorySquare, String type) {
		((EquipmentHandler) inventorySquare.getOnMouseClicked()).init(view, type,this);
		((ItemHandler) inventorySquare.getOnMouseEntered()).init(type);
	}
}
