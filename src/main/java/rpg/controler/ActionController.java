package rpg.controler;

import java.util.HashMap;

import rpg.RPGApp;
import rpg.entities.abilities.Attack;
import rpg.entities.abilities.Skill;
import rpg.entities.abilities.SkillType;
import rpg.entities.character.Character;
import rpg.entities.character.Hero;
import rpg.system.Fights;

public class ActionController {
	
	private HashMap<Character, Character> fighters = new HashMap<>();
	private Fights fights = new Fights();
	
	public ActionController(Character characterA, Character characterB) {
		fighters.put(characterA, characterB);
		fighters.put(characterB, characterA);
	}
	
	public ActionController() {}
	
	public void skillAction(Skill skill, Character character) {
		useSkill(skill, character);
	}
	
	public void useSkill(Skill skill, Character character) {
		if (skill.getType() == SkillType.ATTACK) {
			((Attack) skill).effect(character, fighters.get(character));
		}
		else {
			skill.effect(character);
		}
	}
	
	public void basicAction(String choice, Character character) {
		if (choice.equals("défense") || choice.equals("attaque")) {
			try {
				fights.battle(character, fighters.get(character), choice);
				if (choice.equals("attaque")) {
					if (character instanceof Hero) {
						if (((Hero) character).getEquipement().get("Arme") != null) {
							RPGApp.mainController.playSound(((Hero) character).getEquipement().get("Arme").getName() + "_basic.mp3");
						}
						else {
							RPGApp.mainController.playSound("attack.mp3");
						}
					}
					else {
						RPGApp.mainController.playSound("claws.mp3");
					}
				}
				else {
					RPGApp.mainController.playSound("defense.mp3");
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	public Character getOpponent(Character character) {
		return fighters.get(character);
	}
}

