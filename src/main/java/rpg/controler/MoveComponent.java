package rpg.controler;

import static com.almasb.fxgl.dsl.FXGL.getGameScene;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;

import com.almasb.fxgl.dsl.FXGL;
import com.almasb.fxgl.entity.component.Component;
import com.almasb.fxgl.entity.components.BoundingBoxComponent;
import com.almasb.fxgl.physics.BoundingShape;
import com.almasb.fxgl.physics.HitBox;
import com.almasb.fxgl.texture.Texture;

import javafx.geometry.Point2D;
import rpg.RPGApp;
import rpg.view.DisplayBasic;

public class MoveComponent extends Component {
	
	private HashMap<HitBox, MoveDirection> listOfHitBox = new HashMap<>();
	private MoveDirection lastDirection;
	private HashMap<MoveDirection, Boolean> canMove = new HashMap<>();
	private HashMap<MoveDirection, MoveDirection> opposite = new HashMap<>();
	private HashMap<MoveDirection, ArrayList<BoundingBoxComponent>> collisionsByDirection = new HashMap<>();

	public void clearHitBoxes() {
		RPGApp.player.getBoundingBoxComponent().clearHitBoxes();
	}

	public void initHitBoxes() {
		for (Entry<HitBox, MoveDirection> box : listOfHitBox.entrySet()) {
			RPGApp.player.getBoundingBoxComponent().addHitBox(box.getKey());
		}
	}

	public void AutorizeDirection(MoveDirection direction) {
		canMove.put(direction, true);
	}

	public void addBlocCollision(BoundingBoxComponent boundingBoxComponent, HitBox hitBox) {
		MoveDirection direction = listOfHitBox.get(hitBox);
		collisionsByDirection.get(direction).add(boundingBoxComponent);
		ForbidDirection(direction);
	}
	
	public void hyperClean() {
		for (MoveDirection direction : MoveDirection.values()) {
			collisionsByDirection.get(direction).clear();
			AutorizeDirection(direction);
		}
	}
	
	private void clean() {
		for (Entry<MoveDirection, ArrayList<BoundingBoxComponent>> collisions : collisionsByDirection.entrySet()) {
			if (collisions.getKey() != lastDirection) {
				for (int i = 0; i < collisions.getValue().size(); i++) {
					BoundingBoxComponent box = collisions.getValue().get(i);
					if (!RPGApp.player.getBoundingBoxComponent().isCollidingWith(box)) {
						collisions.getValue().remove(box);
					}
				}
				if (collisionsByDirection.get(collisions.getKey()).isEmpty()) {
					AutorizeDirection(collisions.getKey());
				}
			}
		}
	}

	public void removeBlocCollision() {
		clean();
	}

	public void ForbidDirection(MoveDirection direction) {
		canMove.put(direction, false);
	}

	public MoveDirection getLastDirection() {
		return lastDirection;
	}

	public MoveComponent() {
		init();
	}

	public void init() {
		for (MoveDirection direction : MoveDirection.values()) {
			canMove.put(direction, true);
			collisionsByDirection.put(direction, new ArrayList<>());
		}
		listOfHitBox.put(new HitBox("left", new Point2D(-5, 0), BoundingShape.box(5, 50)), MoveDirection.LEFT);
		listOfHitBox.put(new HitBox("right", new Point2D(50, 0), BoundingShape.box(5, 50)), MoveDirection.RIGHT);
		listOfHitBox.put(new HitBox("up", new Point2D(0, -5), BoundingShape.box(50, 5)), MoveDirection.UP);
		listOfHitBox.put(new HitBox("down", new Point2D(0, 60), BoundingShape.box(50, 5)), MoveDirection.DOWN);
		opposite.put(MoveDirection.RIGHT, MoveDirection.LEFT);
		opposite.put(MoveDirection.LEFT, MoveDirection.RIGHT);
		opposite.put(MoveDirection.UP, MoveDirection.DOWN);
		opposite.put(MoveDirection.DOWN, MoveDirection.UP);
		clearHitBoxes();
		initHitBoxes();
	}

	private boolean checkCanMove(MoveDirection direction) {
		if (canMove.get(direction) && canMove.get(opposite.get(direction)) == false) {
			AutorizeDirection(opposite.get(direction));
		}
		return canMove.get(direction);
	}

	public void move(String direction) {
		switch (direction) {
		case "left":
			if (checkCanMove(MoveDirection.LEFT)) {
				RPGApp.player.translateX(-5); // move left 5 pixels
				lastDirection = MoveDirection.LEFT;
				((Texture) getGameScene().getUINodes().get(0)).set(FXGL.texture("animations/monde/HerosGaucheMV.gif"));
			}
			break;
		case "right":
			if (checkCanMove(MoveDirection.RIGHT)) {
				RPGApp.player.translateX(5); // move left 5 pixels
				lastDirection = MoveDirection.RIGHT;
				((Texture) getGameScene().getUINodes().get(0)).set(FXGL.texture("animations/monde/HerosDroiteMV.gif"));
			}
			break;
		case "up":
			if (checkCanMove(MoveDirection.UP)) {
				RPGApp.player.translateY(-5); // move left 5 pixels
				lastDirection = MoveDirection.UP;
				((Texture) getGameScene().getUINodes().get(0)).set(FXGL.texture("animations/monde/HerosDosMV.gif"));
			}
			break;
		case "down":
			if (checkCanMove(MoveDirection.DOWN)) {
				RPGApp.player.translateY(5); // move left 5 pixels
			lastDirection = MoveDirection.DOWN;
				((Texture) getGameScene().getUINodes().get(0)).set(FXGL.texture("animations/monde/HerosFaceMV.gif"));
			}
			break;
		default:
			DisplayBasic.updateNotif("erreur");
		}
	}
}