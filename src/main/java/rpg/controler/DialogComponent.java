package rpg.controler;

import java.io.FileReader;
import java.util.HashMap;
import java.util.Iterator;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.almasb.fxgl.entity.component.Component;

import javafx.geometry.Point2D;
import rpg.RPGApp;
import rpg.entities.character.Npc;


public class DialogComponent extends Component {
	
	public static void setDialog(String map, Point2D position, String file) {
		HashMap<String,HashMap<String,String[]>> dialog = new HashMap<String,HashMap<String,String[]>>();
		JSONParser parser = new JSONParser();
		try {
			Object obj = parser.parse(new FileReader(file));
			JSONObject jsonObject = (JSONObject) obj;
			JSONObject question;
			JSONArray answer;
			String[] answerString;
			JSONObject iteratorNext;
			Npc pnj = RPGApp.listeMaps.get(map).getPNJList().get(position);
			JSONArray chats = (JSONArray) jsonObject.get("chat");
			Iterator<JSONObject> iterator = chats.iterator();
			String step = "";
			
			while (iterator.hasNext()) {
				//Récupération des questions et réponses
				iteratorNext = iterator.next();
				question = (JSONObject) iteratorNext.get("question");
				answer = (JSONArray) iteratorNext.get("answers");
				
				// Transformation du JSONArray answer en String[] answerString
				answerString = new String[answer.size()];
				for (int i = 0; i < answer.size(); i++) {
					answerString[i] = answer.get(i).toString();
				}
				
				//On cherche si la question vient d'une réponse du personnage ou non
				if(question.get("type").equals("")) {
					step = "from";
				}
				else {
					step = "type";
				}
				
				//On ajoute le nouveau dialogue au personnage
				dialog.put(
						(String) question.get(step),
						RPGApp.Chat(
								answerString, 
								(String) question.get("text")));
				pnj.setListChat(dialog);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
