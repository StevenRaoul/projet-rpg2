package rpg.controler;

import com.almasb.fxgl.dsl.FXGL;
import com.almasb.fxgl.entity.Entity;

import javafx.geometry.Point2D;
import javafx.scene.Node;
import javafx.scene.control.Button;
import rpg.RPGApp;
import rpg.entities.items.Item;
import rpg.entities.objects.Chest;
import rpg.eventHandler.ChestHandler;
import rpg.view.DisplayChest;

public class ChestController {
	private DisplayChest displayChest = new DisplayChest();
	private UserInterfaceController userInterfaceController;
	
	public void init(UserInterfaceController userInterfaceController) {
		this.userInterfaceController = userInterfaceController;
	}
	
	public void findChest(Point2D newPosition) {
		Chest chest = RPGApp.listeMaps.get(RPGApp.hero.getCurrentMap()).getChestList().get(newPosition);
		if (chest.getLoot() != null) {
			Entity item = FXGL.entityBuilder()
					.view(chest.getLoot().getImage())
					.build();
			Node itemImage  = item.getViewComponent().getChildren().get(0);
			openChest(itemImage,chest,newPosition);
			//DisplayChest.openChest(itemImage, chest, newPosition);
		}
		else {
			displayChest.showMessage();
		}
	}
	
	public void openChest(Node itemImage,Chest chest,Point2D position) {
		Button[] buttons = new Button[2];
		buttons[0] = new Button("non");
		buttons[1] = new Button("oui");
		ChestHandler ch = new ChestHandler(chest, position);
		ch.init(this);
		buttons[1].setOnAction(ch);
		displayChest.showBox(itemImage, chest.getLoot().getName(), buttons);
	}
	public void handle(Item item) {
		userInterfaceController.chestHandle("ajout", item);
	}
}
