package rpg.controler;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;

import org.json.simple.JSONObject;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import rpg.entities.abilities.Attack;
import rpg.entities.abilities.Boost;
import rpg.entities.abilities.Heal;
import rpg.entities.abilities.Skill;
import rpg.entities.character.Monster;
import rpg.entities.character.Monsters;

public class BestiaryController {

	private HashMap<Monsters, Monster> bestiary = new HashMap<>();

	public Monster getMonster(Monsters type) {
		return bestiary.get(type).copy();
	}

	public HashMap<Monsters, Monster> getBestiary() {
		return bestiary;
	}
	
	public void loadBestiary() throws Exception {
		loadBestiary("src/main/resources/assets/bestiary/Bestiary.json");
	}
	
	public void loadBestiary(String file) throws Exception{
		ArrayList<LinkedHashMap> ListeMonstresFromFile = null;
		boolean error = false;
		try {
			ListeMonstresFromFile = getBestiaryFile(file);
		} catch (Exception e) {
			error = true;
			e.printStackTrace();
			throw e;
		}
		if (!error) {
		HashMap<Integer,Monster> ListeMonstres = new HashMap<>();
		for (LinkedHashMap monsterData : ListeMonstresFromFile) {
			Monster monster =  new Monster(
					(int) monsterData.get("atk"),
					(int) monsterData.get("def"),
					(int) monsterData.get("hp"),
					(int) monsterData.get("mp"),
					(int) monsterData.get("give_experience"),
					(int) monsterData.get("give_gold"),
					Monsters.valueOf((String)monsterData.get("type"))
					);
			for(LinkedHashMap skillsData : (ArrayList<LinkedHashMap>)monsterData.get("skills")) {
				Skill skill;
				switch ((String) skillsData.get("type")) {
				
				case "Attack":
					skill = new Attack(
							(String) skillsData.get("name"),
							(int) skillsData.get("cost"),
							
							(int) skillsData.get("pourcentage")
							);
					break;
				case "Boost":
					skill = new Boost(
							(String) skillsData.get("name"),
							(int) skillsData.get("cost"),
							(int) skillsData.get("pourcentage"),
							(String) skillsData.get("stat"),
							(int) skillsData.get("nbTurn")
							);
					break;
				case "Heal":
					skill = new Heal(
							(String) skillsData.get("name"),
							(int) skillsData.get("cost"),
							(int) skillsData.get("pourcentage")
							);
					break;
				default:
					skill=null;
					System.out.println("erreur !!!!");
					break;
				}
				monster.addSkill(skill);
			}
			bestiary.put(Monsters.valueOf((String)monsterData.get("type")),monster);
		}
		}
	}
	
	public ArrayList<LinkedHashMap> getBestiaryFile(String file) throws Exception {
		ObjectMapper mapper = new ObjectMapper();
		try {
			JSONObject liste = mapper.readValue(new FileReader(file), JSONObject.class);
			return (ArrayList<LinkedHashMap>) liste.get("monsters");
		} catch (JsonParseException e1) {
			e1.printStackTrace();
			throw new Exception("erreur de parsing du fichier");
		} catch (JsonMappingException e1) {
			e1.printStackTrace();
			throw new Exception("erreur entre l'attendu et le fichier");
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
			throw new Exception("file not found");
		} catch (IOException e1) {
			e1.printStackTrace();
			throw new Exception("no idea");
		}
	}
}

