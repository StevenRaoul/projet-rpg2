package rpg.controler;

import com.almasb.fxgl.entity.Entity;

import javafx.scene.Node;
import javafx.scene.Parent;
import rpg.EntityType;
import rpg.RPGApp;
import rpg.entities.character.Hero;
import rpg.entities.items.Consumable;
import rpg.entities.items.Equipment;
import rpg.entities.items.Item;
import rpg.eventHandler.InventoryHandler;
import rpg.eventHandler.ItemHandler;
import rpg.view.CustomWindow;
import rpg.view.DisplayInventory;

public class InventoryController {
	private Hero hero;
	private DisplayInventory displayInventory = new DisplayInventory() ; 
	private UserInterfaceController userInterfaceController;

	public void Init(Hero hero, UserInterfaceController userInterfaceController) {
		this.hero = hero;
		this.userInterfaceController = userInterfaceController;
	}
	
	public Item getItem(int pos) {
		return hero.getInventory()[pos];
	}
	
	public String getInventoryMessage(int pos) {
		String message = "";
		if (hero.getInventory()[pos] instanceof Consumable) {
			message = " dans ton inventaire veux tu l'utiliser ?";
		} 
		else {
			message = " dans ton inventaire veux tu l'équiper ?";
		}
		return message;
	}
	
	public Parent createInventory() {
		Entity inventory = displayInventory.createInventory().getEntity();
		inventory.setType(EntityType.INVENTORY);
		int lineSquare = 0;
		int columnSquare = 0;
		for (Item item : hero.getInventory()) {
			if (lineSquare >= 4) {
				lineSquare = 0;
				columnSquare = columnSquare + 1;
			}
			CustomWindow inventorySquare = displayInventory.createInventorySquare(lineSquare, columnSquare);
			if(item!=null) {
				displayInventory.initItemViewInventory(item, inventorySquare, "equip");
				userInterfaceController.initInventoryHandlers(displayInventory.getBigView(item),inventorySquare,item.getPosition());
			}
			displayInventory.addInventorySquare(inventorySquare,inventory,lineSquare+columnSquare*4);
			lineSquare++;
		}
		return inventory.getViewComponent().getParent();
	}
	
	public void initHandlers(Node view, CustomWindow inventorySquare, int pos) {
		((InventoryHandler) inventorySquare.getOnMouseClicked()).init(view, pos,this);
		((ItemHandler) inventorySquare.getOnMouseEntered()).init(pos);
	}
	
	public void updateInventory(String action, Item item, int index) {
		if (action.equals("ajout")) {
			hero.addItemInventory(item);
			displayInventory.addItem(item, index);
			initHandlers(displayInventory.getBigView(item), (CustomWindow) displayInventory.getInventory().get(index), index);
		} 
		else if (action.equals("remove")) {
			removeItem(index);
		}
	}
	
	public void removeItem(int index) {
		hero.removeItemInventory(index);
		disableItemHandler(index);
		displayInventory.removeItem(index);
	}
	
	public void disableItemHandler(int index) {
		CustomWindow inventorySquare = ((CustomWindow) displayInventory.getInventory().get(index));
		CustomWindow itemSquare = ((CustomWindow) inventorySquare.getProperties().get("window"));
		((InventoryHandler) itemSquare.getOnMouseClicked()).setState(false);
		((ItemHandler) itemSquare.getOnMouseEntered()).setState(false);
	} 
	
	public void handleItem(int pos) {
		Item item = hero.getInventory()[pos];
		if (item instanceof Consumable) {
			Consumable Consommable = (Consumable) item;
			Consommable.effect();
			RPGApp.mainController.playSound("drink.mp3");
			hero.removeItemInventory(Consommable);
			updateInventory("remove", Consommable, Consommable.getPosition());
		} 
		else {
			Equipment equipment = (Equipment) item;
			if (hero.getEquipement().get(equipment.getType()) != null) {
				updateInventory("remove", equipment, pos);
				hero.getEquipement().get(equipment.getType()).setPosition(pos);
				updateInventory("ajout", hero.getEquipement().get(item.getType()), pos);
				userInterfaceController.updateEquipment("remove", hero.getEquipement().get(item.getType()));
				userInterfaceController.updateEquipment("ajout", equipment);
				hero.removeItemInventory(equipment);
				hero.equip(equipment);
				RPGApp.mainController.playSound("equip.mp3");
			} 
			else {
				updateInventory("remove", equipment, pos);
				userInterfaceController.updateEquipment("ajout", equipment);
				hero.removeItemInventory(equipment);
				hero.equip(equipment);
				RPGApp.mainController.playSound("equip.mp3");
			}
		}
	}
}
