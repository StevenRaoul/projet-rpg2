package rpg.controler;

import java.util.ArrayList;

import rpg.entities.abilities.Skill;
import rpg.entities.abilities.SkillType;

public class MonsterController {
	private ArrayList<SkillType> priorityOrder= new ArrayList<>();
	
	public MonsterController() {
		//priorityOrder.add(SkillType.BOOST);
		priorityOrder.add(SkillType.ATTACK);
	}
	
	public String act(FightController fightController) {	
		if (fightController.getMonster().getPercentage("hp") <= 25) {
			Skill skill = haveSkillType(
					fightController.getMonster().getSkills(),
					SkillType.HEAL
					);
			if (skill != null) {
				useSkill(skill, fightController);
				return "Skill " + skill.getName();
			}
		}
		if (fightController.getMonster().getPercentage("hp") <= 50) {
			Skill skill = null;
			int order = 0;
			while (skill == null && order < priorityOrder.size()) {
				skill = haveSkillType(
						fightController.getMonster().getSkills(),
						priorityOrder.get(order));
				order++;
			}
			if (skill != null) {
				useSkill(skill,fightController);
				return "Skill " + skill.getName();
			}
		}
		fightController.manage("attaque", fightController.getMonster());
		return "Basic Attack";
	}
	
	public Skill haveSkillType(ArrayList<Skill> skills, SkillType type) {
		if (skills.size() != 0) {
			ArrayList<Skill> possibility = new ArrayList<>();
			for (Skill skill : skills) {
				if (skill.getType() == type) {
					possibility.add(skill);
				}
			}
			if (possibility.size() != 0) {
				int random = (int) Math.floor(Math.random() * (possibility.size()-1));
				return possibility.get(random);
			}
			else {
				return null;
			}
		}
		else {
			return null;
		}
	}
	
	public void useSkill(Skill skill, FightController fightController) {
		fightController.manage(skill, fightController.getMonster());
	}
}
