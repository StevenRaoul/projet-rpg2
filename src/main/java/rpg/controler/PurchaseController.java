package rpg.controler;

import java.util.HashMap;

import com.almasb.fxgl.dsl.FXGL;

import javafx.geometry.Point2D;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import rpg.RPGApp;
import rpg.entities.character.Hero;
import rpg.entities.objects.Purchase;
import rpg.view.DisplayBasic;
import rpg.view.DisplayPurchase;

public class PurchaseController {
	
	private DisplayPurchase displayPurchase = new DisplayPurchase();
	private String map;
	private HashMap<Point2D,Purchase> purchases;
	private Point2D position;
	private boolean buyable = false;
	
	
	public void init(HashMap<Point2D, Purchase> purchases) {
		this.purchases = purchases;
	}
	
	public void setMap(String map) {
		this.map = map;
	}
	
	public void setPosition(Point2D position) {
		this.position = position;
	}
	
	public void setBuyable(boolean buyable) {
		this.buyable = buyable;
	}
	
	public void createPurchase(Point2D position) {
		Purchase purchase = RPGApp.listeMaps.get(this.map).getPurchaseList().get(position);
		Text titlePurchase = FXGL.getUIFactoryService().newText(DisplayBasic.returnLine(purchase.getItem().getName(), 28) + " : \n", 
				Color.GOLD,15.0);
		Text descriptionPurchase = FXGL.getUIFactoryService().newText(
				 DisplayBasic.returnLine(purchase.getItem().getEffect(), 28) + "\n",
				Color.WHITE, 14.0);
		Text pricePurchase = FXGL.getUIFactoryService().newText(DisplayBasic.returnLine("Price : " + purchase.getPrice() + "G", 20),
				Color.GOLD, 12.0);
		descriptionPurchase.setLayoutY(35);
		pricePurchase.setLayoutY(55);
		displayPurchase.createPurchase(position, titlePurchase, descriptionPurchase, pricePurchase);
	}
	
	public void lookPurchase(Point2D position) {
		displayPurchase.lookPurchase(position);
		setBuyable(true);
		setPosition(position);;
	}
	
	public void quitPurchase(Point2D position) {
		displayPurchase.quitPurchase(position);
		setBuyable(false);
	}
	
	public void buy(Hero hero) {
		Purchase toBuy = purchases.get(this.position);
		if (buyable && hero.getGold() >= toBuy.getPrice()) {
			hero.buy(toBuy.getPrice());
			displayPurchase.leaveTheShop(this.position);
			RPGApp.mainController.updateInventory(
					"ajout", 
					toBuy.getItem(),
					hero.getPositionVoid()
					);
			RPGApp.mainController.playSound("buy.mp3");
		}
	}
}