package rpg.controler;

import java.io.FileReader;
import java.util.HashMap;
import java.util.Iterator;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.almasb.fxgl.entity.component.Component;

import javafx.geometry.Point2D;
import rpg.RPGApp;
import rpg.entities.character.Hero;
import rpg.entities.character.Npc;
import rpg.system.Quest;
import rpg.system.QuestTarget;
import rpg.view.DisplayNpc;

public class NpcComponent extends Component {
	
	public static String pathNPC = "images/entities/pnjs/vue/";
	private HashMap<String,Npc> npcs = new  HashMap<>();
	private FileController fileController;
	private QuestComponent questController;
	private DisplayNpc displayNpc = new DisplayNpc();	
	
	public void init(QuestComponent questController, FileController fileController) {
		this.questController=questController;
		this.fileController=fileController;
		displayNpc.init(this);
	}
	public void initDialog(Npc npc) {
			if (RPGApp.hero.getFinishQuests().contains(questController.getQuest(npc.getQuestId(),npc.getQuestStep()).getName())) {
				displayNpc.dialog(npc, "finish");
			}
			else if(RPGApp.hero.getCurrentquest() != null && RPGApp.hero.getCurrentquest().getName().equals(questController.getQuest(npc.getQuestId(),npc.getQuestStep()).getName())) {
				displayNpc.dialog(npc, "in progress");
			}
			else {
				displayNpc.dialog(npc, "begin");
			}
		
	}
	public void advance(Npc npc) {
		questController.advanceQuest(QuestTarget.SEE,npc.getName());
	}
	public void dialog(Npc npc, String text) {
		if(text.equals(npc.getgiveQuest())) {
			//questController.advanceQuest(QuestTarget.SEE);
			questController.manageQuest(npc.getQuestId(),npc.getQuestStep());
		}else {
		displayNpc.dialog(npc, text);
		}
	}
	public void setDialogForQuest(String nameNpc,int stepQuest, int ID) {
		System.out.println(nameNpc);
		HashMap<String,HashMap<String,String[]>> chat = fileController.getNpcChat(nameNpc, stepQuest, ID);	
		npcs.get(nameNpc).setListChat(chat);
		npcs.get(nameNpc).setQuestId(ID);
		npcs.get(nameNpc).setQuestStep(stepQuest);
	}
	
	public void initNpc(String file) {
		JSONParser parser = new JSONParser();
		Npc pnj = new Npc();
		Quest quest = new Quest();
		HashMap<String,HashMap<String,String[]>> dialog = new HashMap<String,HashMap<String,String[]>>();
		try {
			Object obj = parser.parse(new FileReader(file));
			JSONObject jsonObject = (JSONObject) obj;
			JSONObject iteratorNext;
			JSONObject positions;
			Point2D position;
			JSONArray npcs = (JSONArray) jsonObject.get("npcs");
			Iterator<JSONObject> iterator = npcs.iterator();
			
			while (iterator.hasNext()) {
				iteratorNext = iterator.next();
				positions = (JSONObject) iteratorNext.get("position");
				position = new Point2D(((Long) positions.get("x")).doubleValue(),((Long) positions.get("y")).doubleValue());
				if(iteratorNext.get("quest").equals("")) {
					
					//Initialisation du PNJ sans quête
					pnj = new Npc(
							(String) iteratorNext.get("name"),
							(String) iteratorNext.get("name"),
							dialog
							);
					this.npcs.put(pnj.getName(), pnj);
				}
				else {
					
					pnj = new Npc(
							(String) iteratorNext.get("name"),
							pathNPC + iteratorNext.get("name") + "_Cadre.png",
							dialog,
							Math.toIntExact((long)iteratorNext.get("id")),
							Math.toIntExact((long)iteratorNext.get("step")),
							(String) iteratorNext.get("givequest")
							);
					this.npcs.put(pnj.getName(), pnj);
				}
				
					//Création du PNJ dans le monde en l'ajoutant dans listeMap
					RPGApp.createPNJ(
						(String) iteratorNext.get("map"),
						pnj,
						position
						);
				
					//Ajout du dialogue au PNJ avec DialogControler
					DialogComponent.setDialog(
							(String) iteratorNext.get("map"), 
							position, 
							"src/main/resources/assets/dialogues/" + iteratorNext.get("name") + "/dialogue-base.json"
							);
					
					
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
