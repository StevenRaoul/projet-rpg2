package rpg;

import static com.almasb.fxgl.dsl.FXGL.getAppHeight;
import static com.almasb.fxgl.dsl.FXGL.getAppWidth;
import static com.almasb.fxgl.dsl.FXGL.getGameScene;
import static com.almasb.fxgl.dsl.FXGL.getGameWorld;
import static com.almasb.fxgl.dsl.FXGL.getInput;
import static com.almasb.fxgl.dsl.FXGL.getPhysicsWorld;

import java.util.HashMap;
import java.util.Map;

import com.almasb.fxgl.app.ApplicationMode;
import com.almasb.fxgl.app.GameApplication;
import com.almasb.fxgl.app.GameSettings;
import com.almasb.fxgl.audio.Music;
import com.almasb.fxgl.dsl.FXGL;
import com.almasb.fxgl.entity.Entity;
import com.almasb.fxgl.input.Input;
import com.almasb.fxgl.input.UserAction;
import com.almasb.fxgl.physics.CollisionHandler;
import com.almasb.fxgl.physics.HitBox;
import com.almasb.fxgl.texture.Texture;

import javafx.geometry.Point2D;
import javafx.scene.input.KeyCode;
import javafx.scene.text.Text;
import rpg.controler.MainController;
import rpg.controler.MoveComponent;
import rpg.controler.QuestComponent;
import rpg.entities.abilities.Boost;
import rpg.entities.abilities.Heal;
import rpg.entities.character.Hero;
import rpg.entities.character.Monster;
import rpg.entities.character.Monsters;
import rpg.entities.character.Npc;
import rpg.entities.character.State;
import rpg.entities.items.Armor;
import rpg.entities.items.HealPotion;
import rpg.entities.items.ManaPotion;
import rpg.entities.items.Weapon;
import rpg.entities.objects.Chest;
import rpg.entities.objects.Clue;
import rpg.entities.objects.Purchase;
import rpg.system.ModeleMap;
import rpg.system.Quest;
import rpg.view.DisplayBasic;
import rpg.view.DisplayNpc;

public class RPGApp extends GameApplication {

	public static final int TILE_SIZE = 64;
	public static Hero hero = new Hero("ian");
	public static Quest quest;
	public static Entity player;
	public static Map<String, ModeleMap> listeMaps = new HashMap<>();
	public static Music test;
	public static boolean save = false;
	public static Entity dialogBox = null;
	private MoveComponent movement;
	public static MainController mainController = new MainController();

	@Override
	protected void initSettings(GameSettings settings) {
		settings.setWidth(960);	//15*64
		settings.setHeight(640);	//10*64
		settings.setTitle("RPGApp");
		settings.setVersion("TechDay");
		settings.setFullScreenAllowed(true);
		settings.setManualResizeEnabled(true);
		settings.setMainMenuEnabled(true);
		settings.setIntroEnabled(false);
		settings.setProfilingEnabled(false);
		settings.setDeveloperMenuEnabled(true);
		settings.setApplicationMode(ApplicationMode.DEVELOPER);
		settings.setAppIcon("images/icon.png");
	}
	
	public Entity getPlayer() {
		return getGameWorld().getSingleton(EntityType.PLAYER);
	}

	@Override
	protected void initGame() {
		getGameWorld().addEntityFactory(new RPGFactory());
		hero.setCurrentMap("maisonRPG.tmx");
		// Initialisation des maps	
		init();
		// Création des portails
		// Portail de la cave
		createPortal("caveRPG.tmx", new Point2D(1472, 960), "maisonRPG.tmx", new Point2D(768, 448));
		// Portail de la maison
		createPortal("maisonRPG.tmx", new Point2D(1152, 1216), "jardinRPG.tmx", new Point2D(1216, 1600));
		createPortal("maisonRPG.tmx", new Point2D(768, 384), "caveRPG.tmx", new Point2D(1472, 832));
		createPortal("maisonRPG.tmx", new Point2D(1216, 1216), "jardinRPG.tmx", new Point2D(1216, 1600));
		// Portail du jardin
		createPortal("jardinRPG.tmx", new Point2D(1216, 1472), "maisonRPG.tmx", new Point2D(1216, 1088));
		createPortal("jardinRPG.tmx", new Point2D(960, 2688), "maisonRPG2.tmx", new Point2D(832, 960));
		createPortal("jardinRPG.tmx", new Point2D(2112, 2176), "maisonRPG3.tmx", new Point2D(832, 960));
		createPortal("jardinRPG.tmx", new Point2D(2432, 2816), "maisonRPG4.tmx", new Point2D(832, 960));
		createPortal("jardinRPG.tmx", new Point2D(640, 1664), "jardinRPG_Ouest.tmx", new Point2D(1344, 1088));
		createPortal("jardinRPG.tmx", new Point2D(640, 1728), "jardinRPG_Ouest.tmx", new Point2D(1344, 1152));
		createPortal("jardinRPG.tmx", new Point2D(640, 1792), "jardinRPG_Ouest.tmx", new Point2D(1344, 1216));
		// Portail des maisons PNJ
		createPortal("maisonRPG2.tmx", new Point2D(768, 1088), "jardinRPG.tmx", new Point2D(960, 2816));
		createPortal("maisonRPG2.tmx", new Point2D(832, 1088), "jardinRPG.tmx", new Point2D(960, 2816));
		createPortal("maisonRPG3.tmx", new Point2D(768, 1088), "jardinRPG.tmx", new Point2D(2112, 2304));
		createPortal("maisonRPG3.tmx", new Point2D(832, 1088), "jardinRPG.tmx", new Point2D(2112, 2304));
		createPortal("maisonRPG4.tmx", new Point2D(768, 1088), "jardinRPG.tmx", new Point2D(2432, 2944));
		createPortal("maisonRPG4.tmx", new Point2D(832, 1088), "jardinRPG.tmx", new Point2D(2432, 2944));
		//Portail du jardin ouest
		createPortal("jardinRPG_Ouest.tmx", new Point2D(1408, 1088), "jardinRPG.tmx", new Point2D(704, 1664));
		createPortal("jardinRPG_Ouest.tmx", new Point2D(1408, 1152), "jardinRPG.tmx", new Point2D(704, 1728));
		createPortal("jardinRPG_Ouest.tmx", new Point2D(1408, 1216), "jardinRPG.tmx", new Point2D(704, 1728));
		createPortal("jardinRPG_Ouest.tmx", new Point2D(704, 1024), "shopRPG.tmx", new Point2D(768, 800));
		createPortal("jardinRPG_Ouest.tmx", new Point2D(960, 1024), "shopRPG.tmx", new Point2D(1216, 800));
		//Portail du shop
		createPortal("shopRPG.tmx", new Point2D(704, 896), "jardinRPG_Ouest.tmx", new Point2D(704, 1088));
		createPortal("shopRPG.tmx", new Point2D(768, 896), "jardinRPG_Ouest.tmx", new Point2D(704, 1088));
		createPortal("shopRPG.tmx", new Point2D(1216, 896), "jardinRPG_Ouest.tmx", new Point2D(960, 1088));
		createPortal("shopRPG.tmx", new Point2D(1280, 896), "jardinRPG_Ouest.tmx", new Point2D(960, 1088));
		//Creation des coffres
		createChest("jardinRPG.tmx", new Point2D(1472, 320), new Chest(new Weapon(40, "Hache", "Hache.png")));
		createChest("maisonRPG.tmx", new Point2D(1664, 448), new Chest(new Weapon(30, "Epee", "Epee.png")));
		createMonster("maisonRPG.tmx", Monsters.RATGRIS, new Point2D(1152, 1200));
		//Creation des achats
		createPurchase("shopRPG.tmx", new Point2D(768, 512), new Purchase(new Weapon(5, "Balai", "Balai.png"), 7));
		createPurchase("shopRPG.tmx", new Point2D(1216, 512), new Purchase(new Armor(5, "Cape en soie bleue", "T-shirt.png"), 4));
		createPurchase("shopRPG.tmx", new Point2D(960, 384), new Purchase(new HealPotion("Potion de soin mineure", "Potion_de_soin.png"), 2));
		createPurchase("shopRPG.tmx", new Point2D(1024, 384), new Purchase(new ManaPotion("Potion de mana mineure", "Potion_de_mana.png"), 2));
		
		hero.setPosition(new Point2D(1536, 448));
		player = getGameWorld().spawn("player", new Point2D(1536, 448));
		mainController.getMapController().chargeMapInit(hero.getCurrentMap());
	    player.addComponent(new MoveComponent());
	    movement = player.getComponent(MoveComponent.class);
	    getGameScene().getViewport().bindToEntity(player, getAppWidth() / 2, getAppHeight() / 2);
		hero.equip(new Armor(1, "T-shirt", "T-shirt.png"));
		hero.addItemInventory(new HealPotion("Potion de soin", "Potion_de_soin.png"));
	}

	@Override
	protected void initInput() {
		Input input = getInput();
		
		input.addAction(new UserAction("Se déplacer à droite") {
			@Override
			protected void onAction() {
				movement.move("right");
			}

			protected void onActionEnd() {
				((Texture) getGameScene().getUINodes().get(0)).set(FXGL.texture("images/entities/joueur/monde/HerosDroite.png"));
			}
		}, KeyCode.D);

		input.addAction(new UserAction("Se déplacer à gauche") {
			@Override
			protected void onAction() {
				movement.move("left");
			}

			protected void onActionEnd() {
				((Texture) getGameScene().getUINodes().get(0)).set(FXGL.texture("images/entities/joueur/monde/HerosGauche.png"));
			}
		}, KeyCode.Q);

		input.addAction(new UserAction("Se déplacer en haut") {
			@Override
			protected void onAction() {
				movement.move("up");
			}

			protected void onActionEnd() {
				((Texture) getGameScene().getUINodes().get(0)).set(FXGL.texture("images/entities/joueur/monde/HerosDos.png"));
			}
		}, KeyCode.Z);

		input.addAction(new UserAction("Se déplacer en bas") {
			@Override
			protected void onAction() {
				movement.move("down");
			}

			protected void onActionEnd() {
				((Texture) getGameScene().getUINodes().get(0)).set(FXGL.texture("images/entities/joueur/monde/HerosFace.png"));
			}
		}, KeyCode.S);

		input.addAction(new UserAction("Afficher les quêtes") {
			@Override
			protected void onActionBegin() {
				try {
					getGameScene().getUINodes().get(1).setVisible(!getGameScene().getUINodes().get(1).isVisible());
					mainController.playSound("tabOpen.mp3");

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}, KeyCode.K);

		input.addAction(new UserAction("Passer la quête") {
			@Override
			protected void onActionBegin() {
				try {
					mainController.nextQuest();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}, KeyCode.B);

		input.addAction(new UserAction("Ouvrir l'inventaire") {
			@Override
			protected void onAction() {
				boolean error;
				try {
					error = false;
					Thread.sleep(200);
				} catch (InterruptedException e) {
					error = true;
					e.printStackTrace();
				}
				if (!error) {
					mainController.changeUINodeVisibility("Inventory");
					mainController.playSound("tabOpen.mp3");
				}
			}
		}, KeyCode.I);

		input.addAction(new UserAction("Ouvrir l'équipement") {
			@Override
			protected void onAction() {
				boolean error;
				try {
					error = false;
					Thread.sleep(200);
				} catch (InterruptedException e) {
					error = true;
					e.printStackTrace();
				}
				if (!error) {
					mainController.changeUINodeVisibility("Equipment");
					mainController.playSound("tabOpen.mp3");
				}
			}
		}, KeyCode.E);
		
		input.addAction(new UserAction("Sauvegarder") {
			@Override
			protected void onAction() {
				boolean error;
				try {
					error = false;
					Thread.sleep(200);
				} catch (InterruptedException e) {
					error = true;
					e.printStackTrace();
				}
				if (!error) {
					hero.setPosition(player.getPosition());
					mainController.save(hero);
				}
			}
		}, KeyCode.W);
		
		input.addAction(new UserAction("Analyze") {
			@Override
			protected void onAction() {
				boolean error;
				try {
					error = false;
					Thread.sleep(200);
				} catch (InterruptedException e) {
					error = true;
					e.printStackTrace();
				}
				if (!error) {
					mainController.analyze();
				}
			}
		}, KeyCode.N);
		
		input.addAction(new UserAction("Charger") {
			@Override
			protected void onAction() {
				boolean error;
				try {
					error = false;
					Thread.sleep(200);
				} catch (InterruptedException e) {
					error = true;
					e.printStackTrace();
				}
				if (!error) {
					hero = mainController.load();
					//TODO
					mainController.getMapController().chargeMapInit(hero.getCurrentMap());
					//mainController.changeMap(getName(), null)
//					DisplayMap.chargeMapInit(hero.getCurrentMap());
				}
			}
		}, KeyCode.X);
		
		input.addAction(new UserAction("Acheter") {
			@Override
			protected void onActionBegin() {
				mainController.buy();
				System.out.println(hero.getGold());
			}
		}, KeyCode.A);
	}

	@Override
	protected void initPhysics() {
		getPhysicsWorld().addCollisionHandler(new CollisionHandler(EntityType.PLAYER, EntityType.NPC) {
	        @Override
	        protected void onCollisionBegin(Entity player, Entity npc) {
	        	mainController.beginDialog(RPGApp.listeMaps.get(RPGApp.hero.getCurrentMap()).getPNJList().get(npc.getPosition()));
	        }
		});

		getPhysicsWorld().addCollisionHandler(new CollisionHandler(EntityType.PLAYER, EntityType.BLOC) {
			@Override
			protected void onHitBoxTrigger(Entity player, Entity bloc, HitBox boxPlayer, HitBox boxBloc) {
				super.onHitBoxTrigger(player, bloc, boxPlayer, boxBloc);
				player.getComponent(MoveComponent.class).addBlocCollision(bloc.getBoundingBoxComponent(), boxPlayer);
			}
			
			@Override
			protected void onCollisionEnd(Entity player, Entity bloc) {
				player.getComponent(MoveComponent.class).removeBlocCollision();
			}
		});

		getPhysicsWorld().addCollisionHandler(new CollisionHandler(EntityType.PLAYER, EntityType.MONSTER) {
			@Override
			protected void onCollisionBegin(Entity player, Entity monsterEntity) {
				Monster monster = monsterEntity.getProperties().getObject("data");
				boolean error = false;
				if (monster.getState() == State.ALIVE) {
					//DisplayBattle.begin(monstre, monstreEntity.getPosition());
					mainController.initFight(monster,monsterEntity);
					try {
						error = false;
						Thread.sleep(200);
					} catch (InterruptedException e) {
						error = true;
						e.printStackTrace();
					}
				}
				if (monster.getState() == State.DEAD) {
					if (!error) {
						FXGL.getGameWorld().removeEntity(monsterEntity);
					}
				}
			}
		});
		
	    getPhysicsWorld().addCollisionHandler(new CollisionHandler(EntityType.PLAYER, EntityType.CHEST) {
	    	@Override
	    	protected void onCollisionBegin(Entity player, Entity chest) {
	    		mainController.findChest(chest.getPosition());
	    		mainController.playSound("chest.wav");
	    	}
	    });
	    
	    getPhysicsWorld().addCollisionHandler(new CollisionHandler(EntityType.PLAYER, EntityType.PURCHASE) {
	    	@Override
	    	protected void onCollisionBegin(Entity player, Entity purchase) {
	    		mainController.lookPurchase(purchase.getPosition());
	    	}
	    	
	    	@Override
	    	protected void onCollisionEnd(Entity player, Entity purchase) {
	    		mainController.quitPurchase(purchase.getPosition());
	    	}
		});
	    
		getPhysicsWorld().addCollisionHandler(new CollisionHandler(EntityType.PLAYER, EntityType.PORTAL) {
			@Override
			protected void onCollisionBegin(Entity player, Entity portal) {
				mainController.playSound("portal.mp3");
				mainController.changeMap(RPGApp.hero.getCurrentMap(), portal.getPosition());
			}
		});
		}

	@Override
	protected void initUI() {
		Text textPixels = new Text();
		textPixels.setTranslateX(50);
		textPixels.setTranslateY(100);
		mainController.initUINodes();
		//FXGL.getGameScene().addUINode(view);
		/*FXGL.getGameScene().addUINode(DisplayQuest.createQuest());
		FXGL.getGameScene().addUINode(DisplayEquipment.createEquipment());
		FXGL.getGameScene().addUINode(DisplayInventory.createInventory());*/
		//getGameScene().addUINode(textPixels);    
	}

	public void initMap(String map, Point2D posHero) {
		ModeleMap mapbase = new ModeleMap();
		mapbase.init();
		mapbase.setPositionHero(posHero);
		listeMaps.put(map, mapbase);
	}


	public static void createMonster(String map, Monsters monster, Point2D posmonstre) {
		if (listeMaps.get(map).getMonsterList().get(posmonstre) == null) {
			listeMaps.get(map).getMonsterList().put(posmonstre, monster);
		}
	}

	public static void createPNJ(String map, Npc npc, Point2D posNpc) {
		if (listeMaps.get(map).getPNJList().get(posNpc) == null) {
			listeMaps.get(map).getPNJList().put(posNpc, npc);
		}
	}

	public void createPortal(String map, Point2D entry, String mapExit, Point2D exit) {
		if (listeMaps.get(map).getPortalList().get(entry) == null) {
			listeMaps.get(map).getPortalList().put(entry, mapExit);
		}
		if (listeMaps.get(mapExit).getReturnPortalList().get(map + entry.toString()) == null) {
			listeMaps.get(mapExit).getReturnPortalList().put(map + entry.toString(), exit);
		}
	}

	public static HashMap<String, String[]> Chat(String[] answers, String question) {
		HashMap<String, String[]> chat = new HashMap<String, String[]>();
		chat.put("answers", answers);
		question = DisplayBasic.returnLine(question,30);
		String[] listeQuestion = new String[1];
		listeQuestion[0] = question;
		chat.put("message", listeQuestion);
		return chat;
	}

	public static void createChest(String map, Point2D pos, Chest chest) {
		if (listeMaps.get(map).getChestList().get(pos) == null) {
			listeMaps.get(map).getChestList().put(pos, chest);
		}
	}

	public static void createClue(String map, Point2D pos, Clue clue) {
		if (listeMaps.get(map).getClueList().get(pos) == null) {
			listeMaps.get(map).getClueList().put(pos, clue);
		}
	}
	
	public static void createPurchase(String map, Point2D pos, Purchase purchase) {
		if (listeMaps.get(map).getPurchaseList().get(pos) == null) {
			listeMaps.get(map).getPurchaseList().put(pos, purchase);
		}
	}

	public void initMaps() {
		initMap("maisonRPG.tmx", new Point2D(1472, 448));
		initMap("caveRPG.tmx", new Point2D(1472, 896));
		initMap("jardinRPG.tmx", new Point2D(1216, 1536));
		initMap("maisonRPG2.tmx", new Point2D(832, 1088));
		initMap("maisonRPG3.tmx", new Point2D(832, 1088));
		initMap("maisonRPG4.tmx", new Point2D(832, 1088));
		initMap("jardinRPG_Ouest.tmx", new Point2D(1024, 832));
		initMap("shopRPG.tmx", new Point2D(1216, 896));
	}

	public void init() {
		initMaps();
		initMonsters();
	}
	public void initMonsters() {
		mainController.init(hero);
	}
	public static void main(String[] args) {
		launch(args);
	}
}
