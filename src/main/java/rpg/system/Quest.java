package rpg.system;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedHashMap;

import rpg.RPGApp;
import rpg.entities.character.Hero;
import rpg.entities.character.Monsters;
import rpg.entities.character.Npc;

public class Quest implements Serializable {
	
	private String name;
	private int reward;
	private boolean succeed = false;
	private int nbKill;
	private int nbTargetMax;
	private int nbTarget;
	private String action;
	private Monsters typeMonster;
	private QuestTarget typeTarget;
	private int kill = 0;
	private String description;
	private LinkedHashMap spawns;
	private ArrayList<String> npcs = new ArrayList<>();
	private int id;
	private int step;
	private String targetToSee;
	
	public boolean advance() {
		nbTarget++;
		return nbTarget>=nbTargetMax;
	}
	public void setStep(int step) {
		this.step=step;
	}
	public int getStep() {
		return this.step;
	}
	public void addNPC(String name) {
		npcs.add(name);
	}
	public ArrayList<String> getNpcs(){
		return npcs;
	}
	
	public Quest(String name, int reward, Monsters monster, int nbTargetMax) {
		this.name = name;
		this.setReward(reward);
		this.nbTargetMax = nbTargetMax;
		this.setTypeMonstre(monster);	
	}
	
	public Quest(String name, int reward, String action, QuestTarget typeTarget, int nbTargetMax, String description) {
		this.name = name;
		this.reward = reward;
		this.action = action;
		this.typeTarget = typeTarget;
		this.nbTarget = 0;
		this.nbTargetMax = nbTargetMax;
		this.description = description;
		this.id = 0;
	}
	
	public Quest(String name, int reward, String action, QuestTarget typeTarget, int nbTargetMax, String description, int id) {
		this.name = name;
		this.reward = reward;
		this.action = action;
		this.typeTarget = typeTarget;
		this.nbTarget = 0;
		this.nbTargetMax = nbTargetMax;
		this.description = description;
		this.id = id;
	}
	
	public Quest(String name) {
		this.name = name;
	}
	
	public Quest() {
		this.name = "null";
	}
	
	public boolean verifQuest() {
		boolean suceedValue = nbTarget >= nbTargetMax;
		if (succeed == false) {
			if (suceedValue) {
				succeed = true;
				return (succeed);
			}
			else {
				return false;
			}
		}
		else {
			return true;
		}
	}
	
	public void validQuest(Hero hero) {
		hero.addlistFinishQuests(name);
		hero.gainExp(this.reward);
	}
	
	public void upNbTarget() {
		this.nbTarget += 1;
	}
	
	public int getReward() {
		return this.reward;
	}
	
	public void setReward(int reward) {
		this.reward = reward;
	}
	
	public int getKill() {
		return kill;
	}
	
	public void setKill(int kill) {
		this.kill = kill;
	}
	
	public Monsters getTypeMonstre() {
		return typeMonster;
	}
	
	public void setTypeMonstre(Monsters typeMonster) {
		this.typeMonster = typeMonster;
	}
	
	public String getName() {
		return this.name;
	}
	
	public int getNbKill() {
		return this.nbKill;
	}
	
	public int getNbTarget() {
		return this.nbTarget;
	}

	public void setNbTarget(int nbTarget) {
		this.nbTarget = nbTarget;
	}
	
	public int getNbTargetMax() {
		return this.nbTargetMax;
	}

	public void setNbTargetMax(int nbTargetMax) {
		this.nbTargetMax = nbTargetMax;
	}

	public QuestTarget getTypeTarget() {
		return this.typeTarget;
	}

	public void setTypeTarget(QuestTarget typeTarget) {
		this.typeTarget = typeTarget;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String getAction() {
		return this.action;
	}
	
	public void setAction(String action) {
		this.action = action;
	}
	
	public int getId() {
		return this.id;
	}
	
	public void setId(int id) {
		this.id = id;
	}

	public LinkedHashMap getSpawns() {
		return spawns;
	}

	public void setSpawns(LinkedHashMap spawns) {
		this.spawns = spawns;
	}
	public String getTargetToSee() {
		return targetToSee;
	}
	public void setTargetToSee(String targetToSee) {
		this.targetToSee = targetToSee;
	}
}