package rpg.system;

import rpg.RPGApp;
import rpg.controler.MainController;
import rpg.entities.abilities.Boost;
import rpg.entities.character.Character;
import rpg.entities.character.Hero;
import rpg.entities.character.Monster;
import rpg.entities.character.State;

public class Fights {
	
	public void battleAttack(Character character, int attack) {
		int result = (attack - character.getDef());
		int result_final = character.getHp() - Math.max(1, result);
		if (result_final <= 0) {
			character.setState(State.DEAD);
			character.setHp(0);
		} 
		else {
			character.setHp(result_final);
		}
	}

	public void battleAttack(Character character1, Character character2) {
		int result = (character1.getAtk() - character2.getDef());
		int result_final = character2.getHp() - Math.max(1, result);
		if (result_final <= 0) {
			character2.setState(State.DEAD);
			character2.setHp(0);
		} 
		else {
			character2.setHp(result_final);
		}
	}
	
	public void afterAction(Hero hero, Monster monster) throws Exception  {
		if (hero.getState() == State.DEAD || monster.getState() == State.DEAD) {
			throw (new Exception("Erreur l'un des 2 personnages est mort"));
		}
		if (monster.getState() == State.ALIVE) {
			battleAttack(monster, hero);
		}
		else if (monster.getState() == State.DEAD) {
			RPGApp.hero.gainExp(monster.getGive_experience());
			if (RPGApp.hero.getCurrentquest() != null) {
				if (RPGApp.hero.getCurrentquest().getTypeMonstre() == monster.getTypeMonstre()) {
					RPGApp.hero.getCurrentquest().setKill(RPGApp.hero.getCurrentquest().getKill() + 1);
				}
			} 
		}
	}

	public void battle(Character character, Character opponentCharacter, String choice) throws Exception {
		if (character.getState() == State.DEAD || opponentCharacter.getState() == State.DEAD) {
			throw (new Exception("Erreur l'un des 2 Character est mort"));
		}
		else if (choice.equals("attaque")) {
			if (character instanceof Hero) {
				if (((Hero) character).getEquipement().get("Arme") != null) {
					for (int i = 0; i < opponentCharacter.getWeaknesses().length; i++) {
						if (((Hero) character).getEquipement().get("Arme").getName().equals(opponentCharacter.getWeaknesses()[i])) {
							battleAttack(opponentCharacter, character.getAtk() * 4);
							return;
						}
					}
					battleAttack(character, opponentCharacter);
					return;
				}
				else {
					battleAttack(character,opponentCharacter);
					return;
				}
			}
			else if (character instanceof Monster) {
				battleAttack(character,opponentCharacter);
				return;
			}
		} 
		else if (choice.equals("défense")) {
			character.setDef(character.getDef()*2);
			character.addBoost(new Boost("défense", 0, 1,"def", 100));
			return;
		}
		else {
			throw (new Exception("Wrong Choice"));
		}
	}
}
