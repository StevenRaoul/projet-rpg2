package rpg.system;

import java.io.Serializable;
import java.util.HashMap;

import com.almasb.fxgl.entity.Entity;

import javafx.geometry.Point2D;
import rpg.entities.character.Monsters;
import rpg.entities.character.Npc;
import rpg.entities.objects.Chest;
import rpg.entities.objects.Clue;
import rpg.entities.objects.Purchase;

public class ModeleMap implements Serializable {
	private HashMap<Point2D, String> portalList;
	private HashMap<String, Point2D> returnPortalList;
	private HashMap<Point2D, Monsters> monsterList;
	private Point2D positionHero;
	private HashMap<Point2D, Chest> chestList;
	private HashMap<Point2D, Npc> npcList;
	private HashMap<Point2D, Clue> clueList;
	private HashMap<Point2D, Purchase> purchaseList;
	public boolean monsterMove = true; 
	public Entity notif = null;

	public ModeleMap() {}

	public void setPortalList(HashMap<Point2D, String> portalList) {
		this.portalList = portalList;
	}
	
	public HashMap<Point2D, String> getPortalList() {
		return this.portalList;
	}
	
	public void setReturnPortalList(HashMap<String, Point2D> returnPortalList) {
		this.returnPortalList = returnPortalList;
	}
	
	public HashMap<String, Point2D> getReturnPortalList() {
		return this.returnPortalList;
	}
	
	public void setMonsterList(HashMap<Point2D, Monsters> monsterList) {
		this.monsterList = monsterList;
	}
	
	public HashMap<Point2D, Monsters> getMonsterList() {
		return this.monsterList;
	}
	
	public void setCoffreList(HashMap<Point2D, Chest> chestList) {
		this.chestList = chestList;
	}
	
	public HashMap<Point2D, Chest> getChestList() {
		return this.chestList;
	}
	
	public void setIndiceList(HashMap<Point2D, Clue> clueList) {
		this.clueList = clueList;
	}
	
	public HashMap<Point2D, Clue> getClueList() {
		return this.clueList;
	}
	
	public void init() {
		portalList = new HashMap<>();
		returnPortalList = new HashMap<>();
		monsterList = new HashMap<>();
		clueList = new HashMap<>();
		purchaseList = new HashMap<>();
		chestList = new HashMap<>();
		setPNJList(new HashMap<>());
	}
	
	public void setPositionHero(Point2D positionHero) {
		this.positionHero = positionHero;
	}
	
	public Point2D getPositionHero() {
		return this.positionHero;
	}
	
	public HashMap<Point2D, Npc> getPNJList() {
		return npcList;
	}
	
	public void setPNJList(HashMap<Point2D, Npc> npcList) {
		this.npcList = npcList;
	}
	
	public HashMap<Point2D, Purchase> getPurchaseList() {
		return purchaseList;
	}
	
	public void setPurchaseList(HashMap<Point2D, Purchase> purchaseList) {
		this.purchaseList = purchaseList;
	}
}