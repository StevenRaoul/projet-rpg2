package rpg.system;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.HashMap;

import javafx.geometry.Point2D;
import rpg.entities.character.Hero;
import rpg.entities.items.Equipment;
import rpg.entities.items.Item;

public class SaveAndLoadController {
	
	private ArrayList<String> listeSaves;
	
	public Hero load() {
		return load("AutoSave");
	}
	
	public Hero load(String nameSave) {
		ObjectInputStream objectInputStream = null;
		FileInputStream file = null;
		try {
			file = new FileInputStream(nameSave);
			objectInputStream = new ObjectInputStream(file);
			final Hero hero = (Hero) objectInputStream.readObject();
			hero.setInventory((Item[]) objectInputStream.readObject());
			HashMap<String, Equipment> equip = (HashMap<String, Equipment>) objectInputStream.readObject();
			hero.setEquipment(equip);
			hero.setPosition(new Point2D(
					objectInputStream.readDouble(), 
					objectInputStream.readDouble())
					);
			SaveMap.load(objectInputStream);
			System.out.println("not null");
			return hero;
		} 
		catch (final java.io.IOException e) {
			e.printStackTrace();
		} 
		catch (final ClassNotFoundException e) {
			e.printStackTrace();
		} 
		finally {
			try {
				if (file != null) {
					file.close();
				}
				if (objectInputStream != null) {
					objectInputStream.close();
				}
			} 
			catch (final IOException ex) {
				ex.printStackTrace();
			}
			finally {
				if (file != null) {
					try {
						file.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
				if (objectInputStream != null) {
					try {
						objectInputStream.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		}
		System.out.println("null");
		return null;
	}
	
	public void save(Hero hero) {
		save(hero,"AutoSave");
	}
	
	public void save(Hero hero, String nameSave) throws NullPointerException {
		ObjectOutputStream objectOutputStream = null;
		FileOutputStream file = null;
		try {
			file = new FileOutputStream(nameSave);
			objectOutputStream = new ObjectOutputStream(file);
			objectOutputStream.writeObject(hero);
			objectOutputStream.writeObject(hero.getInventory());
			objectOutputStream.writeObject(hero.getEquipement());
			objectOutputStream.writeDouble(hero.getPosition().getX());
			objectOutputStream.writeDouble(hero.getPosition().getY());
			SaveMap.save(objectOutputStream);
		} catch (final java.io.IOException e) {
			e.printStackTrace();
		} 
		finally {
			try {
				objectOutputStream.flush();
				file.close();
				objectOutputStream.close();
			} catch (final IOException ex) {
				ex.printStackTrace();
			}
			finally {
				if (file != null) {
					try {
						file.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
				if (objectOutputStream != null) {
					try {
						objectOutputStream.flush();
					} catch (IOException e) {
						e.printStackTrace();
					}
					try {
						objectOutputStream.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		}
	}
}