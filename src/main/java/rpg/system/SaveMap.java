package rpg.system;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Map.Entry;

import javafx.geometry.Point2D;
import rpg.RPGApp;
import rpg.entities.objects.Chest;

public abstract class SaveMap {
	
	public static void save(ObjectOutputStream objectOutputStream) {
		for (Entry<String, ModeleMap> map : RPGApp.listeMaps.entrySet()) {
			try {
				saveChest(objectOutputStream, map.getValue());
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	public static void saveChest(ObjectOutputStream objectOutputStream, ModeleMap modeleMap) throws IOException {
		for (Entry<Point2D, Chest> chest : modeleMap.getChestList().entrySet()) {
			objectOutputStream.writeDouble(chest.getKey().getX());
			objectOutputStream.writeDouble(chest.getKey().getY());
			objectOutputStream.writeObject(chest.getValue());
		}
	}

	public static void loadChest(ObjectInputStream objectInputStream, ModeleMap modeleMap) throws ClassNotFoundException, IOException {
		for (Entry<Point2D, Chest> chestEntry : modeleMap.getChestList().entrySet()) {
			Double posX = objectInputStream.readDouble();
			Double posY = objectInputStream.readDouble();
			Chest chest = (Chest) objectInputStream.readObject();
				modeleMap.getChestList().put(
						new Point2D(posX, posY), chest);
		}
	}
	
	public static void load(ObjectInputStream objectInputStream) {
		for (Entry<String, ModeleMap> map : RPGApp.listeMaps.entrySet()) {
			try {
				loadChest(objectInputStream, map.getValue());
			} catch (IOException | ClassNotFoundException e) {
				e.printStackTrace();
			}
		}
	}
}