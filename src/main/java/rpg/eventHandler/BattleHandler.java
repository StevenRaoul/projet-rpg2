package rpg.eventHandler;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.MenuItem;
import rpg.controler.FightController;
import rpg.entities.abilities.Skill;
import rpg.view.DisplayBasic;

public class BattleHandler extends DisplayBasic implements EventHandler<ActionEvent> {
	private FightController fightController ;
	private String choix;

	public BattleHandler(FightController fightController) {
		this.fightController = fightController;
	}
	
	public BattleHandler(FightController fightController, String choice) {
		this.fightController = fightController;
		this.choix = choice;
	}
	
	public void update(String choix) {
		this.choix = choix;
	}

	@Override
	public void handle(ActionEvent event) {
		if (choix.equals("skills")) {
		fightController.manage(((Skill) ((MenuItem) event.getSource()).getUserData()), fightController.getHero());
		}
		else {
		fightController.manage(choix, fightController.getHero());
		}
	}
}