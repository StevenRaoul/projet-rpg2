package rpg.eventHandler;

import com.almasb.fxgl.dsl.FXGL;

import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;
import rpg.RPGApp;
import rpg.entities.items.Item;
import rpg.view.CustomWindow;
import rpg.view.DisplayBasic;

public class ItemHandler extends DisplayBasic implements EventHandler<MouseEvent> {
	
	private Item item;
	private boolean inventory;
	private int posItem;
	private String type;
	private boolean active = false;

	public ItemHandler(int posItem) {
		this.posItem = posItem;
		inventory = true;
	}

	public ItemHandler() {}

	public void init(int posItem) {
		this.posItem = posItem;
		inventory = true;
		active = true;
	}

	public void init(String type) {
		this.type = type;
		inventory = false;
		active = true;
	}

	public ItemHandler(String type) {
		this.type = type;
		inventory = false;
	}

	@Override
	public void handle(MouseEvent arg0) {
		if (active) {
			if (inventory) {
				this.item = RPGApp.hero.getInventory()[posItem];
			} 
			else {
				this.item = RPGApp.hero.getEquipement().get(type);
			}
			int width = 135;
			int height = 64;
			Rectangle border = new Rectangle(0, 0, width, height);
			border.setFill(Color.rgb(0, 255, 0, 0.8));
			CustomWindow window = new CustomWindow();
			Text explicationItem = FXGL.getUIFactoryService().newText(
					item.getName() + " : \n" + DisplayBasic.returnLine(item.getEffect(), 20), Color.WHITE, 15.0);
			window.setScaleShape(true);
			window.setPrefSize(width, height);
			window.setShape(border);
			Pane textContent = new Pane();
			textContent.getChildren().add(explicationItem);
			window.setContentPane(textContent);
			window.setLayoutX(0);
			window.setLayoutY(-64);
			window.setMouseTransparent(true);
			((Pane) ((CustomWindow) arg0.getSource()).getProperties().get("pane")).getChildren().add(window);
			((CustomWindow) arg0.getSource())
					.setOnMouseExited((e) -> ((Pane) ((CustomWindow) arg0.getSource()).getProperties().get("pane"))
					.getChildren().remove(window));
		}
	}

	public void setState(boolean active) {
		this.active = active;
	}
}