package rpg.eventHandler;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import rpg.RPGApp;
import rpg.controler.NpcComponent;
import rpg.controler.QuestComponent;
import rpg.entities.character.Npc;
import rpg.system.Quest;
import rpg.view.DisplayNpc;
import rpg.view.DisplayQuest;

public class NpcHandler implements EventHandler<ActionEvent> {
	
	private Button button;
	private Npc npc;
	private NpcComponent npcController;
	
	public  NpcHandler(Button button, Npc npc,NpcComponent npcController) {
		this.button = button;
		this.npc = npc;
		this.npcController=npcController;
	}
	
	@Override
	public void handle(ActionEvent ActionEvent) {
		Quest quest = RPGApp.hero.getCurrentquest();
		System.out.println(npc.getQuest());
		if (npc.getgiveQuest().equals(button.getText())) {
			if(quest == null) {
				/*String questFile = "quest-" + npc.getQuest().getId() + "-0.json";
				QuestComponent.setQuest(questFile);*/
				npcController.dialog(npc,button.getText());
			}
			else {
				if (quest.getAction().equals("Voir") && quest.getTypeTarget().equals(this.npc.getName())) {
					quest.upNbTarget();
					//QuestComponent.verifQuest();
				}
			}
			//DisplayQuest.updateQuest();
			RPGApp.dialogBox.removeFromWorld();
			//MusicComponent.soundPlay("accept");
			RPGApp.dialogBox = null;
		}
		else if (npc.getListChat().get(button.getText()) != null) {
			npcController.dialog(npc, button.getText());
		}
		else {
			RPGApp.dialogBox.removeFromWorld();
			RPGApp.dialogBox = null;
			npcController.advance(npc);
		}
	}
}