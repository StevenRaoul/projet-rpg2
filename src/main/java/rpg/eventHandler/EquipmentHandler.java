package rpg.eventHandler;

import com.almasb.fxgl.dsl.FXGL;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.input.MouseEvent;
import rpg.RPGApp;
import rpg.controler.EquipmentController;
import rpg.entities.items.Equipment;

public class EquipmentHandler implements EventHandler<MouseEvent> {
	
	private Node itemViewBig;
	private String type;
	private boolean active = false;
	private EquipmentController equipmentController;

	public EquipmentHandler() {}

	public EquipmentHandler(Node itemViewBig, String type) {
		this.itemViewBig = itemViewBig;
		this.type = type;
	}

	public void setImage(Node image) {
		this.itemViewBig = image;
	}

	public void setType(String type) {
		this.type = type;
	}

	public void setImageAndType(Node image, String type) {
		this.type = type;
		this.itemViewBig = image;
	}

	public void init(Node image, String type,EquipmentController equipmentController) {
		setImageAndType(image, type);
		active = true;
		this.equipmentController=equipmentController;
	}
	
	@Override
	public void handle(MouseEvent event) {
		if (active) {
			Button[] listButton = new Button[2];
			listButton[0] = new Button("non");
			listButton[1] = new Button("oui");
			Equipment item = equipmentController.getEquipment(type);
			String findephrase = " dans ton équipement veux tu la déséquiper ?";
			listButton[1].setOnAction(new EventHandler<ActionEvent>() {
				@Override
				public void handle(ActionEvent ActionEvent) {
					equipmentController.handleUnequip(type);
					RPGApp.mainController.playSound("unequip.mp3");
				}
			});
			try {
				FXGL.getDialogService().showBox("tu as " + item.getName() + findephrase, itemViewBig, listButton);
			} catch (Exception e) {
				System.out.println("Il n'y a plus aucun objet dans cet emplacement");
			}
		}
	}

	public void setState(boolean active) {
		this.active = active;
	}
}