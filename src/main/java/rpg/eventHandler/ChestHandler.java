package rpg.eventHandler;

import com.almasb.fxgl.dsl.FXGL;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Point2D;
import rpg.RPGApp;
import rpg.controler.ChestController;
import rpg.entities.items.Consumable;
import rpg.entities.items.Item;
import rpg.entities.objects.Chest;
import rpg.view.DisplayBasic;

public class ChestHandler implements EventHandler<ActionEvent> {

	private Chest chest;
	private Point2D posChest;
	private ChestController chestController;

	public ChestHandler(Chest chest, Point2D posChest) {
		this.chest = chest;
		this.posChest = posChest;
	}
	
	public void init(ChestController chestController) {
		this.chestController = chestController;
	}
	
	@Override
	public void handle(ActionEvent ActionEvent) {
		Item item = chest.getLoot();
		String text;
		if (item instanceof Consumable) {
			text = " dans ce coffre veux tu l'utiliser ?";
		} 
		else {
			text = " dans ce coffre veux tu l'équiper ?";
		}
		String message = "tu as trouve " + item.getName() + text;
		int positionItem = RPGApp.hero.getPositionVoid();
		chestController.handle(item);
		DisplayBasic.updateNode(
				FXGL.getGameWorld().getEntitiesAt(posChest).get(0).getViewComponent().getChildren().get(0),
				FXGL.texture("images/entities/objets/Coffre_Ouvert.png"),
				FXGL.getGameWorld().getEntitiesAt(posChest).get(0).getViewComponent());
		chest.setLoot(null);
	}
}