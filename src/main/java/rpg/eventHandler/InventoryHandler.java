package rpg.eventHandler;

import com.almasb.fxgl.dsl.FXGL;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.input.MouseEvent;
import rpg.controler.InventoryController;
import rpg.entities.items.Item;

public class InventoryHandler implements EventHandler<MouseEvent> {
	
	private Node itemViewBig;
	private int pos;
	private String message;
	private InventoryController inventoryController;
	private boolean active = false;
	
	
	public InventoryHandler() {}
	
	public InventoryHandler(String message) {
		this.message=message;
	}
	public InventoryHandler(Node itemViewBig, int pos) {
		this.itemViewBig = itemViewBig;
		this.pos = pos;
	}

	public void setPos(int posItem) {
		this.pos = posItem;
	}

	public void setImage(Node imgPopup) {
		itemViewBig = imgPopup;
	}

	public void setImageAndPos(Node imgPopup, int posItem) {
		this.pos = posItem;
		itemViewBig = imgPopup;
	}

	public void init(Node imgPopup, int posItem, InventoryController inventoryController) {
		setImageAndPos(imgPopup, posItem);
		this.active = true;
		this.inventoryController=inventoryController;
	}

	@Override
	public void handle(MouseEvent event) {
		if (this.active) {
			Button[] listeButtonChoix = new Button[2];
			listeButtonChoix[0] = new Button("non");
			listeButtonChoix[1] = new Button("oui");
			message = inventoryController.getInventoryMessage(pos);
			Item item = inventoryController.getItem(pos);
			listeButtonChoix[1].setOnAction(new EventHandler<ActionEvent>() {
				@Override
				public void handle(ActionEvent ActionEvent) {
					inventoryController.handleItem(pos);
//					if (item instanceof Consumable) {
//						Consumable Consommable = (Consumable) item;
//						Consommable.effect();
//						RPGApp.hero.removeItemInventory(Consommable);
//						DisplayInventory.updateInventory("remove", Consommable, Consommable.getPosition());
//					} 
//					else {
//						Equipment equipment = (Equipment) item;
//						if (RPGApp.hero.getEquipement().get(equipment.getType()) != null) {
//							DisplayInventory.updateInventory("remove", equipment, pos);
//							RPGApp.hero.getEquipement().get(equipment.getType()).setPosition(pos);
//							DisplayInventory.updateInventory("ajout", RPGApp.hero.getEquipement().get(item.getType()), pos);
//							DisplayEquipment.updateEquipment("remove", RPGApp.hero.getEquipement().get(item.getType()));
//							DisplayEquipment.updateEquipment("ajout", equipment);
//							RPGApp.hero.removeItemInventory(equipment);
//							RPGApp.hero.equip(equipment);
//						} 
//						else {
//							DisplayInventory.updateInventory("remove", equipment, equipment.getPosition());
//							DisplayEquipment.updateEquipment("ajout", equipment);
//							RPGApp.hero.removeItemInventory(equipment);
//							RPGApp.hero.equip(equipment);
//						}
//					}
				}
			});
			FXGL.getDialogService().showBox("tu as " + item.getName() + message, itemViewBig, listeButtonChoix);
		}
	}

	public void setState(boolean active) {
		this.active = active;
	}
}