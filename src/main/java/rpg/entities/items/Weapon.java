package rpg.entities.items;

import java.io.Serializable;

public class Weapon extends Equipment implements Serializable{
	
	public Weapon(int stat, String name, String image){
		super(stat, name, image);
		this.type = "Arme";
		this.effect = "Atk : " + stat;
	}
}