package rpg.entities.items;

import rpg.RPGApp;

public class ManaPotion extends Consumable {

	public ManaPotion(String name, String image) {
		super(name, image);
		this.type = "potion";
		this.effect = "Restaure 10 points de mana";
	}

	@Override
	public void effect() {
		RPGApp.hero.restore("mp",10);
	}
}