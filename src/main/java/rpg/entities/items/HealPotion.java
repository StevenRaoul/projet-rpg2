package rpg.entities.items;

import rpg.RPGApp;

public class HealPotion extends Consumable {

	public HealPotion(String name, String image) {
		super(name, image);
		this.type = "potion";
		this.effect = "Restaure 50 points de vie";
	}

	@Override
	public void effect() {
		RPGApp.hero.restore("pv", 50);
	}
}