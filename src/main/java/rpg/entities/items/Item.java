package rpg.entities.items;

import java.io.Serializable;

public class Item implements Serializable {
	
	private String name;
	protected String type;
	private String image;
	private int position = -2;
	protected String effect;
	private String path = "images/items/";
	
	public Item(String name, String image) {
		this.setName(name);
		this.image = image;
	}

	public Item() {}

	public String getEffect() {
		return this.effect;
	}
	
	public String getImage() {
		return path + "vue/" + image;
	}

	public String getInventaireImage() {
		String[] itemImage = image.split("\\.");
		return path + "inventaire/" + itemImage[0] + "_Inventaire." + itemImage[1];
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getPosition() {
		return this.position;
	}

	public void setPosition(int position) {
		this.position = position;
	}
}