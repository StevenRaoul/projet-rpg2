package rpg.entities.items;

import java.io.Serializable;

public class Armor extends Equipment implements Serializable {
	
	public Armor(int stat, String name, String image){
		super(stat, name, image);
		this.type = "Armure";
		this.effect = "Def : " + stat;
	}
}