package rpg.entities.abilities;

import rpg.entities.character.Character;

public class Boost extends Skill {
	
	private int nbturn;
	private int boost;
	private int activeTurn;
	private String stat;
	private int pourcentage;
	private boolean active=false;

	public Boost(String name, int cost, int pourcentage, String stat, int nbturn) {
		super(name);
		this.cost = cost;
		this.type = SkillType.BOOST;
		this.nbturn = nbturn;
		this.pourcentage = pourcentage;
		this.inFight = true;
		this.stat = stat;
	}
	
	@Override
	public void effect(Character character) {
		this.boost = character.givemultiply("atk", (int) (this.pourcentage));
		character.give(stat, boost);
		character.useSkill(getCost());
		character.addBoost(this);
		this.activeTurn = nbturn;

	}

	public void deleteBonus(Character character) {
		character.give(stat, -boost);
	}
	public void use() {
		activeTurn--;
		if(activeTurn<=0) {
			activeTurn=0;
			active=false;
		}
	}
	public boolean getActivity() {
		return active;
	}
}
