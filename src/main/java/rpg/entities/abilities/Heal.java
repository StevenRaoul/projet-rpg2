package rpg.entities.abilities;

import rpg.entities.character.Character;
public class Heal extends Skill {

	private int pourcentage;

	public Heal(String name, int cost, int pourcentage) {
		super(name);
		this.cost = cost;
		this.type = SkillType.HEAL;
		this.pourcentage = pourcentage;
		this.inFight = false;
	}
	
	@Override
	public void effect(Character character) {
		character.heal(pourcentage);
		character.useSkill(getCost());
	}
}