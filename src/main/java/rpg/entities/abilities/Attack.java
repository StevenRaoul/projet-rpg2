package rpg.entities.abilities;

import rpg.entities.character.Character;
import rpg.system.Fights;

public class Attack extends Skill {
	
	private double pourcentage;
	transient private Fights fight = new Fights();

	public Attack(String name, int cost, int pourcentage) {
		super(name);
		this.cost = cost;
		this.pourcentage = pourcentage;
		this.type = SkillType.ATTACK;
		this.inFight = true;
	}
	

	public void effect(Character character, Character ennemyCharacter) {
		Double perc=(pourcentage/100)+1;
		fight.battleAttack(ennemyCharacter, (int) (character.getAtk() * (1 + pourcentage / 100)));
		character.useSkill(getCost());
	}
}