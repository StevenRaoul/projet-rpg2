package rpg.entities.objects;

import java.io.Serializable;

import rpg.entities.items.Item;

public class Purchase implements Serializable {
	
	private Item item;
	private String image;
	private int price;
	
	public Purchase(Item item, int price) {
		this.item = item;
		this.image = item.getInventaireImage();
		this.price = price;
	}
	
	public Item getItem() {
		return this.item;
	}
	
	public String getImage() {
		return this.image;
	}
	
	public int getPrice() {
		return this.price;
	}
	
	public void setItem(Item item) {
		this.item = item;
		this.image = this.item.getInventaireImage();
	}
	
	public void setPrice(int value) {
		this.price = value;
	}
}