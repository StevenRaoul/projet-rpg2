package rpg.entities.objects;

import java.io.Serializable;

public class Clue implements Serializable {
	
	private String name;
	private String image;
	
	public Clue(String name, String image) {
		this.name = name;
		this.image = image;
	}

	public Clue() {}

	public String getName() {
		return this.name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getImage() {
		return this.image;
	}
	
	public void setImage(String image) {
		this.image = image;
	}
}