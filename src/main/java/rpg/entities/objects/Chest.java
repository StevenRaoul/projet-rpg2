package rpg.entities.objects;

import java.io.Serializable;

import rpg.entities.items.Item;

public class Chest implements Serializable {
	
	private Item loot;
	
	public Chest(Item loot) {
		this.setLoot(loot);
	}

	public Chest() {}

	public Item getLoot() {
		return this.loot;
	}
	
	public void setLoot(Item loot) {
		this.loot = loot;
	}
}