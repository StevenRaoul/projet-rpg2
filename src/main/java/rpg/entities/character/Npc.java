package rpg.entities.character;

import java.util.HashMap;

import rpg.system.Quest;


public class Npc {
	
	private String name;
	private String image;
	private Quest quest;
	private String giveQuest = "null";
	private HashMap<String,String[]> chat;
	private HashMap<String,HashMap<String,String[]>> listChat;
	private String path = "images/entities/pnjs/vue/";
	private int QuestId;
	private int QuestStep;
	
	public Npc() {
		this.name = "defaut";
		this.image = "defaut";
	}
	
	public Npc(String name, String image, HashMap<String, HashMap<String, String[]>> conversation, int QuestId,int QuestStep, String giveQuest) {
		this.listChat=conversation;
		this.name = name;
		this.image = image;
		this.giveQuest = giveQuest;
		this.QuestId = QuestId;
		this.QuestStep = QuestStep;
	}
	
	public Npc(String name, String image, HashMap<String, HashMap<String, String[]>> conversation) {
		this.listChat = conversation;
		this.name = name;
		this.image = image;
		this.giveQuest="";
	}
	
	public HashMap<String, HashMap<String, String[]>> getListChat() {
		return this.listChat;
	}
	
	public void setListChat(HashMap<String, HashMap<String, String[]>> listChat) {
		this.listChat = listChat;
	}
	
	public String getName() {
		return name;
	}
	
	public Quest getQuest() {
		return quest;
	}
	
	public String getgiveQuest() {
		return giveQuest;
	}
	
	public void setGiveQuest(String giveQuest) {
		this.giveQuest = giveQuest;
	}
	
	public String getImage() {
		return path + this.name + "_Cadre.png";
	}
	
	public String getRotateImage(String ajout) {
		String[] a = getImage().split("_");
		return a[0] + "_" + ajout + ".png";
	}

	public int getQuestId() {
		return QuestId;
	}

	public void setQuestId(int questId) {
		QuestId = questId;
	}

	public int getQuestStep() {
		return QuestStep;
	}

	public void setQuestStep(int questStep) {
		QuestStep = questStep;
	}	
}