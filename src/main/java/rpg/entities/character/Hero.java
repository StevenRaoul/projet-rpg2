package rpg.entities.character;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

import javafx.geometry.Point2D;
import rpg.entities.abilities.Attack;
import rpg.entities.abilities.Boost;
import rpg.entities.abilities.Heal;
import rpg.entities.items.Equipment;
import rpg.entities.items.Item;
import rpg.system.Quest;

public class Hero extends Character implements Serializable {
	
	private transient Item inventory[];
	private int experience = 0;
	private int requis = 550;
	private int gold = 10;
	private transient HashMap<String, Equipment> equipment = new HashMap<>();
	private HashMap<Integer, Integer> levels;
	private HashMap<String, Integer> stats = new HashMap<>();
	private Quest currentquest;
	private int questStep;
	private String currentMap;
	private ArrayList<String> listFinishQuests = new ArrayList<String>();
	private transient Point2D position;
	private final int mpGrowth = 1;
	private final int atkGrowth = 2;
	private final int defGrowth = 2;
	private final int hpGrowth = 5;

	public Hero(String name) {
		super(name, 20, 20, 50);
		this.setInventory(new Item[16]);
		this.levels = new HashMap<>();
		initLevels();
		skills.add(new Heal("firstAid", 3, 40));
		skills.add(new Attack("Slash", 4, 50));
		skills.add(new Boost("Courage", 6, 30, "atk", 3));
		this.mp = 10;
		this.mpMax = 10;
		stats.put("atk", 20);
		stats.put("def", 20);
		stats.put("pv", 50);
		stats.put("mp", 10);
	}

	public void setPosition(Point2D pos) {
		this.position = pos;
	}

	public Point2D getPosition() {
		return this.position;
	}
	
	public int getGold() {
		return this.gold;
	}
	
	public void setGold(int value) {
		this.gold = value;
	}
	
	public void gainGold(int value) {
		this.gold += value;
	}

	public void gainExp(int experience) {
		this.experience = getExperience() + experience;
		while (this.experience >= this.requis) {
			gainLevel(this.experience);
			this.experience = this.experience - requis;
			this.requis = this.requis + 500;
		}
	}

	public int getExp(int level) {
		return this.levels.get(level);
	}

	public void gainLevel(int experience) {
		if (experience >= this.levels.get(this.level)) {
			this.level += 1;
			setAtk(getAtk() + atkGrowth);
			setDef(getDef() + defGrowth);
			mp = mpMax + mpGrowth;
			atkMax = atkMax + atkGrowth;
			defMax = defMax + defGrowth;
			mpMax = mpMax + mpGrowth;
			hpMax = hpMax + hpGrowth;
			setHp(getHp() + hpGrowth);
		}
	}

	public int getPositionVoid() {
		int j = 0;
		for (int i = 0; i < this.inventory.length; i++) {
			if (inventory[i] == null) {
				return j;
			}
			j++;
		}
		return (Integer) null;
	}

	public Item[] getInventory() {
		return inventory;
	}

	public void initLevels() {
		for (int i = 1; i < 50; i++) {
			this.levels.put(i, i * 500 + 50 * i * i);
		}
	}

	public void setInventory(Item inventory[]) {
		this.inventory = inventory;
	}

	public void equip(Equipment equipment) {
		if (this.equipment.get(equipment.getType()) != null) {
			unequip(this.equipment.get(equipment.getType()));
		}
		this.equipment.put(equipment.getType(), equipment);
		if (equipment.getType().equals("Arme")) {
			this.setAtk(this.getAtk() + equipment.getStat());
		} 
		else if (equipment.getType().equals("Armure")) {
			this.setDef(this.getDef() + equipment.getStat());
		}
	}

	public HashMap<String, Equipment> getEquipement() {
		return equipment;
	}

	public void setEquipment(HashMap<String, Equipment> equip) {
		this.equipment = equip;
	}

	public void unequip() {
		unequip(this.equipment.get("Arme"));
		unequip(this.equipment.get("Armure"));
	}

	public void unequip(Equipment equipment) {
		if (equipment.getType().equals("Arme")) {
			this.setAtk(this.getAtk() - equipment.getStat());
		} 
		else if (equipment.getType().equals("Armure")) {
			this.setDef(this.getDef() - equipment.getStat());
		}
		addItemInventory(equipment);
		this.equipment.remove(equipment.getType());
	}

	public void addItemInventory(Item item) {
		int index = getPositionVoid();
		inventory[index] = item;
		item.setPosition(index);
	}

	public void removeItemInventory(Item item) {
		for (int i = 0; i < this.inventory.length; i++) {
			if (inventory[i] == item) {
				inventory[i] = null;
				break;
			}
		}
	}
	public void removeItemInventory(int pos) {
		inventory[pos]=null;
	}

	public int getExperience() {
		return experience;
	}

	public void setExperience(int experience) {
		this.experience = experience;
	}

	public Quest getCurrentquest() {
		return currentquest;
	}

	public void setCurrentquest(Quest currentquest) {
		this.currentquest = currentquest;
	}

	public String getCurrentMap() {
		return currentMap;
	}

	public void setCurrentMap(String currentMap) {
		this.currentMap = currentMap;
	}

	public void rewardQuest() {
		gainExp(currentquest.getReward());
	}

	public int getLevel() {
		return level;
	}

	public void addlistFinishQuests(String finishedQuestName) {
		listFinishQuests.add(finishedQuestName);
	}

	public ArrayList<String> getFinishQuests() {
		return listFinishQuests;
	}

	public boolean finishQuest(String namequest) {
		return listFinishQuests.contains(namequest);
	}

	public void updateStats() {
		stats.replace("atk", getAtk());
		stats.replace("def", getDef());
		stats.replace("pv", getHp());
		stats.replace("mp", mp);
	}

	public HashMap<String, Integer> getStats() {
		updateStats();
		return stats;
	}

	public void givePermanently(String type, int nb) {
		switch (type) {
		case "atk":
			atkMax = (atkMax + nb);
			setAtk(getAtk() + nb);
			break;
		case "def":
			defMax = (defMax + nb);
			setDef(getDef() + nb);
			break;
		case "pv":
			hpMax = (hpMax + nb);
			setHp(getHp() + nb);
			break;
		case "mp":
			mpMax = (mpMax + nb);
			mp = (getMp() + nb);
			break;
		}
	}

	public int getQueststep() {
		return questStep;
	}

	public void setQueststep(int step) {
		questStep = step;
	}
	
	public void setMp(int value) {
		this.mp = value;
	}

	public void buy(int price) {
		this.gold -= price;
	}
}