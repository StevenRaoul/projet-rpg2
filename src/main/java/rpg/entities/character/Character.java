package rpg.entities.character;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;

import rpg.entities.abilities.Boost;
import rpg.entities.abilities.Skill;

public abstract class Character  implements Serializable {
	
	private String name;
	private int hp;
	private int atk;
	private int def;
	protected int mp;
	protected int hpMax;
	protected int atkMax;
	protected int defMax;
	protected int mpMax;
	private int spd;
	private State state;
	private String weaknesses[];
	protected int level = 1;
	protected ArrayList<Skill> skills = new ArrayList<Skill>();
	protected ArrayList<Boost> boost = new ArrayList<Boost>();
	
	public Character(String nom, int atk, int def, int hp) {
		this.setName(nom);
		this.hpMax = hp;
		this.atkMax = atk;
		this.defMax = def;
		this.setAtk(atk);
		this.setDef(def);
		this.setHp(hp);
		this.setSpd(20);
		this.setState(State.ALIVE);
	}
	
	public Character(){}
	
	public int getHp() {
		return hp;
	}
	
	public void setHp(double hp) {
		this.hp = (int) hp;
	}
	
	public int getMp() {
		return mp;
	}
	
	public int getAtk() {
		return atk;
	}
	
	public void setAtk(int atk) {
		this.atk = atk;
	}
	
	public int getDef() {
		return def;
	}
	
	public void setDef(int def) {
		this.def = def;
	}
	
	public int getHpMax() {
		return hpMax;
	}
	
	public void setHpMax(int hpMax) {
		this.hpMax = hpMax;
	}
	
	public int getMpMax() {
		return mpMax;
	}
	
	public int getAtkMax() {
		return atkMax;
	}
	
	public int getDefMax() {
		return defMax;
	}
	
	public int getLv() {
		return level;
	}
	
	public void setFullLife() {
		this.state = State.ALIVE;
		this.setHp(hpMax);
	}
	
	public State getState() {
		return state;
	}

	public void setState(State state) {
		this.state = state;
	}

	public int getSpd() {
		return spd;
	}

	public void setSpd(int spd) {
		this.spd = spd;
	}
	
	public String[] getWeaknesses() {
		return weaknesses;
	}

	public void setWeaknesses(String weaknesses[]) {
		this.weaknesses = weaknesses;
	}

	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public void useSkill(int a) {
		mp = mp - a;
	}
	
	public void useSkill(Skill skill) {
		skill.effect(this);
	}
	
	public void give(String type, int nb) {
		switch (type) {
		case "atk":
			setAtk(getAtk() + nb);
			break;
		case "def":
			setDef(getDef() + nb);
			break;
		case "pv":
			setHp(getHp() + nb);
			break;
		case "mp":
			mp = (getMp() + nb);
			break;
		}
	}

	public int givemultiply(String type, int pourcentage) {
		switch (type) {
		case "atk":
			return (getAtk() * (1 + pourcentage / 100));
		case "def":
			return (getDef() * (1 + pourcentage / 100));
		case "pv":
			return (getHp() * (1 + pourcentage / 100));
		case "mp":
			return (getMp() * (1 + pourcentage / 100));

		}
		return 0;
	}

	public void heal(double a) {
		double result = this.hpMax * (a / 100);
		if (result + this.getHp() >= this.hpMax) {
			this.setHp(this.hpMax);
		} else {
			this.setHp(this.getHp() + result);
		}
	}

	public void restore(String type, int nb) {
		switch (type) {
		case "pv":
			heal(nb);
			break;
		case "mp":
			if (getMp() + nb < mpMax) {
				mp = (getMp() + nb);
			} 
			else {
				mp = mpMax;
			}
			break;

		}
	}

	
	public ArrayList<Skill> getSkills() {
		return this.skills;
	}

	public double getPercentage(String type) {
		double percentage;
		BigDecimal percentageDecimal;
		switch (type) {
		case "hp":
			percentageDecimal = new BigDecimal(Double.toString(hp*100/hpMax));
			break;
		case "mp":
			percentageDecimal = new BigDecimal(Double.toString(mp*100/mpMax));
		default :
			percentageDecimal=new BigDecimal(Double.toString(0));
		}
		
		
		percentage = percentageDecimal.doubleValue();
		return percentage;
	}

	public void resetTemporaryBonus() {
		int finalAtk;
		int finalDef;
		if(this instanceof Hero && ((Hero)this).getEquipement().get("Arme") != null) {
			finalAtk = atkMax + ((Hero)this).getEquipement().get("Arme").getStat();
		}
		else {
			finalAtk = atkMax;
		}
		if(this instanceof Hero && ((Hero)this).getEquipement().get("Armure") != null) {
			finalDef = defMax + ((Hero)this).getEquipement().get("Armure").getStat();
		}
		else {
			finalDef = defMax;
		}
		setAtk(finalAtk);
		setDef(finalDef);
		restore("pv", 0);
		restore("mp", 0);
	}

	public ArrayList<Boost> getBoosts() {
		return boost;
	}

	public void addBoost(Boost value) {
		boost.add(value);
	}
}