package rpg.entities.character;

public enum Monsters {
	//Rats basique de la quête 1 
	RATGRIS,	//Rat commun, plus de pv que les rats classique
	RATROUX,	//Rat commun, plus d'attaque que les rats classique
	RATALBINOS,	//Rat rare, plus d'attaque et de pv que les rats classique
	BOSSRAT,		//Rat présent dans la cave, phase 1 du boss
	BOSSRATROI,			//Rat du parterre de fleur, phase 2 du boss 
	//Rats spéciaux après la quête 1
	RATSLIME,
	RATMECHA,		//Rat puissant et resistant, il ne bouge pas et attaque ceux qui passent trop proche. L'attaquer par derrière le desactive.
	RATANGE,		//Rat très puissant ayant des compétences spéciales de soin
	RATDEMON,		//Rat très puissant ayant des compétences spéciales de dégats
	RATAPOINTE,		//Rat defensif qui renvoie une partie des dégats subit
	RATSQUELETTE,	//Rat attaquant fort mais lentement. Le battre l'empeche de ressusiter pendant quelques temps, mais ne le tue pas.
}
