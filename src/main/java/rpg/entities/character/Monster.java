package rpg.entities.character;

import rpg.entities.abilities.Skill;

public class Monster extends Character{
	
	private Monsters typeMonster = Monsters.RATGRIS;
	private int give_experience = 100;
	private int give_gold = 0;
	private boolean Unique = false;
	private String quest;
	
	public Monster(String name, int atk, int def, int hp, boolean unique, String quest, Monsters type) {
		super(name, atk, def, hp);
		this.Unique = unique;
		this.setWeaknesses(new String[5]);
		this.getWeaknesses()[0] = "Balai";
		this.quest = quest;
		this.setTypeMonster(type);
	}


	public Monster(String name, int atk, int def, int hp, Monsters type) {
		super(name, atk, def, hp);
		this.setTypeMonster(type);
		this.setWeaknesses(new String[5]);
		if (type.equals(Monsters.RATGRIS)) {
			this.getWeaknesses()[0] = "Balai";
		}
	}

	public Monster(int atk, int def, int hp, int mp, int exp, int gold, Monsters type) {
		super(type.toString(), atk, def, hp);
		this.setTypeMonster(type);	
		this.mp = mp;
		this.give_experience = exp;
		this.give_gold = gold;
		this.setWeaknesses(new String[5]);
		this.getWeaknesses()[0] = "Balai";
	}
	public Monster(int atk, int def, int hp, int mp, int exp, int gold, Monsters type, Skill skill) {
		this(atk, def ,hp ,mp , exp, gold, type);
		this.skills.add(skill);
		
	}
    public Monster() {
        super();
    }
    public void addSkill(Skill skill) {
    	this.skills.add(skill);
    }
    
    public int getGive_gold() {
		return give_gold;
	}

	public void setGive_gold(int give_gold) {
		this.give_gold = give_gold;
	}

	public String getImage() {
		return "images/entities/monstres/monde/" + this.typeMonster + "/Face.png";
	}
	public String getDeathImage() {
		return "images/entities/monstres/monde/" + this.typeMonster + "/Mort.png";
	}

    public String getQuest() {
		if (this.Unique) {
			return this.quest;
		}
		else {
			return "Ce monstre n'est pas unique et par conséquent ne dispose pas de quête associée";
		}
	}
    
	public int getGive_experience() {
		return give_experience;
	}

	public void setGive_experience(int give_experience) {
		this.give_experience = give_experience;
	}

	public Monsters getTypeMonstre() {
		return typeMonster;
	}

	public void setTypeMonster(Monsters typeMonster) {
		this.typeMonster = typeMonster;
	}

	public boolean isUnique() {
		return this.Unique;
	}
	@Override
	public boolean equals(Object o) {
		return this == o;
	}
	public Monster copy() {

		Monster copiedMonster = new Monster(getAtk(), getDef(), getHp(), getMp(), getGive_experience(), getGive_gold(), getTypeMonstre());
		for (Skill skill : this.skills) {
			copiedMonster.addSkill(skill);
		}
		copiedMonster.setGive_experience(getGive_experience());
		return copiedMonster;
	}
}

