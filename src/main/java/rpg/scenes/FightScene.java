package rpg.scenes;

import static com.almasb.fxgl.dsl.FXGL.animationBuilder;
import static com.almasb.fxgl.dsl.FXGL.getSceneService;
import static com.almasb.fxgl.dsl.FXGL.getUIFactoryService;

import com.almasb.fxgl.animation.Interpolators;
import com.almasb.fxgl.scene.Scene;
import com.almasb.fxgl.scene.SubScene;
import com.almasb.fxgl.ui.FontType;

import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.geometry.Point2D;
import javafx.scene.Node;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.TextAlignment;

/**
 * @author Almas Baimagambetov (almaslvl@gmail.com)
 */
public class FightScene extends SubScene {
    public FightScene(Node node) {
        var textTutorial = getUIFactoryService().newText("Tap the circle to change ball color\n\n" +
                "Tap the left side of the screen to move left!\n\n" +
                "Tap the right side of the screen to move right!", Color.WHITE, FontType.TEXT, 20);
        textTutorial.setWrappingWidth(250);
        textTutorial.setTextAlignment(TextAlignment.LEFT);

        var background = new Rectangle(300, 230, Color.color(0.3627451f, 0.3627451f, 0.5627451f, 0.55));
        background.setArcWidth(50);
        background.setArcHeight(50);
        background.setStroke(Color.WHITE);
        background.setStrokeWidth(10);
        var stackPane = new StackPane(background, node);
        stackPane.setOnMouseClicked(new EventHandler() {

			@Override
			public void handle(Event event) {
				remove();
			}
        	
        });
        getContentRoot().getChildren().add(stackPane);
    }
    
    public void remove() {
        animationBuilder()
                .onFinished(() -> getSceneService().popSubScene())
                .interpolator(Interpolators.EXPONENTIAL.EASE_IN())
                .translate(getContentRoot())
                .from(new Point2D(0,0))
                .to(new Point2D(1200,1200))
                .buildAndPlay(FightScene.this);
    }
    
    @Override
    public void onEnteredFrom(Scene prevState) {
        animationBuilder()
                .interpolator(Interpolators.EXPONENTIAL.EASE_OUT())
                .translate(getContentRoot())
                .from(new Point2D(1200,1200))
                .to(new Point2D(0,0))
                .buildAndPlay(this);
    }
}