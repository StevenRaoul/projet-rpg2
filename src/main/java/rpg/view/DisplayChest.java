package rpg.view;

import com.almasb.fxgl.dsl.FXGL;

import javafx.scene.Node;
import javafx.scene.control.Button;

public class DisplayChest extends DisplayBasic {
	
	public void showMessage() {
		FXGL.getDialogService().showMessageBox("Tu as déja ouvert ce coffre");
	}
	
	public void showBox( Node itemImage,String chestName, Button[] buttons) {
		FXGL.getDialogService().showBox("Tu as trouvé " + chestName + " dans ce coffre veux tu l'équiper ?", itemImage, buttons);
	}
}