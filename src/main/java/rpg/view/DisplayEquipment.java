package rpg.view;

import com.almasb.fxgl.dsl.FXGL;
import com.almasb.fxgl.entity.Entity;
import com.almasb.fxgl.entity.components.ViewComponent;

import javafx.collections.ObservableMap;
import javafx.geometry.Point2D;
import javafx.scene.Node;
import javafx.scene.paint.Color;
import javafx.scene.shape.Shape;
import javafx.scene.text.Text;
import rpg.EntityType;
import rpg.RPGApp;
import rpg.eventHandler.EquipmentHandler;
import rpg.eventHandler.ItemHandler;

public class DisplayEquipment extends DisplayBasic {
	
	public Node getNodeEquipment() {
		return RPGApp.mainController.getUINode("Equipment");
	}
	
	public ObservableMap<Object, Object> getEquipment() {
		return getNodeEquipment().getProperties();
	}

	public void removeItemEquipment(CustomWindow itemSquare) {
		itemSquare.getContentPane().getChildren().clear();
	}
	
	public ViewComponent createEquipment() {
		double BG_WIDTH = 256;	//64*4
		double BG_HEIGHT = 288;	//64*4+32
		Text goldText = FXGL.getUIFactoryService().newText("Gold: " + RPGApp.hero.getGold(), Color.GOLD, 20.0);
		Entity equipmentTab = createRectangle(
				BG_WIDTH, 
				BG_HEIGHT,
				new Point2D(0, FXGL.getAppHeight() - BG_HEIGHT)
				);
		equipmentTab.setType(EntityType.EQUIPMENT);
		equipmentTab.getViewComponent().addChild(FXGL.texture("images/interface/EquipementFond.png"));
		equipmentTab.getViewComponent().getChildren().get(0).setUserData(equipmentTab);
		equipmentTab.getViewComponent().setVisible(false);
		return equipmentTab.getViewComponent();
	}
	public void addEquipmentSquare(Entity equipmentTab, CustomWindow itemSquare, String type) {
		equipmentTab.getViewComponent().addChild(itemSquare);
		equipmentTab.getViewComponent().getParent().getProperties().put(type, itemSquare);
	}
	public CustomWindow createEquipmentSquare(Point2D equipmentPosition, String equipmentType) {
		CustomWindow itemSquare = new CustomWindow();
		Shape shape = createBorder(64, 64);
		itemSquare.setShape(shape);
		itemSquare.setScaleShape(true);
		itemSquare.setPrefSize(64, 64);
		itemSquare.setUserData(equipmentPosition);
		itemSquare.getProperties().put("window", itemSquare);
		itemSquare.setAccessibleText(equipmentType);
		itemSquare.setLayoutX(equipmentPosition.getX());
		itemSquare.setLayoutY(equipmentPosition.getY());
		itemSquare.setOnMouseClicked(new EquipmentHandler());
		itemSquare.setOnMouseEntered(new ItemHandler());
		return itemSquare;
	}
}