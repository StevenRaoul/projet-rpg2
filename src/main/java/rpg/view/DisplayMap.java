package rpg.view;

import com.almasb.fxgl.dsl.FXGL;
import com.almasb.fxgl.entity.Entity;
import com.almasb.fxgl.texture.Texture;

import javafx.geometry.Point2D;
import rpg.entities.character.Monster;

public class DisplayMap extends DisplayBasic {
	
	public void spawnMap(String map) {
		FXGL.setLevelFromMap(map);
	}
	
	public void spawnClue(Point2D position) {
		FXGL.getGameWorld().spawn("indice", position);
	}
	
	public void spawnPortal(Point2D position) {
		FXGL.getGameWorld().spawn("portal", position);
	}
	
	public void spawnChest(String texturePath, Point2D position) {
		Texture texture = FXGL.texture(texturePath);
		FXGL.getGameWorld().spawn("coffre", position).getViewComponent().addChild(texture);
	}
	
	public void spawnPurchase(String texturePath, Point2D position) {
		Texture texture = FXGL.texture(texturePath);
		FXGL.getGameWorld().spawn("achat", position).getViewComponent().addChild(texture);
	}
	
	public void spawnNpc(String texturePath, Point2D position) {
		Texture texture = FXGL.texture(texturePath);
		FXGL.getGameWorld().spawn("pnj", position).getViewComponent().addChild(texture);
	}
	
	public void spawnMonster(Monster monster, Point2D position) {
		Entity monsterEntity = FXGL.getGameWorld().spawn("monstre", position);
		monsterEntity.setProperty("data", monster);
		Texture texture = FXGL.texture(monster.getImage());
		monsterEntity.getViewComponent().addChild(texture);
	}
}
