package rpg.view;

import com.almasb.fxgl.ui.MDIWindow;

public class CustomWindow extends MDIWindow {
	
	public CustomWindow(){
		super();
		super.setCanClose(false);
		super.setCanMinimize(false);
		super.setCanMove(false);
		super.setCanResize(false);
	}
}