package rpg.view;

import com.almasb.fxgl.dsl.FXGL;

import javafx.scene.Node;

public class DisplayHero extends DisplayBasic {
//	
//	public static void begin() {
//		Entity hero = createEntityWithPicture("perso.png", 32, 48);
//		Node heroView = hero.getViewComponent().getChildren().get(0);
//		heroView.setUserData(hero);
//		heroView.setAccessibleText("hero");
//		int space = 24;
//		Rectangle border = createBorder(FXGL.getSettings().getWidth() / 2, FXGL.getSettings().getHeight() / 2);
//		border.setFill(Color.rgb(0, 0, 0));
//		Entity viewStatus = createRectangleWithBorder(
//				border,
//				new Point2D(
//						RPGApp.player.getX() - FXGL.getAppWidth() / 2, 
//						RPGApp.player.getY() - FXGL.getSettings().getHeight() / 2)
//				); 
//		viewStatus.setType(EntityType.HEROSTATUS);
//<<<<<<< HEAD
//		/*addLifeBar(viewStatus.getViewComponent(), RPGApp.hero, 32, 10);
//		addManaBar(viewStatus.getViewComponent(), RPGApp.hero, 32, 30);*/
//		Text level = FXGL.getUIFactoryService().newText("Niveau : " + RPGApp.hero.getLv() + "  " + RPGApp.hero.getExperience() + "/" + RPGApp.hero.getExp(RPGApp.hero.getLv()), Color.WHITE, 15.0);
//=======
//		Text level = FXGL.getUIFactoryService().newText(
//				"Niveau : " + RPGApp.hero.getLv() + "  " + RPGApp.hero.getExperience() + "/" + RPGApp.hero.getExp(RPGApp.hero.getLv()), 
//				Color.WHITE, 
//				15.0);
//>>>>>>> main
//		Entity levelView = CreateEntityWithNode(level, 272, 32);	//256+16
//		levelView.getViewComponent().getChildren().get(0).setAccessibleText("level");
//		levelView.getViewComponent().getChildren().get(0).setUserData(level);
//		viewStatus.getViewComponent().addChild(levelView.getViewComponent().getChildren().get(0));
//		Text equipmentTitle = FXGL.getUIFactoryService().newText("Equipement :", Color.WHITE, 15.0);
//		Entity equipmentTitleView = CreateEntityWithNode(equipmentTitle, 272, 60);	//256+16 & 32+28
//		int equipmentGap = 2;
//		String[] equipmentType = new String[2];
//		equipmentType[0] = "Arme";
//		equipmentType[1] = "Armure";
//		for (String type : equipmentType) {
//			String name = "";
//			if (RPGApp.hero.getEquipement().containsKey(type)) {
//				name = RPGApp.hero.getEquipement().get(type).getName();
//			}
//			Text equipmentText = FXGL.getUIFactoryService().newText(type + " : " + name, Color.WHITE, 15.0);
//			Entity equipmentView = CreateEntityWithNode(equipmentText, 272, 32 + space * equipmentGap); //256+16
//			equipmentView.getViewComponent().getChildren().get(0).setUserData(equipmentText);
//			equipmentView.getViewComponent().getChildren().get(0).setAccessibleText(type);
//			viewStatus.getViewComponent().addChild(equipmentView.getViewComponent().getChildren().get(0));
//			equipmentGap++;
//		}
//		Text statsTitle = FXGL.getUIFactoryService().newText("Stats :", Color.WHITE, 15.0);
//		Entity statsTitleView = CreateEntityWithNode(statsTitle, 272, 32 + space * equipmentGap);	//256+16
//		viewStatus.getViewComponent().addChild(statsTitleView.getViewComponent().getChildren().get(0));
//		equipmentGap++;
//		for (Entry<String, Integer> stat : RPGApp.hero.getStats().entrySet()) {
//			String maxValue = "";
//			if (stat.getKey().equals("pv")) {
//				maxValue = "/" + RPGApp.hero.getHpMax();
//			}
//			else if (stat.getKey().equals("mp")) {
//				maxValue = "/" + RPGApp.hero.getMpMax();
//			}
//			Text statText = FXGL.getUIFactoryService().newText(stat.getKey() + " : " + stat.getValue() + maxValue, Color.WHITE, 15.0);
//			Entity statView = CreateEntityWithNode(statText, 272, 32 + space * equipmentGap);	//256+16
//			statView.getViewComponent().getChildren().get(0).setUserData(stat);
//			statView.getViewComponent().getChildren().get(0).setAccessibleText(stat.getKey());
//			viewStatus.getViewComponent().addChild(statView.getViewComponent().getChildren().get(0));
//			equipmentGap++;
//		}
//		viewStatus.getViewComponent().addChild(equipmentTitleView.getViewComponent().getChildren().get(0));
//		viewStatus.getViewComponent().addChild(heroView);
//		FXGL.getGameWorld().addEntity(viewStatus);
//		viewStatus.getViewComponent().getChildren().get(0).setVisible(false);
//	}
//	
//	public static Entity getStatusWindow() {
//		return findEntity(
//				new Point2D(
//						RPGApp.player.getX() - FXGL.getSettings().getWidth() / 2,
//						RPGApp.player.getY() - FXGL.getSettings().getHeight() / 2), 
//				EntityType.HEROSTATUS);
//	}
//	
//	public static void removeStatusWindow() {
//		findEntity(
//				new Point2D(
//						RPGApp.player.getX() - FXGL.getSettings().getWidth() / 2,
//						RPGApp.player.getY() - FXGL.getSettings().getHeight() / 2), 
//				EntityType.HEROSTATUS).getViewComponent().getChildren().get(0).setVisible(false);
//		MusicComponent.soundPlay("open");
//	}
//	
//	public static void displayStatusWindow() {
//		findEntity(
//				new Point2D(
//						RPGApp.player.getX() - FXGL.getSettings().getWidth() / 2,
//						RPGApp.player.getY() - FXGL.getSettings().getHeight() / 2), 
//				EntityType.HEROSTATUS).getViewComponent().getChildren().get(0).setVisible(true);
//		MusicComponent.soundPlay("open");
//	}
//	
//	public static void update() {
//		Entity viewStatus = getStatusWindow();
//		boolean bar = true;
//		updateSkills(viewStatus);
//		for (Node statusNode : viewStatus.getViewComponent().getChildren()) {
//			if (statusNode.getAccessibleText() != null) {
//				if (RPGApp.hero.getStats().containsKey(statusNode.getAccessibleText())) {
//					updateStatsText(statusNode);	
//				}
//				else if (RPGApp.hero.getEquipement().containsKey(statusNode.getAccessibleText())) {
//					updateEquipmentText(statusNode);
//				}
//				else if (statusNode.getAccessibleText().equals("level")) {
//					updateLvlText(statusNode);
//				}
//				else if (statusNode.getAccessibleText().equals("border1" + RPGApp.hero.toString()) && bar) {
//					updateLifeBar(RPGApp.hero, statusNode, "red");
//				} 
//				else if (statusNode.getAccessibleText().equals("border2" + RPGApp.hero.toString()) && bar) {
//					bar = false;
//					updateLifeBar(RPGApp.hero, statusNode, "green");
//				}
//				else if (statusNode.getAccessibleText().equals("border1" + RPGApp.hero.toString()) && bar == false) {
//					updateManaBar(RPGApp.hero, statusNode, "red");
//				} 
//				else if (statusNode.getAccessibleText().equals("border2" + RPGApp.hero.toString()) && bar == false) {
//					updateManaBar(RPGApp.hero, statusNode, "blue");
//				}
//			}
//		}
//	}
//	
//	public static void updateSkills(Entity viewStatus) {
//		ArrayList<Skill> skillListe = new ArrayList<Skill>();
//		for (Skill skill : RPGApp.hero.getSkills()) {
//			if (skill.canInFight() == false) {
//				skillListe.add(skill);
//			}
//		} 
//	}
//	
//	public static void updateStats(Entity viewStatus) {
//		for (Node statNode : viewStatus.getViewComponent().getChildren()) {
//			if (statNode.getAccessibleText() != null) {
//				if (RPGApp.hero.getStats().containsKey(statNode.getAccessibleText())) {
//					updateStatsText(statNode);	
//				}
//			}
//		}
//	}
//	
//	public static void updateStatsText(Node statNode) {
//		HashMap<String, Integer> stats = RPGApp.hero.getStats();
//		String maxValue = "";
//		if (statNode.getAccessibleText().equals("pv")) {
//			maxValue = "/" + RPGApp.hero.getHpMax();
//		}
//		else if (statNode.getAccessibleText().equals("mp")) {
//			maxValue = "/" + RPGApp.hero.getMpMax();
//		}
//		((Text) statNode.getUserData()).setText(statNode.getAccessibleText() + " : " + stats.get(statNode.getAccessibleText()) + maxValue);
//	}
//	
//	public static void updateEquipment(Entity viewStatus) {
//		for (Node statusNode : viewStatus.getViewComponent().getChildren()) {
//			if (statusNode.getAccessibleText() != null) {
//				if (RPGApp.hero.getEquipement().containsKey(statusNode.getAccessibleText())) {
//					updateEquipmentText(statusNode);
//				}
//			}
//		}
//	}
//	
//	public static void updateEquipmentText(Node equipmentNode) {
//		HashMap<String, Equipment> equipment = RPGApp.hero.getEquipement();
//		((Text) equipmentNode.getUserData()).setText(equipmentNode.getAccessibleText() + " : " + 
//				equipment.get(equipmentNode.getAccessibleText()).getName());
//	}
//	
//	public static void updateLvl(Entity viewStatus) {
//		for (Node lvlNode : viewStatus.getViewComponent().getChildren()) {
//			if (lvlNode.getAccessibleText() != null) {
//				if (lvlNode.getAccessibleText().equals("level")) {
//					updateLvlText(lvlNode);
//				}
//			}
//		}
//	}
//	
//	public static void updateLvlText(Node lvlNode) {
//		((Text) lvlNode.getUserData()).setText("Niveau : " + RPGApp.hero.getLv() + "  " + RPGApp.hero.getExperience() 
//		+ "/" + RPGApp.hero.getExp(RPGApp.hero.getLv()));
//	}
//	
//	public static void updatebars(Entity viewStatus) {
//		Boolean bar = true;
//		for (Node statusNode : viewStatus.getViewComponent().getChildren()) {
//			if (statusNode.getAccessibleText() != null) {
//				if (statusNode.getAccessibleText().equals("border1" + RPGApp.hero.toString()) && bar) {
//					updateLifeBar(RPGApp.hero, statusNode, "red");
//				} 
//				else if (statusNode.getAccessibleText().equals("border2" + RPGApp.hero.toString()) && bar) {
//					bar = false;
//					updateLifeBar(RPGApp.hero, statusNode, "green");
//				}
//				else if (statusNode.getAccessibleText().equals("border1" + RPGApp.hero.toString()) && bar == false) {
//					updateManaBar(RPGApp.hero, statusNode, "red");
//				} 
//				else if (statusNode.getAccessibleText().equals("border2" + RPGApp.hero.toString()) && bar == false) {
//					updateManaBar(RPGApp.hero, statusNode, "blue");
//				}
//			}
//		}
//	}
//	
	
	public Node getInitImage() {
		Node view = FXGL.texture("images/entities/joueur/monde/HerosFace.png");
		view.setLayoutX(((double) FXGL.getAppWidth() / 2));
		view.setLayoutY(((double) FXGL.getAppHeight() / 2));
		return view;
	}
//	
//<<<<<<< HEAD
////	public class test {
////  	  public ObjectProperty<Paint> couleur;
////  	  public StringProperty fond;
////  	  public BooleanProperty disable;
////  	  
////  	  public test(ObjectProperty<Paint> couleur,StringProperty fond,BooleanProperty disable) {
////  		  this.couleur=couleur;
////  		  this.fond=fond;
////  		  this.disable=disable;
////  	  }
////    }
//	
//	public static void addLifeBar(ViewComponent viewComponent, String character, double posX, double posY) {
//		//double percentage = ((character.getHpMax() - character.getHp()) * 100 / character.getHpMax());
//		//DisplayBasic.addbar(viewComponent, character, posX, posY, 100, Color.rgb(0, 254, 0, 0.8));
//	}
//	
//	public static void updateLifeBar(Character character, Node node, String color) {
//		double percentage = ((character.getHpMax() - character.getHp()) * 100 / character.getHpMax());
//		//DisplayBasic.updateBar(character, node, color, percentage);
//	}
//	
//	public static void addManaBar(ViewComponent viewComponent, String character, double posX, double posY) {
//		//double percentage = ((character.getMpMax() - character.getMp()) * 100 / character.getMpMax());
//		//DisplayBasic.addbar(viewComponent, character, posX, posY, 100, Color.rgb(0, 0, 254, 0.8));
//=======
//	public static void updateLifeBar(Character character, Node node, String color) {
//		double percentage = ((character.getHpMax() - character.getHp()) * 100 / character.getHpMax());
//>>>>>>> main
//	}
//	
//	public static void updateManaBar(Character character, Node node, String color) {
//		double percentage = ((character.getMpMax() - character.getMp()) * 100 / character.getMpMax());
//<<<<<<< HEAD
//		//DisplayBasic.updateBar(character, node, color, percentage);
//=======
//>>>>>>> main
//	}
}