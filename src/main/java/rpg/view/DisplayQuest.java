package rpg.view;

import java.util.Map.Entry;

import com.almasb.fxgl.dsl.FXGL;
import com.almasb.fxgl.entity.Entity;

import javafx.collections.ObservableMap;
import javafx.geometry.Point2D;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import rpg.EntityType;
import rpg.RPGApp;
import rpg.system.Quest;
import rpg.system.QuestTarget;

public class DisplayQuest extends DisplayBasic {
	
	public static ObservableMap<Object, Object> getQuestProperties() {
		Node tabQuest = getQuest();
		return ((Parent) tabQuest).getProperties();
	}
	
	public static Node getQuest() {
		return FXGL.getGameScene().getUINodes().get(1);
	}
	
	public void updateQuest(Quest quest) {
		
		for (Entry<Object, Object> entry : getQuestProperties().entrySet()) {
			String newText = "";
			Node line = (Node) entry.getValue();
			String textName = line.getAccessibleText();
			if (textName != null && quest != null) {
				switch (textName) {
					case "Nom":
						newText = quest.getName();
						break;
					case "Objectif":
						if (quest.getTypeTarget() == QuestTarget.CLUE) {
							newText = " - Trouver des indices : " + quest.getNbTarget() + "  /  " + quest.getNbTargetMax();
						}
						else if (quest.getTypeTarget() == QuestTarget.KILL) {
							newText = " -  Tuer des rats : " + quest.getNbTarget() + "  /  " + quest.getNbTargetMax();
						}
						else {
							newText = " - Parler à " + quest.getTargetToSee();
						}
						break;
					case "Recompense":
						newText = " -  " + quest.getReward() + "  Xp";
						break;
					case "Description":
						newText = " -  " + quest.getDescription();
						break;			
				}
			}
			if(textName == "Nom" || textName == "Description" || textName == "Objectif" || textName == "Recompense") {
				((Text) line).setText(newText);
			}
		}
	}

	public static Parent createQuest() {
		double BG_WIDTH = 256;	//64*4
		double BG_HEIGHT = 192;	//64*3
		Entity tabQuest = createRectangle(
				BG_WIDTH, 
				BG_HEIGHT,
				new Point2D(FXGL.getAppWidth() - BG_WIDTH, 0)
			);
		tabQuest.setType(EntityType.QUESTBOARD);
		tabQuest.getViewComponent().addChild(FXGL.texture("images/interface/QueteFond.png"));
		Quest quest = RPGApp.hero.getCurrentquest();
		
		Text nameText = FXGL.getUIFactoryService().newText("", Color.GOLD, 20.0);
		Text descriptionText = FXGL.getUIFactoryService().newText("", Color.WHITE, 20.0);
		Text goalText = FXGL.getUIFactoryService().newText("", Color.WHITE, 20.0);
		Text rewardText = FXGL.getUIFactoryService().newText("", Color.WHITE, 20.0);
		
		if (quest != null) {
			nameText.setText(quest.getName());
			descriptionText.setText("- " + quest.getDescription());
			if (quest.getAction() == "Voir" ) {
				goalText.setText("- " + quest.getAction() + "  " + quest.getTypeTarget());
			}
			else {
				goalText.setText("-  "+ quest.getAction() + "  " + quest.getNbTarget() + 
						"  /  " + quest.getNbTargetMax() + "  " + quest.getTypeTarget());
			}
			rewardText.setText("-  " + quest.getReward() + "  Xp");
		}
		nameText.setWrappingWidth(BG_WIDTH - 15);
		nameText.fontProperty().unbind();
		nameText.setFont(Font.font("Elephant", FontWeight.BOLD, FontPosture.REGULAR, 14));
		
		descriptionText.setWrappingWidth(BG_WIDTH - 20);
		descriptionText.fontProperty().unbind();
		descriptionText.setFont(Font.font("Elephant", FontWeight.BLACK, FontPosture.REGULAR, 11));
		
		goalText.setWrappingWidth(BG_WIDTH - 20);
		goalText.fontProperty().unbind();
		goalText.setFont(Font.font("Elephant", FontWeight.BLACK, FontPosture.REGULAR, 11));
		
		rewardText.setWrappingWidth(BG_WIDTH - 20);
		rewardText.fontProperty().unbind();
		rewardText.setFont(Font.font("Elephant", FontWeight.BLACK, FontPosture.REGULAR, 11));
		
		setPosition(nameText, 10, 32);
		setPosition(descriptionText, 15, 105);
		setPosition(goalText, 15, 60);
		setPosition(rewardText, 15, 80);
		
		nameText.setAccessibleText("Nom");
		descriptionText.setAccessibleText("Description");
		goalText.setAccessibleText("Objectif");
		rewardText.setAccessibleText("Recompense");

		tabQuest.getViewComponent().addChild(nameText);
		tabQuest.getViewComponent().addChild(descriptionText);
		tabQuest.getViewComponent().addChild(goalText);
		tabQuest.getViewComponent().addChild(rewardText);
		tabQuest.getViewComponent().setVisible(false);
		tabQuest.getViewComponent().getParent().getProperties().put("nom", nameText);
		tabQuest.getViewComponent().getParent().getProperties().put("desc", descriptionText);
		tabQuest.getViewComponent().getParent().getProperties().put("obj", goalText);
		tabQuest.getViewComponent().getParent().getProperties().put("rec",rewardText);
		return tabQuest.getViewComponent().getParent();
	}
	
	public static void setPosition(Node node, int posX, int posY) {
		node.setLayoutX(posX);
		node.setLayoutY(posY);	
	}
}