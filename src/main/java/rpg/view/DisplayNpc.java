package rpg.view;

import java.util.HashMap;

import com.almasb.fxgl.dsl.FXGL;
import com.almasb.fxgl.entity.Entity;

import javafx.geometry.Point2D;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;
import rpg.RPGApp;
import rpg.controler.NpcComponent;
import rpg.entities.character.Npc;
import rpg.eventHandler.NpcHandler;

public class DisplayNpc extends DisplayBasic {
	private NpcComponent npcController;
	
	public void init(NpcComponent npcController) {
		this.npcController=npcController;
	}
	public void dialog(Npc npc, String step) {
		Button[] listButton;
		HashMap<String, String[]> chat = npc.getListChat().get(step);
		System.out.println(chat);
		String[] textButton = chat.get("answers");
		listButton = new Button[textButton.length];
		for (int i = 0; i < textButton.length; i++) {
			listButton[i] =  new Button(textButton[i]);
			listButton[i].setOnAction(new NpcHandler(listButton[i], npc,npcController));
		}
		if (RPGApp.dialogBox == null) {
			Entity npcEntity = FXGL.entityBuilder()
					.view(npc.getImage())
					.at(0, 0)
					.build();
			RPGApp.dialogBox = initDialogBox(FXGL.texture(npc.getImage()), npc.getName(), listButton, chat.get("message")[0]);
		}
		else {
			System.out.println(chat.get("message")[0]);
			showBox(RPGApp.dialogBox, listButton, chat.get("message")[0]);
		}
	}

	public static void rotate(Point2D position, String angle) {
		String newAngle = "";
		String name = RPGApp.listeMaps.get(RPGApp.hero.getCurrentMap()).getPNJList().get(position).getName();
		switch(angle) {
			case "Dos":
				newAngle = "Face";
				break;
			case "Face":
				newAngle = "Dos";
				break;
			case "Gauche":
				newAngle = "Droite";
				break;
			case "Droite":
				newAngle = "Gauche";
				break;
		}
		FXGL.getGameWorld().getEntitiesAt(position).get(0).getViewComponent()
			.addChild(FXGL.texture("images/entities/pnjs/monde/" + name + "/" + name + "_" + newAngle + ".png"));
	}
	
	private static Entity initDialogBox(Node npcView, String npcName, Button[] answers, String npcText) {
		Rectangle border = createBorder(FXGL.getSettings().getWidth(), 192);
		border.setFill(Color.rgb(0, 0, 0));
		Entity dialogBox = createRectangleWithBorder(
				border,
				new Point2D(
						RPGApp.player.getX() - FXGL.getSettings().getWidth() / 2,
						RPGApp.player.getY() - FXGL.getSettings().getHeight() / 2)
				);
		Label dialogTitle = new Label("Dialogue entre " + npcName + " et vous");
		dialogTitle.setTextFill(Color.rgb(254, 254, 254));
		Text npcNameText = FXGL.getUIFactoryService().newText(npcName + " :", Color.WHITE, 20.0);
		Text npcMessageText = FXGL.getUIFactoryService().newText(npcText, Color.WHITE, 15.0);
		double size = FXGL.getSettings().getWidth() / 5;	//15*64/192
		npcNameText.setLayoutX(FXGL.getSettings().getWidth() / 2 - size);
		npcNameText.setLayoutY(32);
		npcMessageText.setLayoutX(FXGL.getSettings().getWidth() / 2 - size);
		npcMessageText.setLayoutY(64);
		npcMessageText.getProperties().put("AccesibleText", "dialog");
		dialogBox.getViewComponent().addChild(npcMessageText);
		int answerSize = 0;
		int widthGap = 0;
		int heightGap = 1;
		String[] line = npcText.split("");
		for (int i = 0; i < npcText.length(); i++) {
			if (line[i].equals("\n")){
				heightGap++;
			}
		}
		for (Button answer : answers) {
			answerSize = answer.getText().length();
			answer.setLayoutX(FXGL.getSettings().getWidth() / 2 - size + 64 * widthGap + (10 * answerSize) * (widthGap));
			answer.setLayoutY(20 * heightGap + 60);
			answer.getProperties().put("AccesibleText", "bouton");
			dialogBox.getViewComponent().addChild(answer);
			widthGap++;
		}
		dialogBox.getViewComponent().addChild(npcNameText);
		dialogBox.getViewComponent().addChild(npcView);
		FXGL.getGameWorld().addEntity(dialogBox);
		return dialogBox;
	}
	
	private static void showBox(Entity dialogBox, Button[] answers, String npcText) {
		int answerIndex = 0;
		for (Node node : dialogBox.getViewComponent().getChildren()) {
			if (node.getProperties().get("AccesibleText") != null) {
				if (node.getProperties().get("AccesibleText").equals("dialog")) {
					Text npcMessageText = (Text) node;
					npcMessageText.setText(npcText);
				}
				if (node.getProperties().get("AccesibleText").equals("bouton")) {
					Button answer = (Button) node;
					String[] line = npcText.split("");
					int heightGap = 1;
					for (int i = 0; i < npcText.length(); i++) {
						if (line[i].equals("\n")) {
							heightGap++;
						}
					}   
					answer.setLayoutY(heightGap * 20 + 60);
					if (answerIndex < answers.length) {
						if (answer.isVisible() == false) {
							answer.setVisible(true);
						}
						answer.setText(answers[answerIndex].getText());
					}
					else {
						if (answer.isVisible()) {
							answer.setVisible(false);
						}
					}
					answerIndex++;
				}
			}
		}
	}
}