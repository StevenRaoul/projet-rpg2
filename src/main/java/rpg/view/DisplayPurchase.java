package rpg.view;

import java.util.HashMap;

import com.almasb.fxgl.dsl.FXGL;

import javafx.geometry.Point2D;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;

public class DisplayPurchase extends DisplayBasic {
	
	private HashMap<Point2D,CustomWindow> windows= new HashMap<>();
	
	public void createPurchase(Point2D position, Text titlePurchase, Text descriptionPurchase, Text pricePurchase) {
		int width = 220;
		int height = 95;
		Rectangle border = new Rectangle(0, 0, width, height);
		border.setFill(Color.rgb(0, 0, 0, 1));
		CustomWindow window = new CustomWindow();
		window.setScaleShape(true);
		window.setPrefSize(width, height);
		window.setShape(border);
		Pane textContent = new Pane();
		textContent.getChildren().add(titlePurchase);
		textContent.getChildren().add(descriptionPurchase);
		textContent.getChildren().add(pricePurchase);
		window.setContentPane(textContent);
		window.setLayoutX(+64);
		window.setLayoutY(-100);
		window.setMouseTransparent(true);
		FXGL.getGameWorld().getEntitiesAt(position).get(0).getViewComponent().addChild(window);
		this.windows.put(position, window);
		window.setVisible(false);
	}
	
	public void lookPurchase(Point2D position) {
		this.windows.get(position).setVisible(true);
	}
	
	public void quitPurchase(Point2D position) {
		this.windows.get(position).setVisible(false);
	}

	public void leaveTheShop(Point2D position) {
		FXGL.getGameWorld().removeEntities(FXGL.getGameWorld().getEntitiesAt(position));
	}
}
