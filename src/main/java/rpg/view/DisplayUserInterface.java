package rpg.view;

import com.almasb.fxgl.dsl.FXGL;

import javafx.scene.Node;

public class DisplayUserInterface {
	
	
	public void addUINode(Node node) {
		FXGL.getGameScene().addUINode(node);
	}
	
	public Node getUINode(int index) {
		return FXGL.getGameScene().getUINodes().get(index);
	}
	
	public void changeVisibilityUINode(int index) {
		Node node = getUINode(index);
		node.setVisible(!node.isVisible());
	}
}
