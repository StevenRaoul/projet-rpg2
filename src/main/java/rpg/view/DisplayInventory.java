package rpg.view;

import com.almasb.fxgl.dsl.FXGL;
import com.almasb.fxgl.entity.Entity;
import com.almasb.fxgl.entity.components.ViewComponent;

import javafx.collections.ObservableMap;
import javafx.geometry.Point2D;
import javafx.scene.Node;
import javafx.scene.shape.Shape;
import rpg.RPGApp;
import rpg.controler.UserInterfaceController;
import rpg.entities.items.Item;
import rpg.eventHandler.InventoryHandler;
import rpg.eventHandler.ItemHandler;

public class DisplayInventory extends DisplayBasic {
	private UserInterfaceController userInterfaceController;
	
	
	public void setUserInterfaceController(UserInterfaceController userInterfaceController) {
		this.userInterfaceController=userInterfaceController;
	}
	public Node getNodeInventory() {
		return RPGApp.mainController.getUINode("Inventory");
	}
	
	public ObservableMap<Object, Object> getInventory() {
		return getNodeInventory().getProperties();
	}
	
	
	public void removeItem(int index) {
		CustomWindow inventorySquare = ((CustomWindow) getInventory().get(index));
		((CustomWindow) inventorySquare.getProperties().get("window")).getContentPane().getChildren().clear();
	}

	public void addItem(Item item, int index) {
		initItemViewInventory(item, (CustomWindow) getInventory().get(index), "equip");
	}

	

	public ViewComponent createInventory() {
		double BG_WIDTH = 272;	//64 * 4 + 16
		double BG_HEIGHT = 288;	//64 * 4 + 32
		Entity inventory = createRectangle(
				BG_WIDTH, 
				BG_HEIGHT,
				new Point2D(
						FXGL.getAppWidth() - BG_WIDTH,
						FXGL.getAppHeight() - BG_HEIGHT)
				);
		inventory.getViewComponent().addChild(FXGL.texture("images/interface/InventaireFond.png"));
		inventory.getViewComponent().setVisible(false);
		return inventory.getViewComponent();
	}
	
	public CustomWindow createInventorySquare(int lineSquare, int columnSquare) {
		CustomWindow inventorySquare = new CustomWindow();
		Shape shape = createBorder(58, 58);
		inventorySquare.setShape(shape);
		inventorySquare.setScaleShape(true);
		inventorySquare.setPrefSize(64, 64);
		inventorySquare.setUserData(new Point2D(64 * lineSquare + 8, 64 * columnSquare + 32));
		inventorySquare.getProperties().put("window", inventorySquare);
		inventorySquare.setLayoutX(64 * lineSquare + 8);
		inventorySquare.setLayoutY(64 * columnSquare + 32);
		inventorySquare.setOnMouseClicked(new InventoryHandler());
		inventorySquare.setOnMouseEntered(new ItemHandler());
		return inventorySquare;
	}
	public void addInventorySquare(CustomWindow inventorySquare ,Entity inventory,int pos) {
		
		inventory.getViewComponent().addChild(inventorySquare);
		inventory.getViewComponent().getParent().getProperties().put(pos, inventorySquare);
	}
}