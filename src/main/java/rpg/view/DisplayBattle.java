package rpg.view;

import java.util.ArrayList;

import com.almasb.fxgl.audio.Music;
import com.almasb.fxgl.dsl.FXGL;
import com.almasb.fxgl.entity.Entity;

import javafx.geometry.Point2D;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import rpg.controler.FightController;
import rpg.entities.abilities.Skill;
import rpg.eventHandler.BattleHandler;

public class DisplayBattle extends DisplayBasic {
	
	public static Music musicBoss;
	private static Entity combatView;
	private Entity monsterEntity;
	
	public void updateMonsterToDeathImage(String updatePath) {
		Node newImage = FXGL.texture(updatePath);
		updateNode(monsterEntity.getViewComponent().getChildren().get(0),newImage,monsterEntity.getViewComponent());
	}
	
	public void setMonster(Entity monsterEntity) {
		this.monsterEntity=monsterEntity;
	}
	private Rectangle createCombatViewBorder() {
		Rectangle border = createBorder(FXGL.getSettings().getWidth(), FXGL.getSettings().getHeight());
		border.setFill(Color.rgb(0, 0, 0));
		return border;
	}
	
//	public Rectangle createCombatViewBorder(String path) {
//		Rectangle border = createBorder(FXGL.getSettings().getWidth(), FXGL.getSettings().getHeight());
//		border.setFill(Color.rgb(0, 0, 0));
//		///border.set
//		return border;
//	}
	
	
	public void createCombatView() {	
		combatView = createRectangleWithBorder(
				createCombatViewBorder(),
				new Point2D(
						0,0
						)
				);
	}
	
	public void addView(String texture, Point2D pos, String property) {
		Node View = FXGL.texture(texture);
		View.setLayoutX(pos.getX());
		View.setLayoutY(pos.getY());
		combatView.getViewComponent().addChild(View);
		combatView.getProperties().setValue(property,View);
	}
	
	public void addBar(String character, Point2D pos, Color color) {
		addbar(combatView.getViewComponent(),character,pos.getX(),pos.getY(),color);
	}
	
	public void addText(Point2D pos, String property) {
		Label label = new Label("Bienvenue");
		label.setTextFill(Color.rgb(254, 254, 254));
		label.setLayoutX(pos.getX());
		label.setLayoutY(pos.getY());
		combatView.getViewComponent().addChild(label);
		combatView.getProperties().setValue(property, label);
	}
	
	public void addButtons(String[] buttonsName, FightController fightController) {
		int buttonGap = 0;
		for (String name : buttonsName) {
			Button bouton= new Button(name);
			bouton.setLayoutX(192 + 64*buttonGap);
			bouton.setLayoutY(385);
			bouton.setOnAction(new BattleHandler(fightController));
			combatView.getViewComponent().addChild(bouton);
			combatView.getProperties().setValue(name, bouton);
			buttonGap++;
		}
	}
	
	public void updateChoiceButton(String property, String choice) {
		((BattleHandler) ((Button) combatView.getProperties().getObject(property)).getOnAction()).update(choice);
	}
	
	public void addMenu(ArrayList<Skill> skills, FightController fightController) {
		Menu menu = new Menu();
		for (Skill skill : skills) {
			MenuItem menuItem = new MenuItem();
			menuItem.setText(skill.getName() + " " + skill.getCost());
			menu.getItems().add(menuItem);
			menuItem.setOnAction(new BattleHandler(fightController,"skills"));
			menuItem.setUserData(skill);
		}
		MenuBar menuBar = new MenuBar();
		menu.setText("Skills");
		menuBar.getMenus().add(menu);
		BorderPane borderPane = new BorderPane();
		borderPane.setTop(menuBar);
		borderPane.setLayoutX(448);
		borderPane.setLayoutY(385);
		combatView.getViewComponent().addChild(borderPane);
		combatView.getProperties().setValue("skills", menu);
	}
	
	public void updateTextButton(String property, String text) {
		((Button) combatView.getProperties().getObject(property)).setText(text);
	}
	
	public void setVisibility(String property, boolean visibility) {
		((Node) combatView.getProperties().getObject(property)).setVisible(visibility);
	}
	
	public void setVisibilityMenu(String property, boolean visibility){
		((Menu) combatView.getProperties().getObject(property)).setVisible(visibility);
	}
	
	public void updateLabelText(String property, String text) {
		((Label) combatView.getProperties().getObject(property)).setText(text);
	}
	
	public void updateEventButton() {}
	
	public void updateDisabledSkills(ArrayList<String> disabledSkills) {
		Menu skills = combatView.getProperties().getObject("skills");
		for (MenuItem skill : skills.getItems()) {
			if (disabledSkills.contains(skill.getText())) {
				skill.setDisable(true);
				skill.setStyle("-fx-background-color: #A0A0A0;");
			}
		
		}
	}
	
	public Node getFightNode() {
		return combatView.getViewComponent().getParent();
	}
	
	public void updateBar(double percentage, String color, String property) {
		updateBar(combatView.getProperties().getObject(property), color, percentage);
	}
	
	public void updateView(String updatePath, String property) {
		Node newImage = FXGL.texture(updatePath);
		updateNode(combatView.getProperties().getObject(property), newImage, combatView.getViewComponent());
		combatView.getProperties().setValue(property, newImage);
	}	
}