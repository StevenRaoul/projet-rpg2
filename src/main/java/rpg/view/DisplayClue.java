package rpg.view;

import com.almasb.fxgl.dsl.FXGL;

import javafx.geometry.Point2D;
import javafx.scene.Node;
import javafx.scene.control.Button;
import rpg.RPGApp;
import rpg.entities.objects.Clue;

public class DisplayClue extends DisplayBasic {
	
	public static void findClue(Point2D position) {
		Clue clue = RPGApp.listeMaps.get(RPGApp.hero.getCurrentMap()).getClueList().get(position);
		Node imageView = FXGL.texture(clue.getImage());        
		displayClue(imageView, clue, position);
	}
	
	public static void displayClue(Node imageView, Clue clue, Point2D position) {
		Button[] button = new Button[1];
		button[0] = new Button("Continuer");
		FXGL.getDialogService().showBox("Tu as trouvé " + clue.getName() + ".", imageView, button);
	}
}