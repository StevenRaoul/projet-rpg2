package rpg.view;

import java.math.BigDecimal;
import java.util.ArrayList;

import com.almasb.fxgl.dsl.FXGL;
import com.almasb.fxgl.entity.Entity;
import com.almasb.fxgl.entity.components.ViewComponent;

import javafx.geometry.Point2D;
import javafx.scene.Node;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;
import rpg.EntityType;
import rpg.RPGApp;
import rpg.entities.items.Item;

public abstract class DisplayBasic {
	
	public static Entity CreateEntityWithNode(Node node, double posX, double posY) {
		Entity affiche = FXGL.entityBuilder()
				.at(posX, posY)
				.view(node)
				.build();
		return affiche;
	}
	public Node getBigView(Item item) {
		return FXGL.texture(item.getImage());
	}
	public void initItemViewInventory(Item item, CustomWindow itemSquare, String action) {
		Pane pane = new Pane();
		pane.setOpacity(10);
		pane.getChildren().add(FXGL.texture(item.getInventaireImage()));
		pane.setLayoutY(-24);
		itemSquare.getProperties().put("pane", pane);
		itemSquare.setContentPane(pane);
	}
	public static void updateNode(Node before, Node after, ViewComponent component) {
		if (before != null && after != null && component != null) {
			component.removeChild(before);
			after.setLayoutX(before.getLayoutX());
			after.setLayoutY(before.getLayoutY());
			component.addChild(after);
		} 
		else {
			System.out.println("Un des elements est null a toi de deviner lequel !");
		}
	}
	
	public static Entity createEntityWithPicture(String picture, double posX, double posY) {
		Entity affiche = FXGL.entityBuilder()
				.at(posX, posY)
				.view(picture)
				.build();
		return affiche;
	}
	
	protected static  Rectangle createBorder(double width, double height) {
        Rectangle border = new Rectangle(width, height);
        border.setStrokeWidth(2.0);
        border.setArcWidth(10.0);
        border.setArcHeight(10.0);
        border.setFill(Color.rgb(0, 0, 0, 0.8));
        return border;
	}
	
	protected static  Entity createRectangleWithBorder(Rectangle border, Point2D position) {
        Entity rectangleView = FXGL.entityBuilder()
        		.at(position)
                .view(border)
                .build();
        return rectangleView;
    }
	
	protected static  Entity createRectangleWithBorder(Rectangle border, Point2D position, EntityType type) {
        Entity rectangleView = FXGL.entityBuilder()
                .at(position)
                .type(type)
                .view(border)
                .build();
        return rectangleView;
    }
	
	protected static  Entity createRectangle(double width, double height, Point2D position) {    
	        Rectangle border = createBorder(width, height);
	        Entity rectangleView = createRectangleWithBorder(border, position);
	        return rectangleView;
	}
	
	public static Entity createNotif(String message) {
		message = returnLine(message, 50);
		String[] line = message.split("");
		int nbLine = 1;
		for (int i = 0; i < message.length(); i++) {
			if (line[i].equals("\n")) {
				nbLine++;
			}
		}
		double BG_WIDTH = FXGL.getAppWidth();
	    double BG_HEIGHT = 40 * nbLine + 6;
	    Entity notif = createRectangle(
	    		BG_WIDTH, 
	    		BG_HEIGHT,
	    		new Point2D(RPGApp.player.getX() - FXGL.getAppWidth() / 2, RPGApp.player.getY() - FXGL.getAppHeight() / 2)
	    );
	    Text text = FXGL.getUIFactoryService().newText(message, Color.WHITE, 30.0);
	    Entity posText = FXGL.entityBuilder()
	    		.at(16, 32)
                .build();
	    text.setLayoutX(16);
	    text.setLayoutY(32);
	    posText.getViewComponent().addChild(text);
	    notif.getViewComponent().addChild(text);
	    return notif;
	}
	
	public static void updateNotif(String message) {
		Entity notif = RPGApp.listeMaps.get(RPGApp.hero.getCurrentMap()).notif;
		Text text = (Text) notif.getViewComponent().getChildren().get(1);
		text.setText(message);
		Rectangle rectangle = (Rectangle) notif.getViewComponent().getChildren().get(0);
		rectangle.setHeight(rectangle.getHeight() + 40);
	}
	
	protected static void removeEntity(Point2D position, EntityType type) {
		Entity entity = findEntity(position, type);
		FXGL.getGameWorld().removeEntity(entity);
	}
	
	public static Entity findEntity(Point2D position, EntityType type) {
		for (Entity entity : FXGL.getGameWorld().getEntitiesAt(position)) {
			if (entity.isType(type)) {
				return entity;
			}
		}
		return null;
	}
	
//	public class Paragraph {
//		private String message;
//		private int line;
//		
//		public Paragraph(String message, int line) {
//			this.message = message;
//			this.line = line;
//		}
//
//		public String getMessage() {
//			return message;
//		}
//
//		public void setMessage(String message) {
//			this.message = message;
//		}
//
//		public int getLine() {
//			return line;
//		}
//
//		public void setLine(int line) {
//			this.line = line;
//		}
//	}
	
	public static String returnLine(String message, int lineSize) {
		ArrayList<Integer> liste = new ArrayList<Integer>();
		int posEndWord = 0;
		String[] line = message.split("");
		for (int i = 0; i < message.length(); i++) {
			if (line[i].equals("\n")) {
				String first = message.substring(0, i + 1);
				String next = returnLine(message.substring(i + 1), lineSize);
				return first + next;
			}
			if (line[i].equals(" ") || line[i].equals("!") || line[i].equals("?")) {
				posEndWord = i+1;
			}
			if (i % lineSize == 0 && i != 0) {
				if (line[i].equals("!") || line[i].equals("?")) {
					liste.add(i+2);
				}
				else if (line[i].equals(" ")) {
					liste.add(i+1);
				}
				else {
					liste.add(posEndWord);
				}
			}
		}
		for (int i = 0; i < liste.size(); i++) {
			String first = message.substring(0, liste.get(i) + i);
			String next = message.substring(liste.get(i) + i); 
			message = first + "\n" + next;
		}	
		return message;
	}
	
	public static void addbar(ViewComponent viewComponent, String property, double posX, double posY, Color color) {
		double percentage=0;
		
		Node ColorBar = createComponentBar(color,posX,posY, percentage,new Point2D(posX,posY));
		Node RedBar = createComponentBar(Color.RED,posX,posY,percentage,new Point2D(posX + 192 * (100 - (percentage)) / 100, posY));
	
		viewComponent.getEntity().getProperties().setValue(property+"Red", RedBar);
		viewComponent.getEntity().getProperties().setValue(property, ColorBar);
		viewComponent.addChild(RedBar);
		viewComponent.addChild(ColorBar);
	}
	public static Node createComponentBar(Color color, double posX, double posY,double percentage,Point2D pos) {
		Rectangle borderColor;
		
		if (color.equals(Color.RED)) {
			borderColor = createBorder(Math.round((192 * (percentage)) / 100), 16);
	
		}
		else {
			borderColor = createBorder(Math.round((192 * (100 - percentage)) / 100), 16);
		}
		borderColor.setLayoutX(posX);
		borderColor.getProperties().put("X", posX);
		borderColor.setFill(color);
		borderColor.setArcHeight(0.0);
		borderColor.setArcWidth(0.0);
		borderColor.setLayoutY(posY);
		borderColor.getProperties().put("Y", posY);
		Entity colorBar = createRectangleWithBorder(borderColor, new Point2D(pos.getX(), pos.getY()));
		colorBar.setProperty("border", borderColor);
		colorBar.setProperty("position", new Point2D(posX, posY));
		return borderColor;
	}
	
	public static void updateBar(Node node, String color, double percentage) {
		BigDecimal percentageDecimal = new BigDecimal(Double.toString(percentage));
		percentage = percentageDecimal.doubleValue();
		if (color.equals("red")) {
			((Rectangle) node).setWidth(Math.round((192 * (percentage)) / 100));
			node.setLayoutX((Double) (node.getProperties().get("X")) + 192 * (100 - (percentage)) / 100);
		} 
		else {
			((Rectangle) node).setWidth(Math.round((192 * (100 - percentage)) / 100));
		}
	}
}