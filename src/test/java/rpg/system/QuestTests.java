package rpg.system;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.jupiter.api.Test;

import rpg.RPGApp;
import rpg.entities.character.Monsters;

public class QuestTests {

	@Test
	public void testValidQuest() {
		// init
		RPGApp rpg = new RPGApp();
		Quest quest = new Quest();
		quest.setName("bouhhhh");
		quest.setReward(400);
		
		// process
		quest.validQuest(rpg.hero);
		
		// assert
		assertEquals(400, rpg.hero.getExperience());
		assertTrue(rpg.hero.getFinishQuests().contains("bouhhhh"));
	}

	@Test
	public void testVerifQuest() {
		// init
		Quest quest = new Quest("beaucoup bobo", 100, Monsters.BOSSRAT, 1000);
		
		// process
		quest.setNbTarget(5);
		
		// assert
		assertTrue(!quest.verifQuest());
		
		// process 2
		quest.setNbTarget(1000);
		
		// assert 2
		assertTrue(quest.verifQuest());
		
		// process 3
		quest.setNbTarget(1500);
		
		// assert 3
		assertTrue(quest.verifQuest());
	}
}