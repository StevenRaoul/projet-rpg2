package rpg.system;

import static org.junit.Assert.assertEquals;

import org.junit.jupiter.api.Test;

import javafx.geometry.Point2D;
import rpg.RPGApp;
import rpg.controler.MainController;
import rpg.entities.character.Hero;
import rpg.entities.items.Weapon;
import rpg.entities.objects.Chest;

public class SaveMapTests {
	
	@Test
	public void saveAndLoadTest() {
		// init
		Hero hero = new Hero("ian");
		hero.setCurrentMap("maison");
		hero.setPosition(new Point2D(0, 0));
		RPGApp rpg = new RPGApp();
		Weapon axe = new Weapon(40, "Hache", "Hache.png");
		Chest chest = new Chest(axe);
		rpg.initMaps();
		MainController main = new MainController();
		
		// process
		rpg.listeMaps.get("maisonRPG.tmx").getChestList().put(new Point2D(0, 10), chest);
		main.save(hero);
		rpg.listeMaps.get("maisonRPG.tmx").getChestList().get(new Point2D(0, 10)).setLoot(null);
		main.load();
		
		// assert
		assertEquals(rpg.listeMaps.get("maisonRPG.tmx").getChestList().get(new Point2D(0, 10)).getLoot().getType(), axe.getType());
	}
}