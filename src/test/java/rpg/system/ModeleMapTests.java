package rpg.system;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

public class ModeleMapTests {

	@Test
	public void testInit() {
		// init
		ModeleMap map = new ModeleMap();
		
		// process
		map.init();
		
		// assert
		assertTrue(map.getReturnPortalList() != null);
		assertTrue(map.getPortalList() != null);
		assertTrue(map.getMonsterList() != null);
		assertTrue(map.getPNJList() != null);
		assertTrue(map.getChestList() != null);
		assertTrue(map.getClueList() != null);
	}
}