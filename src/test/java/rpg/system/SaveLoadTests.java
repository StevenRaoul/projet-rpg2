package rpg.system;

import static org.junit.Assert.assertEquals;

import org.junit.jupiter.api.Test;

import javafx.geometry.Point2D;
import rpg.controler.MainController;
import rpg.entities.character.Hero;

public class SaveLoadTests {

	@Test
	public void saveAndLoadTest() {
		// init
		Hero hero = new Hero("ian");
		hero.setCurrentMap("maison");
		hero.setPosition(new Point2D(0, 0));
		MainController mainController = new MainController();
		
		// process
		mainController.save(hero);
		Hero loadedHero = mainController.load();


		// assert
		assertEquals(hero.getPosition(), loadedHero.getPosition());
		assertEquals(hero.getCurrentMap(), loadedHero.getCurrentMap());
		assertEquals(hero.getName(), loadedHero.getName());
	}
}