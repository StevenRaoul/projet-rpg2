package rpg.system;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import rpg.entities.character.Monster;
import rpg.entities.character.State;

import static org.junit.jupiter.api.Assertions.assertEquals;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class SystemsTests {

    private Monster rat = new Monster();
    private Fights fight = new Fights();

    @BeforeAll
    public void init(){
        rat.setDef(20);
        rat.setHpMax(50);
        rat.setHp(50);
        rat.setAtk(10);
    }

    @Test
    public void fightTest() {
        // init
        int hp = 40;
        
        // process
        fight.battleAttack(rat, hp);
        
        // assert
        assertEquals(30,rat.getHp());
    }

    @Test
    public void deathTest(){
        // init
        rat.setState(State.ALIVE);
        
        // process
        fight.battleAttack(rat, 90);
        
        //Assert
        assertEquals(State.DEAD, rat.getState());
        assertEquals(0, rat.getHp());
    }
}