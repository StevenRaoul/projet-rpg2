package rpg.system;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import rpg.RPGApp;
import rpg.controler.MonsterController;
import rpg.entities.character.Hero;

import rpg.entities.character.Monster;
import rpg.entities.character.State;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class FightTests {

	private Monster rat = new Monster();
	private Fights fight = new Fights();
	
	
	@BeforeEach
	public void init() {
		rat.setDef(20);

		rat.setHpMax(50);
		rat.setHp(50);

		rat.setAtk(30);
	}

	@Test
	public void basicTurnAttackTest() {
		// init
		Hero hero = new Hero("ian");
		hero.setAtk(30);
		hero.setDef(20);
		hero.setHpMax(50);
		hero.setHp(50);
		rat.setState(State.ALIVE);
		
		
		// process
		try {
			fight.battle(hero, rat, "attaque");
			fight.battle(rat, hero, "attaque");
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		// assert
		assertEquals(40, hero.getHp());
		assertEquals(40, rat.getHp());
	}

	@Test
	public void basicTurnDefenseTest() {
		// init
		Hero hero = new Hero("ian");
		hero.setAtk(30);
		hero.setDef(20);
		hero.setHpMax(50);
		hero.setHp(50);
		rat.setState(State.ALIVE);
		
		// process
		try {
			fight.battle(hero, rat, "défense");
			fight.battle(rat, hero, "attaque");
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		// assert
		assertEquals(49, hero.getHp());
		assertEquals(50, rat.getHp());
	}

	@Test
	public void fightTestExceptedException() {
		// init
		Hero hero = new Hero("ian");
		hero.setAtk(30);
		hero.setDef(20);
		hero.setHpMax(50);
		hero.setHp(50);
		
		//process
		Exception ratDead = assertThrows(Exception.class, () -> {
			rat.setState(State.DEAD);
			fight.battle(hero, rat, "défense");
		});
		Exception heroDead = assertThrows(Exception.class, () -> {
			hero.setState(State.DEAD);
			fight.battle(hero, rat, "défense");
		});
		
		//assert
		assertEquals("Erreur l'un des 2 Character est mort", ratDead.getMessage());
		assertEquals("Erreur l'un des 2 Character est mort", heroDead.getMessage());
	}

	@Test
	public void fightTest() {
		// init
		int attack = 40;
		
		// process
		fight.battleAttack(rat, attack);
		
		// assert
		assertEquals(30, rat.getHp());
	}

	@Test
	public void deathTest() {
		// init
		rat.setState(State.ALIVE);
		
		// process
		fight.battleAttack(rat, 90);
		
		// assert
		assertEquals(State.DEAD, rat.getState());
		assertEquals(0, rat.getHp());
	}
}