package rpg.entities.objects;

import org.junit.jupiter.api.Test;

import rpg.entities.items.Item;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ChestTests {
    
    @Test
    public void testGetChest() {
    	Item item = new Item();
    	Chest chest = new Chest(item);
        assertEquals(item, chest.getLoot());
    }
    
    @Test
    public void testSetChest() {
    	Item item = new Item();
    	Chest chest = new Chest();
    	chest.setLoot(item);
        assertEquals(item, chest.getLoot());
    }
}