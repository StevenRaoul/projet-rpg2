package rpg.entities.objects;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class ClueTests {
    
    @Test
    public void testGetClue() {
    	Clue clue = new Clue("clue", "clue.png");
        assertEquals("clue", clue.getName());
        assertEquals("clue.png", clue.getImage());
    }
    
    @Test
    public void testSetClue() {
    	Clue clue = new Clue();
    	clue.setName("clue");
    	clue.setImage("clue.png");
    	assertEquals("clue", clue.getName());
        assertEquals("clue.png", clue.getImage());
    }
}