package rpg.entities.character;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.HashMap;

import org.junit.jupiter.api.Test;

import rpg.system.Quest;

public class NpcTests {

    @Test
    public void testGetNpc() {
    	Quest quest = new Quest();
    	HashMap<String, HashMap<String, String[]>> chat = new HashMap<String, HashMap<String, String[]>>();
    	Npc npc = new Npc("Josef","Josef",chat);
    	npc.setGiveQuest("oui");
    	assertEquals("Josef", npc.getName());
        assertEquals("images/entities/pnjs/vue/Josef_Cadre.png", npc.getImage());
        assertEquals(chat, npc.getListChat());
        //assertEquals(quest, npc.getQuest());
        assertEquals("oui", npc.getgiveQuest());
    }
    
    @Test
    public void testSetNpc() {
    	Npc npc = new Npc();
    	Quest quest = new Quest();
    	HashMap<String, HashMap<String, String[]>> chat = new HashMap<String, HashMap<String, String[]>>();
    	npc.setListChat(chat);
    	npc.setGiveQuest("oui");
    	//npc.setQuest(quest, "oui");
    	assertEquals("defaut", npc.getName());
        assertEquals("images/entities/pnjs/vue/defaut_Cadre.png", npc.getImage());
        assertEquals(chat, npc.getListChat());
        //assertEquals(quest, npc.getQuest());
        assertEquals("oui", npc.getgiveQuest());
    }
    
    @Test
    public void testGetRotateImage() {
    	Npc npc = new Npc();
    	assertEquals("images/entities/pnjs/vue/defaut_Dos.png", npc.getRotateImage("Dos"));
    }
}