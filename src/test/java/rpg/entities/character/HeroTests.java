package rpg.entities.character;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.HashMap;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import javafx.geometry.Point2D;
import rpg.entities.abilities.Attack;
import rpg.entities.abilities.Boost;
import rpg.entities.abilities.Heal;
import rpg.entities.abilities.Skill;
import rpg.entities.items.Armor;
import rpg.entities.items.Equipment;
import rpg.entities.items.Weapon;
import rpg.system.Quest;

public class HeroTests {

    @Test
    public void testGetHero(){
        Hero hero = new Hero("hero");
        ArrayList<Skill> skills = new ArrayList<Skill>();
        HashMap<String, Integer> stats = new HashMap<String, Integer>();
        Quest quest = new Quest();
        quest.setReward(100);
        
        skills.add(new Heal("firstAid", 3, 40));
		stats.put("atk", 20);
		stats.put("def", 20);
		stats.put("pv", 50);
		stats.put("mp", 10);
		hero.setPosition(new Point2D(0,0));
		hero.setCurrentquest(quest);
		hero.setCurrentMap("map.tmx");
		hero.setQueststep(5);
		hero.setExperience(50);
		
        assertEquals("hero", hero.getName());
        assertEquals(20, hero.getAtk());
        assertEquals(20, hero.getDef());
        assertEquals(50, hero.getHp());
        assertEquals(1, hero.getLv());
        assertEquals(50, hero.getExperience());
        assertEquals(skills.get(0).getClass(), hero.getSkills().get(0).getClass());
        assertEquals(10, hero.getMp());
        assertEquals(10, hero.getMpMax());
        assertEquals(stats, hero.getStats());
        assertEquals(20, hero.getSpd());
        assertEquals(State.ALIVE, hero.getState());
        assertEquals(new Point2D(0,0), hero.getPosition());
        assertEquals(quest, hero.getCurrentquest());
        assertEquals("map.tmx", hero.getCurrentMap());
        assertEquals(5, hero.getQueststep());
    }
    
    @Test
    public void testGainExp() {
    	Hero hero = new Hero("hero");
    	hero.gainExp(560);
    	assertEquals(2, hero.getLv());
    	assertEquals(10, hero.getExperience());
    	assertEquals(22, hero.getAtk());
    	assertEquals(22, hero.getDef());
    	assertEquals(55, hero.getHp());
    	assertEquals(11, hero.getMp());
    }
    
    @Test
    public void testEquip() {
    	Hero hero = new Hero("hero");
    	Weapon weaponTest = new Weapon(1, "Couteau en plastique", "couteauPlastique.png");
    	Weapon weapon = new Weapon(10, "Couteau", "couteau.png");
    	Armor armor = new Armor(15, "Pull", "pull.png");
    	hero.equip(weaponTest);
    	hero.equip(weapon);
    	hero.equip(armor);
    	assertEquals(30, hero.getAtk());
    	assertEquals(35, hero.getDef());
    	assertEquals("Couteau", hero.getEquipement().get("Arme").getName());
    	assertEquals("Pull", hero.getEquipement().get("Armure").getName());
    }
    
    @Test
    public void testUnequip() {
    	Hero hero = new Hero("hero");
    	Weapon weapon = new Weapon(10, "Couteau", "couteau.png");
    	Armor armor = new Armor(15, "Pull", "pull.png");
    	hero.equip(weapon);
    	hero.equip(armor);
    	hero.unequip();
    	assertEquals(20, hero.getAtk());
    	assertEquals(20, hero.getDef());
    	assertEquals(null, hero.getEquipement().get("Arme"));
    	assertEquals(null, hero.getEquipement().get("Armure"));
    }
    
    @Test
    public void testAddItemInventory() {
    	Hero hero = new Hero("hero");
    	Weapon weapon = new Weapon(10, "Couteau", "couteau.png");
    	Armor armor = new Armor(15, "Pull", "pull.png");
    	hero.addItemInventory(weapon);
    	hero.addItemInventory(armor);
    	assertEquals(weapon, hero.getInventory()[0]);
    	assertEquals(armor, hero.getInventory()[1]);
    }
    
    @Test
    public void testRemoveItemInventory() {
    	Hero hero = new Hero("hero");
    	Weapon weapon = new Weapon(10, "Couteau", "couteau.png");
    	Armor armor = new Armor(15, "Pull", "pull.png");
    	hero.addItemInventory(weapon);
    	hero.addItemInventory(armor);
    	hero.removeItemInventory(weapon);
    	assertEquals(null, hero.getInventory()[0]);
    	assertEquals(armor, hero.getInventory()[1]);
    }
    
    @Test
    public void testHeal() {
    	Hero hero = new Hero("hero");
    	hero.setHp(10);
    	hero.heal(40);
    	assertEquals(30, hero.getHp());
    }
    
    @Test
    public void testUseSkill() {
    	Hero hero = new Hero("hero");
    	hero.useSkill(5);
    	assertEquals(5, hero.getMp());
    }
    
    @Test
    public void testUpdateStats() {
    	Hero hero = new Hero("hero");
    	hero.setAtk(7000);
    	hero.setDef(800);
    	hero.setHp(1);
    	hero.useSkill(1);
    	hero.updateStats();
    	assertEquals(7000, hero.getStats().get("atk"));
    	assertEquals(800, hero.getStats().get("def"));
    	assertEquals(1, hero.getStats().get("pv"));
    	assertEquals(9, hero.getStats().get("mp"));
    }
    
    @Test
    public void testGivePermanently() {
    	Hero hero = new Hero("hero");
    	hero.givePermanently("atk", 500);
    	hero.givePermanently("def", 500);
    	hero.givePermanently("pv", 500);
    	hero.givePermanently("mp", 500);
    	assertEquals(520, hero.getAtkMax());
    	assertEquals(520, hero.getDefMax());
    	assertEquals(550, hero.getHpMax());
    	assertEquals(510, hero.getMpMax());
    }
    
    @Test
    public void testGive() {
    	Hero hero = new Hero("hero");
    	hero.give("atk", 500);
    	hero.give("def", 500);
    	hero.give("pv", 500);
    	hero.give("mp", 500);
    	assertEquals(520, hero.getAtk());
    	assertEquals(520, hero.getDef());
    	assertEquals(550, hero.getHp());
    	assertEquals(510, hero.getMp());
    }
    
    @Test
    public void testGiveMultiply() {
    	Hero hero = new Hero("hero");
    	assertEquals(120, hero.givemultiply("atk", 500));
    	assertEquals(120, hero.givemultiply("def", 500));
    	assertEquals(300, hero.givemultiply("pv", 500));
    	assertEquals(60, hero.givemultiply("mp", 500));
    	assertEquals(0, hero.givemultiply("fail", 500));
    }
    
    @Test
    public void testResetTemporaryBonus() {
    	Hero hero = new Hero("hero");
    	hero.give("atk", 500);
    	hero.give("def", 500);
    	hero.give("pv", 500);
    	hero.give("mp", 500);
    	hero.resetTemporaryBonus();
    	assertEquals(20, hero.getAtk());
    	assertEquals(20, hero.getDef());
    	assertEquals(50, hero.getHp());
    	assertEquals(10, hero.getMp());
    }
    
    @Test
    public void testResetTemporaryBonusWithWeapon() {
    	Hero hero = new Hero("hero");
    	Weapon weapon = new Weapon(10, "Couteau", "couteau.png");
    	Armor armor = new Armor(15, "Pull", "pull.png");
    	hero.equip(weapon);
    	hero.equip(armor);
    	hero.give("atk", 500);
    	hero.give("def", 500);
    	hero.give("pv", 500);
    	hero.give("mp", 500);
    	hero.resetTemporaryBonus();
    	assertEquals(30, hero.getAtk());
    	assertEquals(35, hero.getDef());
    	assertEquals(50, hero.getHp());
    	assertEquals(10, hero.getMp());
    }
    
    @Test
    public void testAddListFinishQuests() {
    	Hero hero = new Hero("hero");
    	Quest quest = new Quest("Quête");
    	hero.addlistFinishQuests(quest.getName());
    	assertEquals(true, hero.finishQuest(quest.getName()));
    }
    
    @Test
    public void textExpLvlUp() {
    	Hero hero = new Hero("hero");
    	Quest quest = new Quest();
    	hero.setCurrentquest(quest);
    	quest.setReward(500);
    	hero.rewardQuest();
    	assertEquals(50, hero.getExp(1) - hero.getExperience());
    }
    
    @Test
    public void testSetFullLife() {
    	Hero hero = new Hero("hero");
    	hero.setState(State.DEAD);
    	hero.setHp(0);
    	hero.useSkill(10);
    	hero.setFullLife();
    	hero.restore("mp", 2);
    	assertEquals(50, hero.getHp());
    	assertEquals(2, hero.getMp());
    	assertEquals(State.ALIVE, hero.getState());
    }
}