package rpg.entities.character;

import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.Test;

import rpg.system.Quest;

public class MonsterTests {

    @Test
    public void testGetQuestMonster() {
    	Monster monster = new Monster("rat", 50, 40, 30, true, "Quête", Monsters.BOSSRAT);
    	assertEquals("Quête", monster.getQuest());
        assertEquals(Monsters.BOSSRAT, monster.getTypeMonstre());
        assertEquals(true, monster.isUnique());
        assertEquals("images/entities/monstres/monde/BOSSRAT/Face.png", monster.getImage());
    }
    
    @Test
    public void testGetRandomMonster() {
    	Monster monster = new Monster("rat", 50, 40, 30, Monsters.RATGRIS);
    	assertEquals(Monsters.RATGRIS, monster.getTypeMonstre());
        assertEquals("images/entities/monstres/monde/RATGRIS/Face.png", monster.getImage());
        assertEquals("Ce monstre n'est pas unique et par conséquent ne dispose pas de quête associée", monster.getQuest());
        assertEquals("Balai", monster.getWeaknesses()[0]);
    }
    
    @Test
    public void testSetMonster() {
    	Monster monster = new Monster();
    	monster.setTypeMonster(Monsters.RATSLIME);
    	monster.setGive_experience(5000);
    	assertEquals(Monsters.RATSLIME, monster.getTypeMonstre());
    	assertEquals(5000, monster.getGive_experience());
    }
    
    @Test
    public void testCopyMonster() {
    	Monster monster = new Monster(10, 15, 20, 0, 0, 0, Monsters.RATGRIS);
    	
    	monster.setGive_experience(520);
    	monster.setGive_gold(100);
    	Monster copy = monster.copy();
    	
    	assertEquals(10, copy.getAtk());
    	assertEquals(15, copy.getDef());
    	assertEquals(20, copy.getHp());
    	assertEquals(520, copy.getGive_experience());
    	assertEquals(100, copy.getGive_gold());
    	assertEquals("Balai", copy.getWeaknesses()[0]);
    }
}