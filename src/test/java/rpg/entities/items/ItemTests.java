package rpg.entities.items;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;

import rpg.RPGApp;

public class ItemTests {

    @Test
    public void testGetItem() {
    	Item item = new Item("item", "item.png");
    	item.setType("item");
        assertEquals("item", item.getName());
        assertEquals("images/items/vue/item.png", item.getImage());
        assertEquals("item", item.getType());
        assertEquals(-2, item.getPosition());
    }
    
    @Test
    public void testArmor() {
    	Armor armor = new Armor(100, "Armure de mithril", "armure.png");
    	assertEquals("Armure de mithril", armor.getName());
    	assertEquals("images/items/vue/armure.png", armor.getImage());
    	assertEquals(100, armor.getStat());
    	assertEquals("Armure", armor.getType());
    	assertEquals("images/items/inventaire/armure_Inventaire.png", armor.getInventaireImage());
    	assertEquals("Def : 100", armor.getEffect());
    }
    
    @Test
    public void testWeapon() {
    	Weapon weapon = new Weapon(100, "Épée du dragon sacré", "epee.png");
    	assertEquals("Épée du dragon sacré", weapon.getName());
    	assertEquals("images/items/vue/epee.png", weapon.getImage());
    	assertEquals(100, weapon.getStat());
    	assertEquals("Arme", weapon.getType());
    	assertEquals("images/items/inventaire/epee_Inventaire.png", weapon.getInventaireImage());
    	assertEquals("Atk : 100", weapon.getEffect());
    }
    
    @Test
    public void testHealPotion() {
    	HealPotion hPotion = new HealPotion("Potion de vie", "healPotion.png");
    	assertEquals("Potion de vie", hPotion.getName());
    	assertEquals("images/items/vue/healPotion.png", hPotion.getImage());
    	assertEquals("potion", hPotion.getType());
    	assertEquals("images/items/inventaire/healPotion_Inventaire.png", hPotion.getInventaireImage());
    	assertEquals("Restaure 50 points de vie", hPotion.getEffect());
    }
    
    @Test
    public void testManaPotion() {
    	ManaPotion mPotion = new ManaPotion("Potion de mana", "manaPotion.png");
    	assertEquals("Potion de mana", mPotion.getName());
    	assertEquals("images/items/vue/manaPotion.png", mPotion.getImage());
    	assertEquals("potion", mPotion.getType());
    	assertEquals("images/items/inventaire/manaPotion_Inventaire.png", mPotion.getInventaireImage());
    	assertEquals("Restaure 10 points de mana", mPotion.getEffect());
    }
    
    @Test
    public void testEffectPotion() {
    	ManaPotion mPotion = new ManaPotion("Potion de mana", "manaPotion.png");
    	HealPotion hPotion = new HealPotion("Potion de vie", "healPotion.png");
    	RPGApp.hero.setHp(0);
    	RPGApp.hero.setMp(0);
    	mPotion.effect();
    	hPotion.effect();
    	assertEquals(25, RPGApp.hero.getHp());
    	assertEquals(10, RPGApp.hero.getMp());
    }
}