package rpg.entities.abilities;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;

import rpg.RPGApp;
import rpg.entities.character.Monster;
import rpg.entities.character.Monsters;

public class SkillTests {

    @Test
    public void testGetHeal() {
    	Heal heal = new Heal("Soin supérieur", 50, 70);
    	assertEquals("Soin supérieur", heal.getName());
    	assertEquals(50, heal.getCost());
    	assertEquals(false, heal.canInFight());
    	assertEquals(SkillType.HEAL, heal.getType());
    }
    
    @Test
    public void testGetAttack() {
    	Attack attack = new Attack("German suplex", 150, 70);
    	assertEquals("German suplex", attack.getName());
    	assertEquals(150, attack.getCost());
    	assertEquals(true, attack.canInFight());
    	assertEquals(SkillType.ATTACK, attack.getType());
    }
    
    @Test
    public void testGetBoost() {
    	Boost boost = new Boost("Danse-lames", 20, 5, "atk", 2);
    	assertEquals("Danse-lames", boost.getName());
    	assertEquals(20, boost.getCost());
    	assertEquals(true, boost.canInFight());
    	assertEquals(SkillType.BOOST, boost.getType());
    }
    
    @Test
    public void testHealEffect() {
    	Heal heal = new Heal("Soin inférieur", 5, 10);
    	RPGApp.hero.setHp(10);
    	RPGApp.hero.restore("mp", 10);
    	heal.effect(RPGApp.hero);
    	assertEquals(15, RPGApp.hero.getHp());
    	assertEquals(5, RPGApp.hero.getMp());
    }
    
    @Test
    public void testAttackEffect() {
    	Attack attack = new Attack("Coup en diagonale", 2, 100);
    	Monster monster = new Monster("monster", 0, 0, 100, Monsters.RATGRIS);
    	RPGApp.hero.restore("mp", 10);
    	attack.effect(RPGApp.hero, monster);
    	assertEquals(60, monster.getHp());
    	assertEquals(8, RPGApp.hero.getMp());
    }
    
    @Test
    public void testBoostEffect() {
    	Boost boost = new Boost("Mur de fer", 1, 200, "def", 5);
    	RPGApp.hero.restore("mp", 10);
    	boost.effect(RPGApp.hero);
    	assertEquals(80, RPGApp.hero.getDef());
    	assertEquals(9, RPGApp.hero.getMp());
    	assertEquals(boost.getName(), RPGApp.hero.getBoosts().get(0).getName());
    }
    
    @Test
    public void testDeleteBonusBoost() {
    	Boost boost = new Boost("Mur de fer", 1, 5, "def", 2);
    	boost.effect(RPGApp.hero);
    	boost.deleteBonus(RPGApp.hero);
    	assertEquals(20, RPGApp.hero.getDef());
    }
}