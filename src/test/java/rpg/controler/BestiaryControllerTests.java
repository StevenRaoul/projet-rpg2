package rpg.controler;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import org.junit.jupiter.api.Test;

import rpg.entities.character.Monster;
import rpg.entities.character.Monsters;

public class BestiaryControllerTests {
	
	
	@Test
	public void testload() {
		// init
		BestiaryController bestiaryController = new BestiaryController();
		
		//process
		Exception fileNotFound = assertThrows(Exception.class, () -> {
			bestiaryController.loadBestiary("lolo");
		});
		
		Exception fileBadlyParsed = assertThrows(Exception.class, () -> {
			String FileName = createFile("lolo");
			FileWriter file = new FileWriter(FileName);
			file.write("<<>>");
			file.close();
			bestiaryController.loadBestiary(FileName);
		});
//		Exception fileB = assertThrows(Exception.class, () -> {
//			String FileName = createFile("lolo");
//			FileWriter file = new FileWriter(FileName);
//			file.write("{'monsters':[{'atk':'yo'}]");
//			file.close();
//			bestiaryController.loadBestiary(FileName);
//		});
		try {
			bestiaryController.loadBestiary();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			assertTrue(false);
			e.printStackTrace();
		}
		
		
		//assert
		assertEquals(fileNotFound.getMessage(),"file not found");
		assertEquals("erreur de parsing du fichier",fileBadlyParsed.getMessage());
		//assertEquals("erreur de parsing du fichier",fileB.getMessage());
		assertTrue(bestiaryController.getMonster(Monsters.RATGRIS)!=null);

	}
	public String createFile(String name) {
		return new File(name+".json").getAbsolutePath();
	}
	@Test
	public void getMonsterTest() {
		//init
		BestiaryController bestiaryController = new BestiaryController();
		Monster monstre = new Monster(30, 30, 30, 30, 0, 0, Monsters.RATGRIS);
		monstre.setGive_experience(30);
		try {
			bestiaryController.loadBestiary();
		} catch (Exception e4) {
			// TODO Auto-generated catch block
			e4.printStackTrace();
		}
		//
		String FileName = createFile("lolilop");
		FileWriter file=null;
		try {
			file = new FileWriter(FileName);
		} catch (IOException e3) {
			// TODO Auto-generated catch block
			e3.printStackTrace();
		}
		try {
			//file.write(bestiaryController.getMonster(Monsters.RATGRIS).toString());
			file.write("{\"monsters\":[{"
					+ "\"atk\":30,"
					+ "\"def\":30,"
					+ "\"hp\":30,"
					+ "\"mp\":30,"
					+ "\"type\":\"RATGRIS\","
					+ "\"give_experience\":30,"
					+ "\"skills\":[]}]}"
					);
			
		} catch (IOException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		try {
			file.close();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			bestiaryController.loadBestiary(FileName);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println(bestiaryController.getBestiary());
		Monster monster = bestiaryController.getMonster(Monsters.RATGRIS);
		assertEquals(monstre.getClass(), monster.getClass());
	}

}
