package rpg.controler;

import static org.junit.Assert.assertTrue;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map.Entry;

import org.json.simple.JSONObject;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import javafx.geometry.Point2D;
import rpg.RPGApp;
import rpg.system.ModeleMap;


@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class NpcControlerTests {

	@Test
	public void testInitNpc() { // test for the method working
		//init
		int index = 0;
		String map = "maisonRPG.tmx";
		RPGApp rpg = new RPGApp();
		ArrayList<LinkedHashMap> npcListe = getTestingFile("src/main/resources/assets/npcs/baseNpc.json");
		LinkedHashMap position = (LinkedHashMap) npcListe.get(index).get("position");
		rpg.initMaps();
		NpcComponent npcComponent = new NpcComponent();
		//process
		npcComponent.initNpc("src/main/resources/assets/npcs/baseNpc.json");	
		//Assert
		assertTrue(rpg.listeMaps.get(map).getPNJList()
				.containsKey(new Point2D((int) position.get("x"), (int) position.get("y"))));
		assertTrue(npcListe.get(index).get("name").equals("pere"));
	}

	@Test
	public void testIntegrationNpcs() { // test the method integrate correctly all the npcs
		RPGApp rpg = new RPGApp();
		rpg.initMaps();
		NpcComponent npcComponent = new NpcComponent();
		npcComponent.initNpc("src/main/resources/assets/npcs/baseNpc.json");
		ArrayList<LinkedHashMap> npcListe = getTestingFile("src/main/resources/assets/npcs/baseNpc.json");
		if (npcListe != null) {
			for (int i = 0; i < npcListe.size(); i++) {
				for (Entry<String, ModeleMap> mapEntry : rpg.listeMaps.entrySet()) {
					String map = mapEntry.getKey();
					if (npcListe.get(i).get("map").equals(map)) {
						LinkedHashMap position = (LinkedHashMap) npcListe.get(i).get("position");
						assertTrue(rpg.listeMaps.get(map).getPNJList()
								.containsKey(new Point2D((int) position.get("x"), (int) position.get("y"))));
					}
				}
			}
		} 
		else {
			assertTrue(false);
		}
	}

	public ArrayList<LinkedHashMap> getTestingFile(String file) {
		ObjectMapper mapper = new ObjectMapper();
		try {
			JSONObject liste = mapper.readValue(new FileReader(file), JSONObject.class);
			return (ArrayList<LinkedHashMap>) liste.get("npcs");
		} catch (JsonParseException e1) {
			e1.printStackTrace();
		} catch (JsonMappingException e1) {
			e1.printStackTrace();
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		return null;
	}
}