package rpg.controler;

import static org.junit.Assert.assertEquals;

import java.util.HashMap;
import java.util.Map.Entry;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import rpg.entities.abilities.Attack;
import rpg.entities.character.Character;
import rpg.entities.character.Hero;
import rpg.entities.character.Monster;
import rpg.entities.character.Monsters;
import rpg.entities.character.State;
import rpg.system.QuestTarget;

public class FightControllerTests {
	
	
	@Test
	public void test() {
		FightController fightController = new FightController();
		Hero hero = new Hero("ian");
		hero.setAtk(100);
		Monster monster = new Monster(30, 30, 30, 30, 0, 0, Monsters.RATGRIS);
		fightController.initPlayer(hero, monster);
		//fightController.checkDeaths();
		HashMap<Character, Boolean> CanAct = fightController.getCanAct();
		hero.setState(State.DEAD);
		CanAct.put(hero, true);
		for (Entry<Character, Boolean> player : CanAct.entrySet()) {
			if (player.getKey().getState() == State.DEAD) {
				CanAct.replace(player.getKey(), false);
			}
		}
		Assertions.assertTrue(!CanAct.get(hero));
		Assertions.assertTrue(CanAct.get(monster));
		/*if (CanAct.get(character)) {
			if (character instanceof Monster) {
				monsterController.act(this);
				return;
			}
		}
		if (!CanAct.get(character) && !CanAct.get(actionController.getOpponent(character))) {
			String state = getState();
			if(state.equals("monsterDead")) {
				hero.gainExp(monster.getGive_experience());
				hero.gainGold(monster.getGive_gold());
				questController.advanceQuest(QuestTarget.KILL ,monster.getTypeMonstre());
			}
			nextTurn(state);
			for (Entry<Character, Boolean> player : CanAct.entrySet()) {
				if (player.getKey().getState() == State.ALIVE) {
					CanAct.replace(player.getKey(), true);
				}
			}
		}*/
	}
	
	
	@Test
	public void MonsterFighttest() {
		FightController fightController = new FightController();
		Hero hero = new Hero("ian");
		Monster monster = new Monster(30, 30, 30, 30, 0, 0, Monsters.RATGRIS);
		fightController.initPlayer(hero, monster);
		fightController.manage("attaque", monster);
		assertEquals(40, hero.getHp());
		assertEquals(fightController.getAct(monster), false);
	}
	@Test
	public void stateTest() {
		FightController fightController = new FightController();
		Hero hero = new Hero("ian");
		hero.setAtk(60);
		hero.setHp(30);
		Monster monster = new Monster(30, 30, 30, 30, 0, 0, Monsters.RATGRIS);
		fightController.initPlayer(hero, monster);
		fightController.useBasicAction("attaque", hero);
		String state=fightController.getState();
		assertEquals("monsterDead", state);
		assertEquals(false, fightController.getAct(hero));
	}
	@Test
	public void MonsterFightSkilltest() {
		FightController fightController = new FightController();
		Hero hero = new Hero("ian");
		hero.setHp(100);
		hero.setDef(40);
		Monster monster = new Monster(30, 30, 30, 30, 0, 0, Monsters.RATGRIS);
		fightController.initPlayer(hero, monster);
		fightController.manage(new Attack("GodSlash", 10, 50), monster);
		assertEquals(95, hero.getHp());
		assertEquals(false, fightController.getAct(monster));
	}

	@Test
	public void updateTextTest() {
		FightController fightController = new FightController();
		Hero hero = new Hero("ian");
		Monster monster = new Monster(30, 30, 30, 30, 30, 0, Monsters.RATGRIS);
		monster.setGive_experience(30);
		String beginText = fightController.updateText("begin", monster);
		String continueText = fightController.updateText("continue", monster);
		String monsterDeadText = fightController.updateText("monsterDead", monster);
		String heroDeadText = fightController.updateText("heroDead", monster);
		String SkillMonsterText = fightController.getMessage(new Attack("Gigentaille !", 20, 20), monster);
		String SkillHeroText = fightController.getMessage(new Attack("Gigentaille !", 20, 20), hero);
		String AttackMonsterText = fightController.getMessage("attaque", monster);
		String DefenseMonsterText = fightController.getMessage("défense", monster);
		String AttackHeroText = fightController.getMessage("attaque", hero);
		String DefenseHeroText = fightController.getMessage("défense", hero);
		assertEquals("RATGRIS as utilisé Gigentaille !", SkillMonsterText);
		assertEquals("ian as utilisé Gigentaille !", SkillHeroText);
		assertEquals("RATGRIS as fait attaque", AttackMonsterText);
		assertEquals("RATGRIS as fait défense", DefenseMonsterText);
		assertEquals("ian as fait attaque", AttackHeroText);
		assertEquals("ian as fait défense", DefenseHeroText);
		assertEquals("Tu as trouvé RATGRIS, que veut tu faire ?", beginText);
		assertEquals("RATGRIS est encore vivant, que veut tu faire ?", continueText);
		assertEquals("RATGRIS est mort !\nBravo tu as gagné 30 points d'expérience", monsterDeadText);
		assertEquals("Tu est mort, veux tu recommencer ?", heroDeadText);
	}
}