package rpg.controler;

import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.Test;

import rpg.entities.abilities.Attack;
import rpg.entities.abilities.Heal;
import rpg.entities.character.Hero;
import rpg.entities.character.Monster;
import rpg.entities.character.Monsters;

public class ActionControllerTests {

	@Test
	public void useSkillTest() {
		Hero hero = new Hero("Ian");
		hero.setAtk(20);
		hero.setHp(5);
		hero.setHpMax(100);
		Monster monster = new Monster(10, 10, 100, 0, 0, 0, Monsters.RATDEMON);
		ActionController actionController = new ActionController(hero, monster);
		
		actionController.skillAction(new Heal("MegaHeal", 0, 20), hero);
		actionController.skillAction(new Attack("UltimateOmegaSlash",0,50), hero);
		
		assertEquals(25, hero.getHp()); //5+(100*(20/100))=5+(100*0.2)=5+20=25
		assertEquals(80, monster.getHp()); //100-((20*1+(50/100))-10)=100-((20*1.5)-10)=100-(30-10)=100-20=80
	}
	
	@Test
	public void useBasicActionAttackTest() {
		Hero hero = new Hero("Ian");
		Monster monster = new Monster(10, 10, 100, 0, 0, 0, Monsters.RATDEMON);
		ActionController actionController = new ActionController(hero, monster);
	
		actionController.basicAction("attaque", hero);
		actionController.basicAction("attaque", monster);
		
		assertEquals(49, hero.getHp());
		assertEquals(90, monster.getHp());
	}
	
	@Test
	public void useBasicActionDefenseTest() {
		Hero hero = new Hero("Ian");
		Monster monster = new Monster(10, 10, 100, 0, 0, 0, Monsters.RATDEMON);
		ActionController actionController = new ActionController(hero, monster);
	
		actionController.basicAction("défense", hero);
		actionController.basicAction("défense", monster);
		
		assertEquals(40, hero.getDef());
		assertEquals(20, monster.getDef());
	}
	
	@Test
	public void getOpponentTest() {
		Hero hero = new Hero("Ian");
		Monster monster = new Monster(10, 10, 100, 0, 0, 0, Monsters.RATDEMON);
		ActionController actionController = new ActionController(hero, monster);
	
		assertEquals(monster, actionController.getOpponent(hero));
		assertEquals(hero, actionController.getOpponent(monster));
	}
	
}
